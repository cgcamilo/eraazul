<?php
include 'includes.php';
$usuario = unserialize($_SESSION['usuario']);
if( !isset($_SESSION['usuario']) ){
    header("location: ./index.php?oops");
    exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>La Era Azul - Libros y Accesorios</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<link type="image/x-icon" href="era_azul.ico" rel="shortcut icon" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/large.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/mootools-core-1.3.1-full-compat-yc.js"></script>
<script type="text/javascript" src="js/sexyforms.v1.3.mootools.min.js"></script>
<script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link   type="text/css" rel="stylesheet" href="sexyforms/gamma/sexyforms-gamma.css"  media="all" id="theme" />
<script type="text/javascript">
window.addEvent('domready', function() {
  $$(".sexyform input", ".sexyform select", ".sexyform textarea").each(function(el) {
    el.DoSexy();
  });
});
</script>

<script src="js/jquery.tools.min.js"></script>
<style type="text/css">
<!--
label {padding-right: 10px;}
-->
</style>
</head>
<body> 
<div id="header">
  <div class="gutter">
    <div class="inner">
      <div id="htop">
        <?php include("includes/bar.php"); ?>
        <?php include("includes/logo.php"); ?>
      </div>
      <?php include("includes/menu.php"); ?>
    </div>
  </div>
</div>
<div id="wrapper">
  <div id="wrapperbg">
    <div class="inner clearfix">
      <div id="main">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="215" valign="top"><?php include("includes/publicidad.php"); ?></td>
            <td valign="top">
            <div id="m_tot">
              <div id="contenido">
                <div class="content">
                  <div id="cart"> 
                    <h1>Mi Cuenta</h1>
                    <form action="php/action/editDatos.php" method="post" class="sexyform">
        <table class="fondoTabla" width="100%" cellspacing="0" cellpadding="0">
          <tbody>
          <tr>
            <td width="25%" align="right">Nombre</td>
            <td width="75%">
           	<input type="text" name="nombre" value="<?php echo $usuario->getNombre();?>" />            </td>
          </tr>
          <tr>
            <td align="right">Apellidos</td>
            <td>
              <input type="text" name="apellidos" value="<?php echo $usuario->getApellidos();?>" />           </td>
          </tr>
          <tr>
            <td align="right">Fecha de Nacimiento</td>
            <td><input type="text" name="nacimiento" value="<?php echo $usuario->getNacimiento();?>" /></td>
          </tr>
          <tr>
            <td align="right">Correo electrónico</td>
            <td>
                <input type="text" readonly name="email" value="<?php echo $usuario->getLogin();?>" />            </td>
          </tr>
          <tr>
            <td align="right">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="right"><label for="dir">Dirección:</label></td>
            <td><input name="dir" type="text" id="dir" value="<?php echo $usuario->getDir();?>" /></td>
          </tr>
          <tr>
            <td align="right"><label for="ciudad">Ciudad:</label></td>
            <td><input name="ciudad" type="text"  id="ciudad" value="<?php echo $usuario->getCiudad();?>"/></td>
          </tr>
          <tr>
            <td align="right"><label for="depto-region">Depto / Región:</label></td>
            <td><input name="depto" type="text" id="depto-region" value="<?php echo $usuario->getDepto();?>" /></td>
          </tr>
          <tr>
            <td align="right"><label for="pais">País</label></td>
            <td><input name="pais" type="text" id="pais" value="<?php echo $usuario->getPais();?>"/></td>
          </tr>
          <tr>
            <td align="right"><label for="tel">Teléfono:</label></td>
            <td><input name="tel" type="text" id="tel" value="<?php echo $usuario->getTel();?>" /></td>
          </tr>
          <tr>
            <td align="right"><label for="fax">FAX:</label></td>
            <td><input name="fax" type="text" id="fax" value="<?php echo $usuario->getFax();?>"  /></td>
          </tr>
          <tr>
            <td align="right"></td>
            <td style="padding:5px 0px 0px 5px;">
                <?php if( isset($_GET['okEdit']) ){ ?>
                <span style="color: green">Datos guardada</span>
                <?php } ?>
            </td>
          </tr>
          <tr>
            <td align="right"></td>
            <td style="padding:5px 0px 0px 5px;"><span style="padding:5px 0px 0px 5px;">
              <input name="recibir" type="checkbox" id="deseo" <?php if($usuario->getRecibir()) echo 'checked';?> />
            </span><label for="deseo">deseo recibir a mi mail las promociones que la era azul tiene para mi</label></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><label for="button2"></label>
              <input name="button2" type="submit" id="button2" value="Guardar cambios" /></td>
          </tr>
        </tbody></table>
	</form>
                 <br />

                    <h1>Cambiar Contraseña</h1>
                    <form action="php/action/cambiarPass.php" method="post" class="sexyform">
                        <table class="fondoTabla" width="100%" border="0" cellspacing="0" cellpadding="0" >
          <tbody>
          <tr>
              <td align="right" width="25%" >Contraseña Anterior</td>
            <td width="75%" >
                <input type="password" name="passOld" >            </td>
          </tr>
              <tr>
            <td align="right">Cambiar Nueva</td>
            <td>
                    <input type="password" name="pass1" >            </td>
          </tr>
              <tr>
            <td align="right">Confirmar contraseña</td>
            <td>
                    <input type="password" name="pass2" >            </td>
          </tr>

          <tr>
            <td align="right"></td>
            <td style="padding:5px 0px 0px 5px;">&nbsp;
                <?php if( isset($_GET['oldPass']) ){ ?>
                <span style="color: red">Contraseña anterior erronea</span>
                <?php } ?>
                <?php if( isset($_GET['missPass']) ){ ?>
                <span style="color: red">Contraseñas nuevas diferentes</span>
                <?php } ?>
                <?php if( isset($_GET['okPass']) ){ ?>
                <span style="color: green">Nueva contraseña guardada</span>
                <?php } ?>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><label for="button2"></label>
              <input name="button2" type="submit" id="button2" value="Cambiar" /></td>
          </tr>
        </tbody></table>
	</form>
                </div>
                </div>
              </div>
            </div>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
    <a name="pass"></a>
<?php include("includes/footer.php"); ?>
</div>




<!--Carrito -->
<?php include("includes/mcart.php"); ?>



<!--Registro o login -->
<!--Funtions -->
<script type="text/javascript" src="js/jquery.hoverIntent.minified.js"></script> 
<script type="text/javascript" src="js/funtionm.js"></script> 
<script> 
$(document).ready(function() { 
	var triggers = $(".modalInput").overlay({ 
		mask: {
			color: '#ebecff',
			loadSpeed: 200,
			opacity: 0.9
		}
	});
});
</script>
</body>
</html>