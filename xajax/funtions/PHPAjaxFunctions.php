<?php


function addToCart($idProducto, $cantidad, $idNotification){

    $response = new xajaxResponse();


    if( $cantidad == 0  || $cantidad == "" ){
        $response->alert('La cantidad debe ser superior a 1');
        return $response;
    }

    if( !isset($_SESSION['carrito']) ){
        $carrito = array();
        $_SESSION['carrito'] = serialize($carrito);
    }

   
    $carrito = unserialize($_SESSION['carrito']);


    $productDAO = new productoDAO();
    //compruebo que esté disponible
    $productoSelected = $productDAO->getById($idProducto);

    if($productoSelected == null){
        $response->alert('producto no encontrado');
        return $response;
    }

    if($productoSelected->getStock() <= 0){
        $response->alert('Producto agotado');
        return $response;
    }


    //compruebo si ya esta ese producto, si esta lo tomo
    if( isset($carrito[$idProducto]) ){
        $productoToAdd = $carrito[$idProducto];
    }else{
        //si no esta lo creo
        $productoToAdd = $productDAO->getById($idProducto);
        $productoToAdd->setCantidad(0);
    }

    

    //cantidad es la actual mas la de ahora
    $productoToAdd->setCantidad($cantidad + $productoToAdd->getCantidad());

    //Lo pongo en el array nuevamente
    $carrito[$idProducto] = $productoToAdd;

    $total = 0;
    foreach ($carrito as $producto){
        $lineaValor = $producto->getCantidad()*$producto->getPrecio();
        $total += $lineaValor;
    }

    $_SESSION['carrito'] = serialize($carrito);
    $_SESSION['total'] = serialize($total);

    //update mcart.php file
    ob_start();
    include 'includes/mcart2.php';
    $answer = ob_get_clean();
    $response->assign("modalcart", "innerHTML", $answer);



    $response->alert('Producto añadido a tu carrito de compras');
    $response->assign("totalItems", "innerHTML", count($carrito));
    $response->assign("subtotal", "innerHTML", number_format($total,  0, ',', '.'));
    $response->assign($idNotification, "innerHTML", "Producto añadido");

    return $response;
}


/*
 * in this case, $idProduct is the $id of that product in the cart ($id).
 */
function updateCart($idProducto, $cantidad){

    $response = new xajaxResponse();


    if( $cantidad == 0  || $cantidad == "" || !is_numeric($cantidad) ){
        $response->alert('La cantidad debe ser por lo menos 1');
        return $response;
    }


    if( !isset($_SESSION['carrito']) ){
        $carrito = array();
        $_SESSION['carrito'] = serialize($carrito);
    }


    $carrito = unserialize($_SESSION['carrito']);


    if(isset($carrito[$idProducto])){
        $carrito[$idProducto]->setCantidad($cantidad);
        $response->assign("itemPrice".$idProducto, "innerHTML", "$".number_format(($carrito[$idProducto]->getCantidad()*$carrito[$idProducto]->getPrecio()),  2, ',', '.') );
    }

    $total = 0;
    foreach ($carrito as $producto){
        $lineaValor = $producto->getCantidad()*$producto->getPrecio();
        $total += $lineaValor;
    }

    $_SESSION['carrito'] = serialize($carrito);
    $_SESSION['total'] = serialize($total);

    $response->alert('Product updated');
    $response->assign("totalItems", "innerHTML", count($carrito));
    $response->assign("subtotal", "innerHTML", number_format($total,  2, ',', '.'));
    $response->assign("cartSubtotal", "innerHTML", number_format($total,  2, ',', '.'));
    return $response;
}

function stockMessage($form_entrada){

   $respuesta = new xajaxResponse();


    $nombre = $form_entrada['nombre'];
    $email  = $form_entrada['email'];
    $ciudad  = $form_entrada['ciudad'];
    $tel  = $form_entrada['tel'];
    $corta  = $form_entrada['corta'];
    $id = $form_entrada['id'];


    if($email == "" || $nombre == "" || $corta == ""){
        $respuesta->alert('Introduce por lo menos tu email, nombre y comentario');
        return $respuesta;
    }

    if(!ValidaMail($email)){
        $respuesta->alert('Email invalido');
        return $respuesta;
    }

    $productoDAO = new productoDAO();
    $producto = $productoDAO->getById($id);

    if($producto == null){
        $respuesta->alert('Gracias por tu mensaje...');
        return $respuesta;
    }

    $aspectoDAO = new AspectsDAO();
    $email2 = $aspectoDAO->getAspect('email');

    $body = "<h3>Notificaci&oacute;n producto agotado LA ERA AZUL  </h3>\n\n";
    $body .= "Un usuario envia este mensaje referido a un producto que est&aacute; agotado.<br />\n";
    $body .= "<b>Producto:</b> ".$producto->getNombre()."<br />\n";
    $body .= "<b>Referencia:</b> ".$producto->getRef()."<br />\n";
    $body .= "<b>Nombre completo:</b> ".text2HTML($nombre)."<br />\n";
    $body .= "<b>Correo Electr&oacute;nico:</b> ".text2HTML($email)."<br />\n";
    $body .= "<b>Ciudad:</b> ".text2HTML($ciudad)."<br />\n";
    $body .= "<b>Tel&eacute;fono:</b>".$tel."<br />\n";
    $body .= "<b>Comentario:</b> <br />".text2HTML(nl2br($corta))."<br />\n";


    $mail2 = new sendCMail($email2->getValue(), "info@laerazul.com.co", "zona de contacto", $body, "text/html");
    $mail2->sendMail();
    

    $respuesta->alert('Gracias por tu mensaje');
    $respuesta->assign("noti", "innerHTML", "Gracias por tu mensaje");

   //tenemos que devolver la instanciación del objeto xajaxResponse
   return $respuesta;
}



function actualizaCiudad($departamentoID, $tagId){


    $response = new xajaxResponse();
    
    
    
    $daoConnection = new DAO;
    $daoConnection->conectar();

    

    $sql = "SELECT municipio_municipio FROM municipios WHERE municipio_departamento = '".$departamentoID."' ORDER BY municipio_municipio asc";
    $query = $daoConnection->consulta($sql);



    $selectInit = '<select name="ciudad" style="width: 100px">';
    $selectEnd = '</select>';


    $option = '<option value="0">Seleccionar Ciudad</option>';
    while ($row = mysql_fetch_array($query)) {
        $option .= '<option class="sexy-select-center">'.  text2HTML(utf8_encode($row['municipio_municipio'])).'</option>';
    }



    $response->assign($tagId,"innerHTML",$selectInit.$option.$selectEnd);

    return $response;
    
}
?>