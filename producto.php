<?php
include 'includes.php';
$id = $_GET['id'];
$productoDAO = new productoDAO();
$productoS = $productoDAO->getById($id);
if($productoS == null){
    $productoS = new producto();
    $productoS->setNombre('No encontrado');
}

$prodRelDAO = new proRelDAO();
$cat1DAO = new cat1DAO();
$cat2DAO = new cat2DAO();

$cat1S =$cat1DAO->getById($productoS->getIdCat1());
if($cat1S == null){
    $cat1S = new cat1();
    $cat1S->setId(0);
}
$cat2S = $cat2DAO->getById($productoS->getIdCat2());
if($cat2S == null){
    $cat2S = new cat1();
    $cat2S->setId(0);
}


$p1 = 'producto.php?id='.$id;
$p2 = 'producto.php?id='.$id;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>La Era Azul - Libros y Accesorios</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<link type="image/x-icon" href="era_azul.ico" rel="shortcut icon" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/large.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/mootools-core-1.3.1-full-compat-yc.js"></script>
<script type="text/javascript" src="js/sexyforms.v1.3.mootools.min.js"></script>
<script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link   type="text/css" rel="stylesheet" href="sexyforms/gamma/sexyforms-gamma.css"  media="all" id="theme" />
<script type="text/javascript">
window.addEvent('domready', function() {
  $$(".sexyform input", ".sexyform select", ".sexyform textarea").each(function(el) {
    el.DoSexy();
  });
});
</script>

<script src="js/jquery.tools.min.js"></script>
<style type="text/css">
<!--

-->
</style>
<?php
    //Xajax
    $xajax->printJavascript("xajax/");
?>
<script type="text/javascript" src="js/jquery.history.js"></script>
<script type="text/javascript" src="js/jquery.galleriffic.js"></script>
<script type="text/javascript" src="js/jquery.opacityrollover.js"></script>
<!-- We only want the thunbnails to display when javascript is disabled -->
<script type="text/javascript">
	document.write('<style>.noscript { display: none; }</style>');
</script>
<link rel="stylesheet" href="css/galleriffic-5.css" type="text/css" />
<link rel="stylesheet" href="css/white.css" type="text/css" />
</head>
<body> 
<div id="header">
  <div class="gutter">
    <div class="inner">
      <div id="htop">
        <?php include("includes/bar.php"); ?>
        <?php include("includes/logo.php"); ?>
      </div>
      <?php include("includes/menu.php"); ?>
    </div>
  </div>
</div>
<div id="wrapper">
  <div id="wrapperbg">
    <div class="inner clearfix">
      <div id="main">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="215" valign="top">
              <?php include("includes/tematico.php"); ?>
              <?php include("includes/publicidad.php"); ?></td>
            <td valign="top">
            <div id="m_tot">
              <div id="contenido">
                <div class="content">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td colspan="2">
                      <div id="categorias">
                        <h6>Categoría:
                            <a href="productos.php?param=idCat1&value=<?php echo $cat1S->getId();?>"><?php echo $cat1S->getNombre();?></a>
                        </h6>
                        <h6>Sub-Categoría:
                            <a href="productos.php?param=idCat2&value=<?php echo $cat2S->getId();?>"><?php echo $cat2S->getNombre();?></a>
                            <?php
                                for($i=1;$i<=5;$i++){
                                    if($productoS->{"getIdCat1".$i}() != 0 ){
                                        $cat2E = $cat2DAO->getById($productoS->{"getIdCat1".$i}());
                                        if($cat2E != null){
                            ?>
                            / <a href="productos.php?param=idCat2&value=<?php echo $cat2E->getId();?>"><?php echo $cat2E->getNombre();?></a>
                            <?php
                                        }
                                    }
                                }
                            ?>
                        </h6>
                      </div>
                    </td>
                    </tr>
                  <tr>
                    <td width="18%" valign="top">
                        <div><a class="btrig modalInput" href="producto.php?id=<?php echo $productoS->getId();?>" rel="#modalgallery"><img src="imagenes/productos/<?php echo $productoS->getImg(); ?>" width="113"/></a></div>
                        <span class="imapro_buy" id="noti<?php echo $productoS->getId();?>">
                        <?php if($productoS->getStock() < 1){  ?>
                        <a class="btrig modalInput" href="#" rel="#modal_ups">
                        <?php }else{?>
                        <a href="#" onclick="xajax_addToCart('<?php echo $productoS->getId();?>','1','noti<?php echo $productoS->getId();?>');return false;">
                        <?php }?>
                            <img src="img/bt_buy.png" border="0" />
                        </a>
                    </span>
                    </td>
                    <td width="82%" valign="top">
                      <div id="tabladet">
                          <p>
                              <?php echo nl2br($productoS->getDatosTecnicos()); ?>
                          </p>
                      </div>
                      <h3>Título: <?php echo $productoS->getNombre(); ?></h3>
                      <p><?php echo $productoS->getDatos(); ?></p>
                      <?php if($productoS->getDescuento()){  ?>
                      <p>Precio Anterior: <span class="pri">$<strike><?php echo $productoS->getPrecioFormatoDescuento(); ?></strike></span></p>
                      <?php } ?>
                      <p>Precio: <span class="pri">$<?php echo $productoS->getPrecioFormato(); ?></span></p>
                      <p><!-- AddThis Button BEGIN -->
                        <div class="addthis_toolbox addthis_default_style ">
                        <a href="http://www.addthis.com/bookmark.php?v=250&amp;pubid=xa-4d97f04870073c4c" class="addthis_button_compact">Compartir</a>
                        <span class="addthis_separator">|</span>
                        <a class="addthis_button_preferred_1"></a>
                        <a class="addthis_button_preferred_2"></a>
                        <a class="addthis_button_preferred_3"></a>
                        <a class="addthis_button_preferred_4"></a>                        </div>
                        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4d97f04870073c4c"></script>
                        <!-- AddThis Button END --></p>
                      <p><script src="http://connect.facebook.net/es_LA/all.js#xfbml=1"></script><fb:like href="" show_faces="false" width="450" font="arial"></fb:like></p></td>
                  </tr>
                  <tr>
                    <td colspan="2" valign="top">
                    <div class="resena">
                  <?php echo $productoS->getCorta(); ?>
                </div></td>
                    </tr>
                </table>
                </div>
              </div>
            </div>
            <div id="m_tot">
              <div id="contenido">
                <div class="content" style="height:350px;">
                <h1>Productos Relacionados</h1>
                <div class="itemsrec">
                    <div class="recom">
                     <?php
                        $idRels = $prodRelDAO->getsByIdPro("id", "asc", $productoS->getId());
                        foreach ($idRels as $idRel){
                        $producto = $productoDAO->getById($idRel->getIdProductoRel());
                        if($producto == null)
                            continue;
                    ?>
                     <div class="imagor">
                         <span class="imagor_ima"><a href="producto.php?id=<?php echo $producto->getId();?>"><img src="imagenes/productos/<?php echo $producto->getImg();?>" width="113" border="0"/></a></span>
                         <span class="imagor_tit"><?php echo $producto->getNombre();?></span>
                         <?php if($producto->getDescuento()){  ?>
                         <span class="imagor_pri" style="font-size: small;">$<strike><?php echo $productoS->getPrecioFormatoDescuento(); ?></strike></span>
                        <?php } ?>
                         <span class="imagor_pri">$<?php echo $producto->getPrecioFormato();?></span>
                         <span class="imagor_buy" id="notiR<?php echo $producto->getId();?>">
                             <a href="producto.php?id=<?php echo $producto->getId();?>" onclick="xajax_addToCart('<?php echo $producto->getId();?>','1','notiR<?php echo $producto->getId();?>');return false;">
                                 <img src="img/bt_buy.png" border="0" />
                             </a>
                         </span>
                     </div>
                     <?php } ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
              </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include("includes/footer.php"); ?>
</div>

<!--Carrito -->
<?php include("includes/mcart.php"); ?>

<!--Registro o login -->
<?php include("includes/relog.php"); ?>

<!--Gallery -->
<?php include("includes/gallery.php"); ?>

<!--Caso: Producto agotado -->
<?php include("includes/out_of_stock.php"); ?>

<!--Funtions -->
<script type="text/javascript" src="js/jquery.hoverIntent.minified.js"></script> 
<script type="text/javascript" src="js/funtionm.js"></script> 

<script> 
$(document).ready(function() { 
	var triggers = $(".modalInput").overlay({ 
		mask: {
			color: '#ebecff',
			loadSpeed: 200,
			opacity: 0.9
		}
	});
});
</script>
<script type="text/javascript">
	$(document).ready(function($) {
		// We only want these styles applied when javascript is enabled
		$('div.content').css('display', 'block');
		// Initially set opacity on thumbs and add
		// additional styling for hover effect on thumbs
		var onMouseOutOpacity = 0.67;
		$('#thumbs ul.thumbs li, div.navigation a.pageLink').opacityrollover({
			mouseOutOpacity:   onMouseOutOpacity,
			mouseOverOpacity:  1.0,
			fadeSpeed:         'fast',
			exemptionSelector: '.selected'
		});

		// Initialize Advanced Galleriffic Gallery

		var gallery = $('#thumbs').galleriffic({

			delay:                     2500,

			numThumbs:                 10,

			preloadAhead:              10,

			enableTopPager:            false,

			enableBottomPager:         false,

			imageContainerSel:         '#slideshow',

			controlsContainerSel:      '#controls',

			captionContainerSel:       '#caption',

			loadingContainerSel:       '#loading',

			renderSSControls:          true,

			renderNavControls:         true,

			playLinkText:              'Correr presentación',

			pauseLinkText:             'Pausar presentación',

			prevLinkText:              '&lsaquo; Anterior foto',

			nextLinkText:              'Siguiente foto &rsaquo;',

			nextPageLinkText:          'Siguiente &rsaquo;',

			prevPageLinkText:          '&lsaquo; Anterior',

			enableHistory:             true,

			autoStart:                 false,

			syncTransitions:           true,

			defaultTransitionDuration: 900,

			onSlideChange:             function(prevIndex, nextIndex) {

				// 'this' refers to the gallery, which is an extension of $('#thumbs')

				this.find('ul.thumbs').children()

					.eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()

					.eq(nextIndex).fadeTo('fast', 1.0);



				// Update the photo index display

				this.$captionContainer.find('div.photo-index')

					.html('Photo '+ (nextIndex+1) +' of '+ this.data.length);

			},

			onPageTransitionOut:       function(callback) {

				this.fadeTo('fast', 0.0, callback);

			},

			onPageTransitionIn:        function() {

				var prevPageLink = this.find('a.prev').css('visibility', 'hidden');

				var nextPageLink = this.find('a.next').css('visibility', 'hidden');

				

				// Show appropriate next / prev page links

				if (this.displayedPage > 0)

					prevPageLink.css('visibility', 'visible');



				var lastPage = this.getNumPages() - 1;

				if (this.displayedPage < lastPage)

					nextPageLink.css('visibility', 'visible');



				this.fadeTo('fast', 1.0);

			}

		});



		/**************** Event handlers for custom next / prev page links **********************/



		gallery.find('a.prev').click(function(e) {

			gallery.previousPage();

			e.preventDefault();

		});



		gallery.find('a.next').click(function(e) {

			gallery.nextPage();

			e.preventDefault();

		});



		/****************************************************************************************/



		/**** Functions to support integration of galleriffic with the jquery.history plugin ****/



		// PageLoad function

		// This function is called when:

		// 1. after calling $.historyInit();

		// 2. after calling $.historyLoad();

		// 3. after pushing "Go Back" button of a browser

		function pageload(hash) {

			// alert("pageload: " + hash);

			// hash doesn't contain the first # character.

			if(hash) {

				$.galleriffic.gotoImage(hash);

			} else {

				gallery.gotoIndex(0);

			}

		}



		// Initialize history plugin.

		// The callback is called at once by present location.hash. 

		$.historyInit(pageload, "advanced.html");



		// set onlick event for buttons using the jQuery 1.3 live method

		$("a[rel='history']").live('click', function(e) {

			if (e.button != 0) return true;



			var hash = this.href;

			hash = hash.replace(/^.*#/, '');



			// moves to a new page. 

			// pageload is called at once. 

			// hash don't contain "#", "?"

			$.historyLoad(hash);
			return false;
		});
		/****************************************************************************************/
	});
</script>
</body>
</html>