<?php
include 'includes.php';
$bannerTOPDAO = new bannerDAO();
$bannerLAT = $bannerTOPDAO->gets("id", "asc");

$productoDAO = new productoDAO();
$productosReg = $productoDAO->search("id", "desc", 0, 40, "regalar = 1");
$productosNov = $productoDAO->search("id", "desc", 0, 40, "novedad = 1");

$productosScroll1 = $productoDAO->search("id", "desc", 0, 100, "vendido = 1");
$productosScroll2 = $productoDAO->search("id", "desc", 0, 100, "recomendado = 1");
$productosScroll3 = $productoDAO->search("id", "desc", 0, 100, "popular = 1");

$mensajeDAO = new mensajeDAO();
$mensajes = $mensajeDAO->gets("id", "desc");

$p1 = 'index.php?';
$p2 = 'index.php?';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>La Era Azul - Libros y Accesorios</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<link type="image/x-icon" href="era_azul.ico" rel="shortcut icon" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/large.css" rel="stylesheet" type="text/css" />
<?php
    //Xajax
    $xajax->printJavascript("xajax/");
?>
<script type="text/javascript">
function changeValue(id, value){
   document.getElementById(id).value = value;
}
</script>
<script type="text/javascript" src="js/mootools-core-1.3.1-full-compat-yc.js"></script>
<script type="text/javascript" src="js/sexyforms.v1.3.mootools.min.js"></script>
<link   type="text/css" rel="stylesheet" href="sexyforms/gamma/sexyforms-gamma.css"  media="all" id="theme" />
<script type="text/javascript">
window.addEvent('domready', function() {
  $$(".sexyform input", ".sexyform select", ".sexyform textarea").each(function(el) {
    el.DoSexy();
  });
});
</script>
<script src="js/jquery.tools.min.js"></script>
<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/tabs.css" />
<script type="text/javascript" language="JavaScript" src="js/jquery.carouFredSel-3.2.1.js"></script>
<script type="text/javascript" language="javascript">
$(function() {
	$('#novelties').carouFredSel({
		items: 'variable',
		next: '#next1',
		prev: '#prev1',
		scroll: {
			duration        : 5000,
			pauseDuration   : 10000,
			pauseOnHover    : true
		}
	});
	$('#tab100').carouFredSel({
		items: 'variable',
		next: '#next100',
		prev: '#prev100',
		scroll: {
			duration        : 1000,
			pauseDuration   : 1000000,
			pauseOnHover    : true
		}
	});
	$('#tab200').carouFredSel({
		items: 'variable',
		next: '#next200',
		prev: '#prev200',
		scroll: {
			duration        : 1000,
			pauseDuration   : 1000000,
			pauseOnHover    : true
		}
	});
	$('#tab300').carouFredSel({
		items: 'variable',
		next: '#next300',
		prev: '#prev300',
		scroll: {
			duration        : 1000,
			pauseDuration   : 1000000,
			pauseOnHover    : true
		}
	});
	$('#regalos').carouFredSel({
		items : {
		visible: 3,
		},
		scroll: {
			items           : 1,
			duration        : 5000,
			pauseDuration   : 10000,
			pauseOnHover    : true,
			onBefore		: function(oldItems, newItems, newSizes, duration) {
				$("#regalos").parent().parent().animate({ paddingTop: (680 - newSizes.height) / 2 }, duration);
			}
		},
		next: '#next2',
		prev: '#prev2',
		direction: 'up'
		}).parent().parent().css({
	paddingTop: (680 - $("#regalos").parent().outerHeight()) / 2
	});
});
</script>
<link type="text/css" href="css/flick/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript">
	$(function(){
		// Tabs
		$('#tabs').tabs();
	});
</script>
</head>
<body>
<div id="header">
  <div class="gutter">
    <div class="inner">
      <div id="htop">
        <?php include("includes/bar.php"); ?>
        <?php include("includes/logo.php"); ?>
      </div>
      <?php include("includes/menu.php"); ?>
    </div>
  </div>
</div>
<div id="wrapper">
  <div id="wrapperbg">
    <div class="inner clearfix">
      <div id="mainhm">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="2" valign="top"><div id="m_top">
                <div class="ban1" style="display:nonex">
                  <div id="baner">
                    <div class="balt"></div>
                    <div class="bart"></div>
                    <div class="balb"></div>
                    <div class="barb"></div>
                    <div id="slider" class="nivoSlider">
                        <?php foreach($bannerLAT as $banner){
                            if( !is_numeric($banner->getUrl()) )
                                    $url = 'http://'.$banner->getUrl();
                                else{
                                    $proyecto = $productoDAO->getById($banner->getUrl());
                                    if($proyecto != null)
                                        $url = 'producto.php?id='.$proyecto->getId();
                                    else
                                        $url = '#';
                                }
                        ?>
                        <a href="<?php echo $url;?>"><img src="imagenes/banners/<?php echo $banner->getImg();?>" alt="" title="<?php echo $banner->getNombre();?>" /></a>
                      <?php } ?>

                    </div>
                    </div>
                  </div>
              </div></td>
            <td width="208" rowspan="2" valign="top">
              <div id="m_rig">
              <div id="msg">
                  <div class="mod_tit"><a href="cita.php"><img src="img/cita.png" /></a></div>
                <div class="msg_cont"><?php foreach ($mensajes as $mensaje){ echo $mensaje->getCortaShortNoTags(200); break;}?>
                  <div align="right" style="margin-top: 10px; font-size:11px; background: url(img/arrow_greytransparent.gif) no-repeat 145px 50%" ><a href="cita.php">Ver más</a></div>
                </div>
              </div>
                <div class="mod_tit"><img src="img/reg.png" /></div>
              <div id="gral">
                  <a id="next2" href="#"><img src="img/scro_r.png" /></a>
                  <a id="prev2" href="#"><img src="img/scro_l.png" /></a>
                <div class="list_vcarousel">
                    <div style="float: left;margin-left: 15px; font-size:12px;">
                      <a href="productos2.php?param=regalar&value=1">
                           &gt; Ver más
                      </a>
                  </div>
                    <br />
                  <ul id="regalos">
                    <?php foreach($productosReg as $producto){ $idtag = $producto->getId().'REG'; ?>
                    <li><a class="btrig modalInput" href="#" rel="#<?php echo $idtag;?>"><img src="imagenes/productos/<?php echo $producto->getImg();?>" width="112" /></a></li>
                    <?php } ?>
                  </ul>
                </div>
              </div>
            </div></td>
          </tr>
          <tr>
            <td width="215" valign="top">
              <?php include("includes/publicidad.php"); ?></td>
            <td valign="top">
              <div id="m_cen">
              <div class="ban2" style="display:nonex">
              <div id="novelties_tit">
                  <span>Novedades:</span> &gt;
                  <a href="productos2.php?param=novedad&value=1">
                      Ver todas
                  </a>
              </div>
              <a id="next1" href="#"><img src="img/scro_r.png" /></a>
              <a id="prev1" href="#"><img src="img/scro_l.png" /></a>
              <div class="list_carousel">
                    <ul id="novelties">
                        <?php foreach($productosNov as $producto){ $idtag = $producto->getId().'NOV';?>
                        <li><a class="btrig modalInput" href="#" rel="#<?php echo $idtag;?>"><img src="imagenes/productos/<?php echo $producto->getImg();?>" width="113" border="0" /></a></li>
                        <?php } ?>
                    </ul>
                    <div class="clearfix"></div>
                    </div>
              </div>
              <div class="ban3">
              <div id="tabs">
			<ul>
				<li><a href="#tabs-1">LOS MÁS VENDIDOS</a></li>
				<li><a href="#tabs-2">LOS MÁS RECOMENDADOS</a></li>
				<li><a href="#tabs-3">LOS MÁS POPULARES</a></li>
			</ul>
			<div id="tabs-1">
            <div class="ban100">
              <a id="next100" href="#">&nbsp;</a>
              <a id="prev100" href="#">&nbsp;</a>
              <div class="list_carouseltab">
                  <div style="float: right;">
                      <a href="productos2.php?param=vendido&value=1">
                           &gt; Ver más
                      </a>
                  </div>
                    <ul id="tab100">
                        <?php foreach($productosScroll1 as $producto){ ?>
                        <li>
                         <span class="imago_ima">
                             <a class="btrig modalInput" href="#" rel="#modalpop">
                                 <a href="producto.php?id=<?php echo $producto->getId();?>">
                                     <img src="imagenes/productos/<?php echo $producto->getImg();?>" width="113" border="0"/>
                                 </a>
                             </a>
                         </span>
                         <span class="imago_tit"><?php echo $producto->getNombre();?></span>
                         <span class="imago_pri">$<?php echo $producto->getPrecioFormato(); ?></span>
                         <span class="imago_buy" id="scroll1<?php echo $producto->getId();?>">
                             <a href="#" onclick="top.xajax_addToCart('<?php echo $producto->getId();?>','1','scroll1<?php echo $producto->getId();?>');return false;">
                                 <img src="img/bt_buy.png" border="0" alt="" />
                             </a>
                         </span>
                        </li>
                        <?php  } ?>
                    </ul>
                    <div class="clearfix"></div>
                    </div>
              </div>
            </div>
			<div id="tabs-2">
            <div class="ban200">
              <a id="next200" href="#">&nbsp;</a>
              <a id="prev200" href="#">&nbsp;</a>
              <div class="list_carouseltab">
                  <div style="float: right;">
                      <a href="productos2.php?param=recomendado&value=1">
                           &gt; Ver más
                      </a>
                  </div>
                    <ul id="tab200">
                        <?php foreach($productosScroll2 as $producto){ ?>
                        <li>
                         <span class="imago_ima">
                             <a class="btrig modalInput" href="#" rel="#modalpop">
                                 <a href="producto.php?id=<?php echo $producto->getId();?>">
                                     <img src="imagenes/productos/<?php echo $producto->getImg();?>" width="113" border="0"/>
                                 </a>
                             </a>
                         </span>
                         <span class="imago_tit"><?php echo $producto->getNombre();?></span>
                         <span class="imago_pri">$<?php echo $producto->getPrecioFormato(); ?></span>
                         <span class="imago_buy" id="scroll2<?php echo $producto->getId();?>">
                             <a href="#" onclick="top.xajax_addToCart('<?php echo $producto->getId();?>','1','scroll2<?php echo $producto->getId();?>');return false;">
                                 <img src="img/bt_buy.png" border="0" alt="" />
                             </a>
                         </span>
                        </li>
                        <?php  } ?>
                    </ul>
                    <div class="clearfix"></div>
                    </div>
              </div>
            </div>
			<div id="tabs-3">
            <div class="ban300">
              <a id="next300" href="#">&nbsp;</a>
              <a id="prev300" href="#">&nbsp;</a>
              <div class="list_carouseltab">
                  <div style="float: right;">
                      <a href="productos2.php?param=popular&value=1">
                           &gt; Ver más
                      </a>
                  </div>
                    <ul id="tab300">
                        <?php foreach($productosScroll3 as $producto){ ?>
                        <li>
                         <span class="imago_ima">
                             <a class="btrig modalInput" href="#" rel="#modalpop">
                                 <a href="producto.php?id=<?php echo $producto->getId();?>">
                                     <img src="imagenes/productos/<?php echo $producto->getImg();?>" width="113" border="0"/>
                                 </a>
                             </a>
                         </span>
                         <span class="imago_tit"><?php echo $producto->getNombre();?></span>
                         <span class="imago_pri">$<?php echo $producto->getPrecioFormato() ?></span>
                         <span class="imago_buy" id="scroll3<?php echo $producto->getId();?>">
                             <a href="#" onclick="top.xajax_addToCart('<?php echo $producto->getId();?>','1','scroll3<?php echo $producto->getId();?>');return false;">
                                 <img src="img/bt_buy.png" border="0" alt="" />
                             </a>
                         </span>
                        </li>
                        <?php  } ?>
                    </ul>
                    <div class="clearfix"></div>
                    </div>
              </div>
            </div>
		</div>


              </div>
              </div></td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include("includes/footer.php"); ?>
</div>


<!--Carrito -->
<?php include("includes/mcart.php"); ?>
<!--Registro o login -->
<?php include("includes/relog.php"); ?>
<!--Pop Producto -->
<?php foreach($productosReg as $producto){ $idtag = $producto->getId().'REG'; ?>
<?php include("includes/productopop.php"); ?>
<?php } ?>
<?php foreach($productosNov as $producto){ $idtag = $producto->getId().'NOV'; ?>
<?php include("includes/productopop.php"); ?>
<?php } ?>
<!--Caso: Producto agotado -->
<?php include("includes/out_of_stock.php"); ?>
<!--Funtions -->
<script type="text/javascript" src="js/jquery.hoverIntent.minified.js"></script>
<script type="text/javascript" src="js/funtionm.js"></script>
<script type="text/javascript" src="js/jquery.nivo.slider.pack.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
    </script>
<script>
$(document).ready(function() {
	var triggers = $(".modalInput").overlay({
		mask: {
			color: '#ebecff',
			loadSpeed: 200,
			opacity: 0.9
		}
	});
});
</script>

</body>
</html>