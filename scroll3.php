<?php
include 'includes.php';
$productoDAO = new productoDAO();
$productos = $productoDAO->search("id", "desc", 0, 100, "popular = 1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>La Era Azul</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<link type="image/x-icon" href="era_azul.ico" rel="shortcut icon" />
<script src="js/jquery.tools.min.js"></script> 
<link rel="stylesheet" type="text/css" href="css/scrollable-horizontal.css" /> 
<link rel="stylesheet" type="text/css" href="css/scrollable-buttons.css" /> 
</head>
<body>
<a class="prev browse left"></a>
<div class="scrollable">
   <div class="items">
      <div>
         <?php $j = 0; foreach($productos as $producto){ ?>
         <div class="imago">
             <span class="imago_ima"><a href="producto.php?id=<?php echo $producto->getId();?>" target="_parent"><img src="imagenes/productos/<?php echo $producto->getImg();?>" width="113" border="0"/></a></span>
             <span class="imago_tit"><?php echo $producto->getNombre();?></span>
             <span class="imago_priold" style="font-size: small;">
                 <?php if($producto->getDescuento()){  ?>
                 $<strike><?php echo $producto->getPrecioFormatoDescuento(); ?></strike>
                 <?php } ?>
             </span>
             <span class="imago_pri">$<?php echo $producto->getPrecioFormato();?></span>
             <span class="imago_buy" id="noti<?php echo $producto->getId();?>">
                 <a href="#" onclick="top.xajax_addToCart('<?php echo $producto->getId();?>','1','noti<?php echo $producto->getId();?>');return false;">
                     <img src="img/bt_buy.png" border="0" alt="" />
                 </a>
             </span>
         </div>
          <?php $j++; if($j%4 == 0) echo '</div><div> '; } ?>
      </div>
   </div>
</div>
<a class="next browse right"></a>
<br clear="all" />
<script> 
// execute your scripts when the DOM is ready. this is mostly a good habit
$(function() { 
	// initialize scrollable
	$(".scrollable").scrollable({circular: true}).autoscroll({ autoplay: false, interval: 10000 }); 
});
</script> 
</body>
</html>