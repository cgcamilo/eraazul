<?php
include 'includes.php';
unset($_SESSION['carrito']);
unset($_SESSION['total']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>La Era Azul - Libros y Accesorios</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<?php
    //Xajax
    $xajax->printJavascript("xajax/");
?>
<link type="image/x-icon" href="era_azul.ico" rel="shortcut icon" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/mootools-core-1.3.1-full-compat-yc.js"></script>
<script type="text/javascript" src="js/sexyforms.v1.3.mootools.min.js"></script>
<script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link   type="text/css" rel="stylesheet" href="sexyforms/gamma/sexyforms-gamma.css"  media="all" id="theme" />
<script type="text/javascript">
window.addEvent('domready', function() {
  $$(".sexyform input", ".sexyform select", ".sexyform textarea").each(function(el) {
    el.DoSexy();
  });
});
</script>
<script src="js/jquery.tools.min.js"></script>
<style type="text/css">
<!--
#wrapper #contenido { margin:100px; padding: 70px;}
#wrapper #contenido h1 { font-size: 66px; background:url(img/003.jpg) top right no-repeat; line-height:129px; border-radius: 10px; -moz-border-radius: 10px; -webkit-border-radius: 10px; padding-bottom: 0px; border: 1px solid #ccc; padding-left:20px}
-->
</style>
</head>
<body> 
<div id="header">
  <div class="gutter">
    <div class="inner">
      <div id="htop">
        <?php include("includes/bar.php"); ?>
        <?php include("includes/logo.php"); ?>
      </div>
      <?php include("includes/menu.php"); ?>
    </div>
  </div>
</div>
<div id="wrapper">
  <div id="wrapperbg">
    <div class="inner clearfix">
      <div id="main">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="215" valign="top">
              <!--php publicidad --></td>
            <td valign="top"><div id="m_tot">
              <div id="contenido">
                <div class="content">
                    <h1>Gracias</h1>
                    <br />
                    Tú pedido está siendo procesado por el sistema.
                </div></div>
            </div>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<!--php footer -->
<?php include("includes/footer.php"); ?>
</div>








<!--Registro o login -->
<?php include("includes/relog.php"); ?>
<!--Funtions -->
<script type="text/javascript" src="js/jquery.hoverIntent.minified.js"></script> 
<script type="text/javascript" src="js/funtionm.js"></script> 
<script> 
$(document).ready(function() { 
	var triggers = $(".modalInput").overlay({ 
		mask: {
			color: '#ebecff',
			loadSpeed: 200,
			opacity: 0.9
		}
	});
});
</script>
<script>  
$(function() { 
	// if the function argument is given to overlay,
	// it is assumed to be the onBeforeLoad event listener
	$(".ajax_prog").overlay({ 
		mask: {
			color: '#ebecff',
			loadSpeed: 200,
			opacity: 0.9
		},
		onBeforeLoad: function() { 
			// grab wrapper element inside content
			var wrap = this.getOverlay().find(".mod_ajax_cart"); 
			// load the page specified in the trigger
			wrap.load(this.getTrigger().attr("href"));
		} 
	});
});
</script>
<!--Load Ajax -->
<div class="modal_ajax" id="overlay"> 
	<!-- the external content is loaded inside this tag --> 
	<div class="mod_ajax_cart"></div> 
</div>
</body>
</html>