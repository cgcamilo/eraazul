<?php
include 'includes.php';
$fecha = $_GET['fecha'];
$mensajeDAO = new mensajeDAO();
$mensajes = $mensajeDAO->gets("id", "desc", 0, 15);

if( isset($_GET['id']) ){
    $mensaje = $mensajeDAO->getById($_GET['id']);
}else{
    if(!isset($_GET['fecha']))
        $mensaje = $mensajes[0];
    else{
       $mensajes  = $mensajeDAO->getsFecha ("id", "desc", $_GET['fecha'], $_GET['fecha']);
       if( count($mensajes) > 0)
        $mensaje = $mensajes[0];
       else{
           $mensaje = new mensaje();
           $mensaje->setCorta('no hay mensajes en esta fecha');
       }
    }
}
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="mensajeActual">
  <tr>
    <td><acronym class="publishedblu" title="<?php echo $mensaje->getFecha();?>">
<span class="pub-fechablu"><?php echo $mensaje->getDia();?></span>
<span class="pub-monthblu"><?php echo $mensaje->getMes();?></span>
  </acronym>
  <h1>Mensaje del día</h1>
  <p><?php echo $mensaje->getCorta();?></p></td>
      <td valign="top">
          <?php if($mensaje->getImg() != ""){ ?>
          <img style="margin-left:15px; margin-top: 45px;" src="imagenes/mensajes/<?php echo $mensaje->getImg();?>" width="150" />
          <?php } ?>
      </td>
  </tr>
</table>
