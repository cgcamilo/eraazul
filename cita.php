<?php
include 'includes.php';
$mensajeDAO = new mensajeDAO();

if( !isset($_GET['fecha']) )
    $mensajes = $mensajeDAO->gets("id", "desc", 0, 15);
else
    $mensajes = $mensajeDAO->getsFecha("id", "desc", "2000-01-01", $_GET['fecha']);

if( isset($_GET['id']) )
    $mensaje = $mensajeDAO->getById($_GET['id']);
else{
    $mensaje = $mensajes[0];
}


$mesesMensaje = array();
$mensajesUltimos = $mensajeDAO->gets("id", "desc", 0, 300);
foreach ($mensajesUltimos as $mensajeUltimo){
    $mesesMensaje[$mensajeUltimo->getMesAno()] = $mensajeUltimo->getFechaSQL();
}

$p1 = 'cita.php?';
$p2 = 'cita.php?';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>La Era Azul - Libros y Accesorios</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<link type="image/x-icon" href="era_azul.ico" rel="shortcut icon" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/large.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/mootools-core-1.3.1-full-compat-yc.js"></script>
<script type="text/javascript" src="js/sexyforms.v1.3.mootools.min.js"></script>
<script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link   type="text/css" rel="stylesheet" href="sexyforms/gamma/sexyforms-gamma.css"  media="all" id="theme" />
<script type="text/javascript">
window.addEvent('domready', function() {
  $$(".sexyform input", ".sexyform select", ".sexyform textarea").each(function(el) {
    el.DoSexy();
  });
});
</script>

<script src="js/jquery.tools.min.js"></script>
<style type="text/css">
<!--
#calendar {
	height:200px;
	width:210px;
	float:left;
}
#calroot {
	z-index: 999;	
}
-->
</style>
</head>
<body> 
<div id="header">
  <div class="gutter">
    <div class="inner">
      <div id="htop">
        <?php include("includes/bar.php"); ?>
        <?php include("includes/logo.php"); ?>
      </div>
      <?php include("includes/menu.php"); ?>
    </div>
  </div>
</div>
<div id="wrapper">
  <div id="wrapperbg">
    <div class="inner clearfix">
      <div id="main">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="215" valign="top">
              <div id="contenido" style="margin-top:8px">
                <h2>Historia</h2>
                <ul id="menuhd">
                  <li><a href="cita.php">Último</a></li>
                  <?php foreach($mesesMensaje as $mes => $mesSQL){ ?>
                  <li><a href="cita.php?fecha=<?php echo $mesSQL;?>"><?php echo $mes?></a></li>
                  <?php } ?>
                </ul>
              </div>
              <?php include("includes/publicidad.php"); ?></td>
            <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top">
                <div id="m_cita1">
              <div id="contenido">
                <div class="content">
                  <div>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0" id="mensajeActual">
                      <tr>
                        <td><acronym class="publishedblu" title="<?php echo $mensaje->getFecha();?>">
                    <span class="pub-dateblu"><?php echo $mensaje->getDia();?></span>
                    <span class="pub-monthblu"><?php echo $mensaje->getMes();?></span>
                      </acronym>
                      <h1>Mensaje del día</h1>
                      <div><?php echo $mensaje->getCorta();?></div></td>
                          <td valign="top"><img style="margin-left:15px; margin-top: 45px;" src="imagenes/mensajes/<?php echo $mensaje->getImg();?>" width="150" /></td>
                      </tr>
                    </table>
                  </div>
                  </div>
              </div>
            </div></td>
                <td valign="top">
                <div id="m_cita2">
              <div id="contenido" style="height:262px;">
                <div class="content">
                <div id="calendar"> 
                    <input type="dateinput" name="mydate" value="0" id="datePicker2" min="2011-05-01"/>
                </div> 
                </div>
              </div>
            </div>
                </td>
              </tr>
            </table>

            
            <div id="m_tot">
              <div id="contenido">
                <div class="content">
                <h2>Mensajes pasados</h2>
                <?php foreach ($mensajes as $mensaje){?>
                <div class="citas">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                      <td width="87%"><acronym class="published" title="<?php echo $mensaje->getFecha();?>"> <span class="pub-date"><?php echo $mensaje->getDia();?></span> <span class="pub-month"><?php echo $mensaje->getMes();?></span> </acronym><?php echo $mensaje->getCortaShortNoTags(200);?></td>
                    <td width="13%"><div align="right"><a href="cita.php?id=<?php echo $mensaje->getId();?>">Ver más</a></div></td>
                  </tr>
                </table>
                  </div>
                <?php }?>
                  
                  </div>
              </div>
            </div>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include("includes/footer.php"); ?>
</div>




<!--Carrito -->
<div class="modal" id="modalcart"> 
</div>



<!--Registro o login -->
<?php include("includes/relog.php"); ?>
<!--Funtions -->
<script type="text/javascript" src="js/jquery.hoverIntent.minified.js"></script> 
<script type="text/javascript" src="js/funtionm.js"></script> 
<script> 
	$.tools.dateinput.localize("fr",  {
	   months:        'enero,febrero,marzo,abril,mayo,junio,julio,agosto,' +
						'septiembre,octubre,noviembre,diciembre',
	   shortMonths:   'ene,feb,mar,abr,may,jun,jul,ago,sep,oct,nov,dic',
	   days:          'domingo,lunes,martes,miercoles,jueves,viernes,sabado',
	   shortDays:     'dom,lun,mar,mie,jue,vie,sab'
	});

</script> 
<script> 
$(document).ready(function() { 
	var triggers = $(".modalInput").overlay({ 
		mask: {
			color: '#ebecff',
			loadSpeed: 200,
			opacity: 0.9
		}
	});
});
</script>
<script>
$.tools.dateinput.localize("es",  {
   months:        'enero,febrero,marzo,abril,mayo,junio,julio,agosto,' +
                   	'septiembre,octubre,noviembre,diciembre',
   shortMonths:   'ene,feb,mar,abr,may,jun,jul,ago,sep,oct,nov,dic',
   days:          'domingo,lunes,martes,miércoles,jueves,viernes,sábado',
   shortDays:     'dom,lun,mar,mié,jue,vie,sáb'
});
$(function() {

change = 0;
// initialize dateinput
$('#datePicker2').dateinput( {
		
	// closing is not possible
	onHide: function()  {
		return false; 
	},
	lang: 'es',
	trigger: false,
	selectors: true,
		
	// when date changes update the day display
	change: function(e, date)  {
		$("#datePicker2").html(this.getValue("dd<span>mmmm yyyy</span>"));
                if(change > 1){
                    $("#mensajeActual").load('citaMini.php?fecha='+this.getValue("yyyy-mm-dd"));
                }else{change++;}
                
	}
        
		
// set initial value and show dateinput when page loads	
}).data("dateinput").setValue(0).show();



});
</script> 
</body>
</html>