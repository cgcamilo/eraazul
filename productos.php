<?php
include 'includes.php';

$itemsPerPage = 20;
$orden = isset($_GET['orden']) ? $_GET['orden'] : "nombre";
$page = isset($_GET['page']) ? $_GET['page'] : 0;
$param = $_GET['param'];
$value = $_GET['value'];
$orderType = "asc";
$getsPage = 'param='.$param.'&value='.$value.'&orden='.$orden;

if( $orden == 'novedad' )
	$orderType = "desc";
	
 
$productoDAO = new productoDAO();


if($param != "idCat2" && $param != "idCat21" && $param != "idCat22" &&$param != "idCat23"  &&$param != "idCat24"  &&$param != "idCat25" ){
    $productos = $productoDAO->search($orden, $orderType,$page*$itemsPerPage, $itemsPerPage, $param.'="'.$value.'"');
    $total = $productoDAO->total(2, $param, $value);
}else{
    $where = "(idCat2 = ".$value." OR idCat21 = ".$value." OR idCat22 = ".$value." OR idCat23 = ".$value." OR idCat24 = ".$value." OR idCat25 = ".$value.")";
    $productos = $productoDAO->search($orden, $orderType,$page*$itemsPerPage, $itemsPerPage, $where);
    $total = count($productoDAO->search($orden, $orderType,0, 1000, $where));
}

$totalPages = (int)($total/$itemsPerPage);
if( ($total%$itemsPerPage) != 0)
        $totalPages++;

$pageNext = $page + 1;
$pagePrev = $page - 1;
if( $pageNext >= $totalPages )
    $pageNext = 0;
if( $pagePrev < 0 )
    $pagePrev = 0;


$p1 = 'productos.php?'.$getsPage;
$p2 = 'productos.php?'.$getsPage;


///
$cat1DAO = new cat1DAO();
$cat2DAO = new cat2DAO();
$titulo1 = "";
$titulo2 = "";
$titulo = "";
$idCat1 = "#";
switch ($param){
    case 'idCat1':
        $cat1S = $cat1DAO->getById($value);
        if($cat1S == null){
            $cat1S = new cat1();
            $cat1S->setId(0);
        }
        $titulo1 = $cat1S->getNombre();
        $titulo = $titulo1;
        $idCat1 = $value;
        break;
    case 'idCat2':
        $cat2S = $cat2DAO->getById($value);
        if($cat2S == null){
            $cat2S = new cat1();
            $cat2S->setId(0);
        }
        $titulo2 = $cat2S->getNombre();
        $cat1S = $cat1DAO->getById($cat2S->getIdCat1());
        if($cat1S == null){
            $cat1S = new cat1();
            $cat1S->setId(0);
        }
        $titulo1 = $cat1S->getNombre();
        $idCat1 = $cat2S->getIdCat1();
        $titulo = $titulo2;
        break;
    default :
        $titulo1 = $value;
        $titulo = $titulo1;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>La Era Azul - Libros y Accesorios</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<link type="image/x-icon" href="era_azul.ico" rel="shortcut icon" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/large.css" rel="stylesheet" type="text/css" />
<?php
    //Xajax
    $xajax->printJavascript("xajax/");
?>
<script type="text/javascript" src="js/mootools-core-1.3.1-full-compat-yc.js"></script>
<script type="text/javascript" src="js/sexyforms.v1.3.mootools.min.js"></script>
<script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link   type="text/css" rel="stylesheet" href="sexyforms/gamma/sexyforms-gamma.css"  media="all" id="theme" />
<script type="text/javascript">
window.addEvent('domready', function() {
  $$(".sexyform input", ".sexyform select", ".sexyform textarea").each(function(el) {
    el.DoSexy();
  });
});
</script>
<script type="text/javascript">
function changeValue(id, value){
   document.getElementById(id).value = value;
}
</script>
<script src="js/jquery.tools.min.js"></script>
</head>
<body> 
<div id="header">
  <div class="gutter">
    <div class="inner">
      <div id="htop">
        <?php include("includes/bar.php"); ?>
        <?php include("includes/logo.php"); ?>
      </div>
      <?php include("includes/menu.php"); ?>
    </div>
  </div>
</div>
<div id="wrapper">
  <div id="wrapperbg">
    <div class="inner clearfix">
      <div id="main">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="215" valign="top">
              <?php if( isset($cat1S) ) include("includes/tematico.php"); ?>
              <?php include("includes/publicidad.php"); ?></td>
            <td valign="top">
            <div id="pathway">
                <p class="breadcrumb"><a href="index.php">Inicio</a>
                    <a href="productos.php?param=idCat1&value=<?php echo $idCat1;?>"><?php echo $titulo1;?></a>
                    <?php echo $titulo2;?>
                </p>
            </div>
            <div id="m_tot">
                <div class="pagination">
                    <?php if( $page == 0 ){ ?>
                    <span class="disabled">Prev</span>
                    <?php }else{ ?>
                    <a href="?page=<?php echo $pagePrev.'&'.$getsPage;?>" class="prev">Prev</a>
                    <?php } ?>
                    <?php
                    if( $totalPages > 10 && ($page > 5) )
                            echo '...';
                    //en caso de haber menos de 10 paginas...
                    for($i = 0; $i<$totalPages; $i++){
                        $is = $i+1;
                        if($page == $i){
                    ?>
                    <span class="current"><?php echo $is;?></span>
                    <?php
                    }else{
                    if( $totalPages > 10 && ($i-5 > $page || $i+5 < $page ) )
                        continue;
                    ?>
                    <a href="?page=<?php echo $i.'&'.$getsPage;?>"><?php echo $is;?></a>
                    <?php
                    }
                    }

                    if( $totalPages > 10 && ($page+6 < $totalPages) )
                            echo '...';

                    ?>
                    <?php if( $page+1 == $totalPages ){ ?>
                    <span class="disabled">Next</span>
                    <?php }else{ ?>
                    <a href="?page=<?php echo $pageNext.'&'.$getsPage;?>" class="next">Next</a>
                    <?php } ?>
                </div>
                <div id="contenido">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><h1><?php echo $titulo;?></h1></td>
                    <td>
                        <div id="ordenar">
                        <form id="form1" name="form1" method="get" action="" class="sexyform">
                          <label for="ordenarpor"></label>
                          <select name="orden" id="ordenarpor">
                              <option value="vendido" <?php if($orden == 'vendido') echo 'selected'; ?>>Más vendido</option>
                              <option value="novedad" <?php if($orden == 'novedad') echo 'selected'; ?>>Novedades</option>
                              <option value="nombre" <?php if($orden == 'nombre') echo 'selected'; ?>>Nombre</option>
                              <option value="precio" <?php if($orden == 'precio') echo 'selected'; ?>>Precio</option>
                          </select>
                          <input type="hidden" name="param" value="<?php echo $param;?>"/>
                          <input type="hidden" name="value" value="<?php echo $value;?>"/>
                          <input type="submit" value="Ordenar"/>
                        </form>
                        </div>
                    </td>
                    <td>
                        
                        <form id="form1" name="form1" method="get" action="" class="sexyform">
                          <label for="ordenarpor"></label>
                          <input type="hidden" name="orden" value="novedad"/>
                          <input type="hidden" name="param" value="<?php echo $param;?>"/>
                          <input type="hidden" name="value" value="<?php echo $value;?>"/>
                          <input type="submit" value="Novedades"/>
                        </form>
                        
                    </td>
                  </tr>
                </table>
                <div class="productus">
                <?php foreach ($productos as $producto){ ?>
                  <div class="imapro">
                      <span class="imapro_ima"><a href="producto.php?id=<?php echo $producto->getId();?>" ><img src="imagenes/productos/<?php echo $producto->getImg();?>" width="113" border="0"/></a></span>
                    <span class="imapro_tit"><?php echo $producto->getNombre();?></span>
                    <?php if($producto->getDescuento()){  ?>
                         <span class="imagor_pri" style="font-size: small;">$<strike><?php echo $producto->getPrecioFormatoDescuento(); ?></strike></span>
                        <?php } ?>
                    <span class="imapro_pri">$<?php echo $producto->getPrecioFormato();?></span>
                    <span class="imapro_buy" id="noti<?php echo $producto->getId();?>">
                        <?php if($producto->getStock() < 1){  ?>
                        <a class="btrig modalInput" href="#" rel="#modal_ups" onclick="changeValue('out_id','<?php echo $producto->getId();?>');">
                        <?php }else{?>
                        <a href="#" onclick="xajax_addToCart('<?php echo $producto->getId();?>','1','noti<?php echo $producto->getId();?>');return false;">
                        <?php }?>
                            <img src="img/bt_buy.png" border="0" />
                        </a>
                    </span>
                  </div>
                <?php } ?>
                </div><br clear="all" />
                </div>
            </div>
              <div class="pagination">
                    <?php if( $page == 0 ){ ?>
                    <span class="disabled">Prev</span>
                    <?php }else{ ?>
                    <a href="?page=<?php echo $pagePrev.'&'.$getsPage;?>" class="prev">Prev</a>
                    <?php } ?>
                    <?php
                    if( $totalPages > 10 && ($page > 5) )
                            echo '...';
                    //en caso de haber menos de 10 paginas...
                    for($i = 0; $i<$totalPages; $i++){
                        $is = $i+1;
                        if($page == $i){
                    ?>
                    <span class="current"><?php echo $is;?></span>
                    <?php
                    }else{
                    if( $totalPages > 10 && ($i-5 > $page || $i+5 < $page ) )
                        continue;
                    ?>
                    <a href="?page=<?php echo $i.'&'.$getsPage;?>"><?php echo $is;?></a>
                    <?php
                    }
                    }

                    if( $totalPages > 10 && ($page+6 < $totalPages) )
                            echo '...';

                    ?>
                    <?php if( $page+1 == $totalPages ){ ?>
                    <span class="disabled">Next</span>
                    <?php }else{ ?>
                    <a href="?page=<?php echo $pageNext.'&'.$getsPage;?>" class="next">Next</a>
                    <?php } ?>
                </div>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include("includes/footer.php"); ?>
</div>




<!--Carrito -->
<?php include("includes/mcart.php"); ?>


<!--Caso: Producto agotado -->
<?php include("includes/out_of_stock.php"); ?>

<!--Registro o login -->
<?php include("includes/relog.php"); ?>
<!--Funtions -->
<script type="text/javascript" src="js/jquery.hoverIntent.minified.js"></script> 
<script type="text/javascript" src="js/funtionm.js"></script> 
<script> 
$(document).ready(function() { 
	var triggers = $(".modalInput").overlay({ 
		mask: {
			color: '#ebecff',
			loadSpeed: 200,
			opacity: 0.9
		}
	});
});
</script>
</body>
</html>