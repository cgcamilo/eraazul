<?php
include 'includes.php';

$itemsPerPage = 20;
$page = isset($_GET['page']) ? $_GET['page'] : 0;
$orden = isset($_GET['orden']) ? $_GET['orden'] : "nombre";

$productoDAO = new productoDAO();

$productos = $productoDAO->search($orden, "asc",$page*$itemsPerPage, $itemsPerPage, ' promocion="1" ');
$total = $productoDAO->total(2, "promocion", "1");

$totalPages = (int)($total/$itemsPerPage);
if( ($total%$itemsPerPage) != 0)
        $totalPages++;

$pageNext = $page + 1;
$pagePrev = $page - 1;
if( $pageNext >= $totalPages )
    $pageNext = 0;
if( $pagePrev < 0 )
    $pagePrev = 0;


$p1 = 'promo.php';
$p2 = 'promo.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>La Era Azul - Libros y Accesorios</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<link type="image/x-icon" href="era_azul.ico" rel="shortcut icon" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/large.css" rel="stylesheet" type="text/css" />
<?php
    //Xajax
    $xajax->printJavascript("xajax/");
?>
<script type="text/javascript" src="js/mootools-core-1.3.1-full-compat-yc.js"></script>
<script type="text/javascript" src="js/sexyforms.v1.3.mootools.min.js"></script>
<script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link   type="text/css" rel="stylesheet" href="sexyforms/gamma/sexyforms-gamma.css"  media="all" id="theme" />
<script type="text/javascript">
window.addEvent('domready', function() {
  $$(".sexyform input", ".sexyform select", ".sexyform textarea").each(function(el) {
    el.DoSexy();
  });
});
</script>

<script src="js/jquery.tools.min.js"></script>
</head>
<body>
<div id="header">
  <div class="gutter">
    <div class="inner">
      <div id="htop">
        <?php include("includes/bar.php"); ?>
        <?php include("includes/logo.php"); ?>
      </div>
      <?php include("includes/menu.php"); ?>
    </div>
  </div>
</div>
<div id="wrapper">
  <div id="wrapperbg">
    <div class="inner clearfix">
      <div id="main">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="215" valign="top">
              <?php include("includes/publicidad.php"); ?></td>
            <td valign="top">
            <div id="pathway">
                <p class="breadcrumb"><a href="index.php">Inicio</a>
                    <a href="promo.php?">Promociones</a>
                </p>
            </div>
            <div id="m_tot">
                <div class="pagination">
                    <?php if( $page == 0 ){ ?>
                    <span class="disabled">Prev</span>
                    <?php }else{ ?>
                    <a href="?page=<?php echo $pagePrev;?>" class="prev">Prev</a>
                    <?php } ?>
                    <?php
                    for($i = 0; $i<$totalPages; $i++){
                        $is = $i+1;
                        if($page == $i){
                    ?>
                    <span class="current"><?php echo $is;?></span>
                    <?php
                    }else{
                    ?>
                    <a href="?page=<?php echo $i;?>"><?php echo $is;?></a>
                    <?php
                    }
                    }
                    ?>
                    <?php if( $page+1 == $totalPages ){ ?>
                    <span class="disabled">Next</span>
                    <?php }else{ ?>
                    <a href="?page=<?php echo $pageNext;?>" class="next">Next</a>
                    <?php } ?>
                </div>
                <div id="contenido">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><h1>Promociones</h1></td>
                    <td><div id="ordenar">
                                    <form id="form1" name="form1" method="get" action="" class="sexyform">
                                      <label for="ordenarpor"></label>
                                      <select name="orden" id="ordenarpor">
                                          <option value="vendido" <?php if($orden == 'vendido') echo 'selected'; ?>>Más vendido</option>
                                          <option value="novedad" <?php if($orden == 'novedad') echo 'selected'; ?>>Novedades</option>
                                          <option value="nombre" <?php if($orden == 'nombre') echo 'selected'; ?>>Nombre</option>
                                          <option value="precio" <?php if($orden == 'precio') echo 'selected'; ?>>Precio</option>
                                      </select>
                                      <input type="hidden" name="param" value="<?php echo $param;?>"/>
                                      <input type="hidden" name="value" value="<?php echo $value;?>"/>
                                      <input type="submit" value="Ordenar"/>
                                    </form>
                                    </div></td>
                  </tr>
                </table>
                <div class="productus">
                <?php foreach ($productos as $producto){ ?>
                  <div class="imapro">
                      <span class="imapro_ima"><a href="producto.php?id=<?php echo $producto->getId();?>" ><img src="imagenes/productos/<?php echo $producto->getImg();?>" width="113" border="0"/></a></span>
                    <span class="imapro_tit"><?php echo $producto->getNombre();?></span>
                    <span class="imapro_pri">$<?php echo $producto->getPrecioFormato();?></span>
                    <span class="imapro_buy" id="noti<?php echo $producto->getId();?>">
                        <a href="#" onclick="xajax_addToCart('<?php echo $producto->getId();?>','1','noti<?php echo $producto->getId();?>');return false;">
                            <img src="img/bt_buy.png" border="0" />
                        </a>
                    </span>
                  </div>
                <?php } ?>
                </div><br clear="all" />
                </div>
            </div>
              <div class="pagination">
                    <?php if( $page == 0 ){ ?>
                    <span class="disabled">Prev</span>
                    <?php }else{ ?>
                    <a href="?page=<?php echo $pagePrev;?>" class="prev">Prev</a>
                    <?php } ?>
                    <?php
                    for($i = 0; $i<$totalPages; $i++){
                        $is = $i+1;
                        if($page == $i){
                    ?>
                    <span class="current"><?php echo $is;?></span>
                    <?php
                    }else{
                    ?>
                    <a href="?page=<?php echo $i;?>"><?php echo $is;?></a>
                    <?php
                    }
                    }
                    ?>
                    <?php if( $page+1 == $totalPages ){ ?>
                    <span class="disabled">Next</span>
                    <?php }else{ ?>
                    <a href="?page=<?php echo $pageNext;?>" class="next">Next</a>
                    <?php } ?>
                </div>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include("includes/footer.php"); ?>
</div>




<!--Carrito -->
<?php include("includes/mcart.php"); ?>



<!--Registro o login -->
<?php include("includes/relog.php"); ?>
<!--Funtions -->
<script type="text/javascript" src="js/jquery.hoverIntent.minified.js"></script>
<script type="text/javascript" src="js/funtionm.js"></script>
<script>
$(document).ready(function() {
	var triggers = $(".modalInput").overlay({
		mask: {
			color: '#ebecff',
			loadSpeed: 200,
			opacity: 0.9
		}
	});
});
</script>
</body>
</html>