<?php session_start();?>
<?php
ini_set('memory_limit', '-1');

foreach (glob("./php/dao/*.php") as $filename){
    include $filename;
}

foreach (glob("./php/entities/*.php") as $filename){
    include $filename;
}

foreach (glob("./php/functions/*.php") as $filename){
    include $filename;
}


if( !isset($_SESSION['carrito']) ){
    $carrito = array();
    $_SESSION['carrito'] = serialize($carrito);
    $_SESSION['total'] = serialize(0);
}

require ('xajax/xajax_core/xajax.inc.php');
$xajax = new xajax();
include 'xajax/funtions/PHPAjaxFunctions.php';
$xajax->registerFunction("addToCart");
$xajax->registerFunction("updateCart");
$xajax->registerFunction("stockMessage");
$xajax->registerFunction("actualizaCiudad");
$xajax->processRequest();

?>