<?php
include 'includes.php';


$location = "location: ./cart.php?";
if( !isset($_SESSION['usuario']) ){
    header($location.'&user');
    exit;
}

$usuario = unserialize($_SESSION['usuario']);

if( !isset($_SESSION['order']) ){
    $order = new order();
    $order->setBAddress($usuario->getDir());
    $order->setBCity($usuario->getCiudad());
    $order->setBCountry($usuario->getPais());
    $order->setBEmail($usuario->getLogin());
    $order->setBCompany('');
    $order->setBName($usuario->getNombre());
    $order->setBPhone($usuario->getTel());
    $order->setBState($usuario->getDepto());
    $order->setBZip('-');

    $order->setSAddress($usuario->getDir());
    $order->setSCity($usuario->getCiudad());
    $order->setSCountry($usuario->getPais());
    $order->setSEmail($usuario->getLogin());
    $order->setSCompany('');
    $order->setSName($usuario->getNombre());
    $order->setSPhone($usuario->getTel());
    $order->setSState($usuario->getDepto());
    $order->setSZip('-');
    
    $_SESSION['order'] = serialize($order);
}else
    $order = unserialize($_SESSION['order']);


if( isset($_POST['cambiar']) ){
    foreach ($_POST as $key => $value) {
        $$key = text2HTML($value);
    }
    
    $order = new order();
    $order->setBAddress($dir);
    $order->setBCity($ciudad);
    $order->setBCountry($pais);
    $order->setBEmail($email);
    $order->setBCompany($empresa);
    $order->setBName($nombre);
    $order->setBPhone($tel);
    $order->setBState($barrio);
    $order->setBZip($iddepto);

    $order->setSAddress($dir);
    $order->setSEmail($email);
    $order->setSCompany($empresa);
    $order->setSName($nombre);
    $order->setSState($barrio);
    $order->setSPhone($tel);
    $order->setSZip($iddepto2);
    $_SESSION['order'] = serialize($order);

}


$daoConnection = new DAO;
$daoConnection->conectar();

$sql = "SELECT * FROM departamentos ORDER BY departamento_departamento";
$query = $daoConnection->consulta($sql);
$sql = "SELECT municipio_municipio FROM municipios WHERE municipio_departamento = '".$order->getBZip()."' ORDER BY municipio_municipio asc";
$query2 = $daoConnection->consulta($sql);


$sql = "SELECT * FROM departamentos ORDER BY departamento_departamento";
$query3 = $daoConnection->consulta($sql);
$sql = "SELECT municipio_municipio FROM municipios WHERE municipio_departamento = '".$order->getSZip()."' ORDER BY municipio_municipio asc";
$query4 = $daoConnection->consulta($sql);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>La Era Azul - Libros y Accesorios</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <?php
    //Xajax
    $xajax->printJavascript("xajax/");
?>
        <link type="image/x-icon" href="era_azul.ico" rel="shortcut icon" />
        <link href="css/css.css" rel="stylesheet" type="text/css" />
        <link href="css/large.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="js/mootools-core-1.3.1-full-compat-yc.js"></script>
        <script type="text/javascript" src="js/sexyforms.v1.3.mootools.min.js"></script>
        <script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
        <link   type="text/css" rel="stylesheet" href="sexyforms/gamma/sexyforms-gamma.css"  media="all" id="theme" />
        <script type="text/javascript">
        window.addEvent('domready', function() {
          $$(".sexyform input", ".sexyform select", ".sexyform textarea, .sexyformx input", ".sexyformx textarea").each(function(el) {
            el.DoSexy();
          });
        });
        </script>
        <script src="js/jquery.tools.min.js"></script>
        <style type="text/css">
        <!--
        #example {
                display: none
        }
        -->
        </style>
    </head>
<body> 
<div id="header">
  <div class="gutter">
    <div class="inner">
      <div id="htop">
        <?php include("includes/bar.php"); ?>
        <?php include("includes/logo.php"); ?>
      </div>
      <?php include("includes/menu.php"); ?>
    </div>
  </div>
</div>
<div id="wrapper">
  <div id="wrapperbg">
    <div class="inner clearfix">
      <div id="main">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="215" valign="top">
              <?php include("includes/publicidad.php"); ?></td>
            <td valign="top"><div id="m_tot">
              <div id="contenido">
                <div class="content">
                  <div id="cart"> 
                    <h1>Información de la facturación</h1>
                    <?php if(isset($_GET['falta']) ){ ?>
                    <span style="color: red">
                        Antes de "Continuar" por favor introduce toda la información de facturación y envío<br />
                        y haz clic en "Actualizar datos".
                    </span>
                    <?php } ?>
                    <form action="checkout.php" method="post" class="sexyformx">
                     <div class="modcont">
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                         <tr>
                           <td width="18%"><label for="nombre">Nombre completo:</label></td>
                           <td width="21%"><input name="nombre" type="text"  id="nombre" value="<?php echo $order->getBName();?>"/></td>
                           <td width="61%">&nbsp;</td>
                           </tr>
                         <tr>
                            <td>
                                <label for="iddepto">Departamento:</label>
                            </td>
                            <td>
                                <select name="iddepto" class="campo" id="iddepto" style="width: 134px" onchange="xajax_actualizaCiudad(this.options[this.options.selectedIndex].value, 'ciudadSelect');return false;" >
                                <option value="0">Seleccione</option>
                                    <?php while ($row = mysql_fetch_array($query)) { ?>
                                        <option value="<?= $row['departamento_id'] ?>" <?php  if($order->getBZip() == $row['departamento_id']) echo 'selected'; ?>>
                                            <?= text2HTML(utf8_encode($row['departamento_departamento'])) ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                         <tr>
                           <td><label for="cit">Ciudad:</label></td>
                           <td id="ciudadSelect">
                               <select name="ciudad" class="campo" id="cit" style="width: 134px">
                                 <option value="">Seleccionar ciudad</option>
                                   <?php while ($row = mysql_fetch_array($query2)) { ?>
                                        <option <?php  if(  text2HTML(utf8_encode($order->getBCity())) == text2HTML(  utf8_encode($row['municipio_municipio']) ) )  echo 'selected'; ?>>
                                        <?php echo text2HTML(utf8_encode($row['municipio_municipio'])) ?>                                        </option>
                                    <?php } ?>
                             </select>
                           </td>
                           <td>&nbsp;</td>
                         </tr>
                         <tr>
                           <td><label for="dir">Dirección:</label></td>
                           <td>
                               <input name="dir" type="text"  id="dir" value="<?php echo $order->getBAddress();?>" />                           </td>
                           <td>&nbsp;</td>
                           </tr>
                           <tr>
                           <td><label for="dir">Barrio:</label></td>
                           <td>
                               <input name="barrio" type="text"  id="dir" value="<?php echo $order->getBState();?>" />                           </td>
                           <td>&nbsp;</td>
                           </tr>
                         <tr>
                           <td><label for="tel">Teléfono:</label></td>
                           <td><input name="tel" type="text" id="tel" value="<?php echo $order->getBPhone();?>"  /></td>
                           <td>&nbsp;</td>
                           </tr>
                         <tr>
                           <td><label for="email">Correo electrónico:</label></td>
                           <td><input name="email" type="text" id="email" value="<?php echo $order->getBEmail();?>" /></td>
                           <td>&nbsp;</td>
                           </tr>
                         <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                         </tr>
                         <tr>
                           <td>&nbsp;</td>
                           <td><label for="button3"></label>
                               <input type="hidden" name="cambiar" value="1">
                                   <input type="submit" name="button3" id="button3" value="Actualizar dirección" /></td>
                           <td>&nbsp;</td>
                           </tr>
                       </table>
                     </div>
                   </form>
                </div>
                </div>
              </div>
            </div>
            <div id="m_tot">
              <div id="contenido">
                <div class="content">
                  <div id="cart"> 
                    <h1>Información de envío</h1>
                    <form action="php/action/checkOut-Billing.php" method="post" class="sexyformx">
                     <div class="modcont">
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                         <tr>
                           <td width="18%">&nbsp;</td>
                           <td width="73%"><input type="checkbox" name="mismo" id="misma"/>
                             <label for="misma">Enviar a la misma dirección de facturación</label></td>
                           <td width="9%">&nbsp;</td>
                         </tr>
                         <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                         </tr>
                         <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                         </tr>
                         <tr>
                           <td>&nbsp;</td>
                           <td>
                               <input type="button" name="button3" id="anadir_link" value="Añadir nueva dirección de envío"/>                             </td>
                           <td>&nbsp;</td>
                         </tr>
                         <tr>
                           <td>&nbsp;</td>
                           <td colspan="2">
                           <div id="anadir_nuevos_datos" style="display:none">
                             <label for="ana_cit"></label>
                             <select name="iddepto2" class="campo" id="iddepto" style="width: 134px" onchange="xajax_actualizaCiudad(this.options[this.options.selectedIndex].value, 'ciudadSelect2');return false;" >
                                <option value="0">Seleccione</option>
                                <?php while ($row = mysql_fetch_array($query3) ) { ?>
                                        <option value="<?= $row['departamento_id'] ?>" <?php  if($order->getSZip() == $row['departamento_id']) echo 'selected'; ?>>
                                            <?= text2HTML(utf8_encode($row['departamento_departamento'])) ?>
                                        </option>
                                    <?php } ?>
                            </select>
                             <div id="ciudadSelect2">
                                <select name="ciudad" class="campo" id="cit" style="width: 134px">
                                    <option value="">Seleccionar departamento</option>
                                    <?php while ($row = mysql_fetch_array($query4)) { ?>
                                        <option <?php  if(  text2HTML(utf8_encode($order->getSCity())) == text2HTML(  utf8_encode($row['municipio_municipio']) ) )  echo 'selected'; ?>>
                                        <?= utf8_encode($row['municipio_municipio']) ?>
                                        </option>
                                    <?php } ?>
                                </select>
                             </div>
                             <label for="ana_dir"></label>
                             <input name="dir2" type="text" id="ana_dir" value="<?php if($order->getSAddress() != "") echo $order->getSAddress(); else echo 'Nueva dirección'?>" />
                             <label for="ana_barrio"></label>
                             <input name="barrio2" type="text" id="ana_barrio" value="<?php if($order->getSState() != "") echo $order->getSState(); else echo 'Nuevo Barrio'?>" />
                             <label for="ana_tel"></label>
                             <input name="tel2" type="text" id="ana_tel" value="<?php if($order->getSPhone() != "") echo $order->getSPhone(); else echo 'Nuevo teléfono'?>" />
                           </div>
                           <script>
							$("#anadir_link").click(function () {
							$("#anadir_nuevos_datos").fadeIn("slow");
							});
							</script>                           </td>
                           </tr>
                         <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                         </tr>
                         <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                         </tr>
                         <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td><input type="submit" name="Continuar" id="Continuar" value="Continuar" /></td>
                         </tr>
                       </table>
                     </div>
                   </form>
                </div>
                </div>
              </div>
            </div>
              </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include("includes/footer.php"); ?>
</div>




<!--Carrito -->
<?php include("includes/mcart.php"); ?>



<!--Registro o login -->
<?php include("includes/relog.php"); ?>
<!--Funtions -->
<script type="text/javascript" src="js/jquery.hoverIntent.minified.js"></script> 
<script type="text/javascript" src="js/funtionm.js"></script> 
<script> 
$(document).ready(function() { 
	var triggers = $(".modalInput").overlay({ 
		mask: {
			color: '#ebecff',
			loadSpeed: 200,
			opacity: 0.9
		}
	});
});
</script>
</body>
</html>