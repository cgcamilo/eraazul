<?php include 'includes.php';?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>La Era Azul - Libros y Accesorios</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<link type="image/x-icon" href="era_azul.ico" rel="shortcut icon" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/large.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/mootools-core-1.3.1-full-compat-yc.js"></script>
<script type="text/javascript" src="js/sexyforms.v1.3.mootools.min.js"></script>
<script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link   type="text/css" rel="stylesheet" href="sexyforms/gamma/sexyforms-gamma.css"  media="all" id="theme" />
<script type="text/javascript">
window.addEvent('domready', function() {
  $$(".sexyform input", ".sexyform select", ".sexyform textarea").each(function(el) {
    el.DoSexy();
  });
});
</script>

<script src="js/jquery.tools.min.js"></script>
<style type="text/css">
<!--
label {padding-right: 10px;}
-->
</style>
</head>

    <body>
<div id="header">
  <div class="gutter">
    <div class="inner">
      <div id="htop">
        <?php include("includes/bar.php"); ?>
        <?php include("includes/logo.php"); ?>
      </div>
      <?php include("includes/menu.php"); ?>
    </div>
  </div>
</div>
<div id="wrapper">
  <div id="wrapperbg">
    <div class="inner clearfix">
      <div id="main">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="215" valign="top"><?php include("includes/publicidad.php"); ?></td>
            <td valign="top">
            <div id="m_tot">
              <div id="contenido">
                <div class="content">
                  <div id="cart">
                    <h1>CHAT</h1>
                    <form name="chatform" class="chat-form" method="post" action="php/action/loginChat.php">
                      <table width="400" border="0" cellspacing="0" cellpadding="0" class="">
                      <tr class="tabla1_titulo">
                        <td colspan="2" class="tabla1_titulo_c3">Para iniciar sesión por favor ingrese los siguientes datos</td>
                        </tr>
                      <?php if(isset($_GET['error1'])){ ?>
                      <tr>
                          <td colspan="2" align="center" class="tabla1_titulo_c3" style="color: red">Debes seleccionar una figura que corresponda con la requerida</td>
                      </tr>
                      <?php } ?>
                      <tr>
                        <td><div class="etiquetas">Nombre</div></td>
                        <td><div class="etiquetas">Email</div></td>
                      </tr>
                      <tr>
                        <td><input name="nombre" type="text" class="textfield1" id="nombre" /></td>
                        <td><input name="email" type="text" class="textfield1" id="email" /></td>
                      </tr>
                      <tr>
                        <td colspan="2" align="left">&nbsp;</td>
                      </tr>
                      <tr>
                      <td colspan="2" align="left"><h5>Para activar el chat es indispensable activar la visualización de pop-ups de su navegador</h5></td>
                      </tr>
                      <tr>
                      <td colspan="2" align="left">
                      <div class="myCaptcha"></div>
                            <script type="text/javascript">
                                    $(document).ready(function() {
                                            $('.myCaptcha').sexyCaptcha('captcha.process.php');
                                    });
                            </script>                      </td>
                      </tr>
                      <tr>
                        <td><div class="etiquetas"></div></td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td><input type="image" src="imagenes/boton_ingresar.png" name="button" id="button" value="Enviar"/></td>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                    </form>
                </div>
                </div>
              </div>
            </div>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include("includes/footer.php"); ?>
</div>




<!--Carrito -->
<?php include("includes/mcart.php"); ?>



<!--Registro o login -->
<?php include("includes/relog.php"); ?>
<!--Funtions -->
<script type="text/javascript" src="js/jquery.hoverIntent.minified.js"></script>
<script type="text/javascript" src="js/funtionm.js"></script>
<script>
$(document).ready(function() {
	var triggers = $(".modalInput").overlay({
		mask: {
			color: '#ebecff',
			loadSpeed: 200,
			opacity: 0.9
		}
	});
});
</script>
</body>
</html>
</html>