<?php
/**
 * Description of selectCity
 *
 * @author Oscar David Flórez Hernández
 * 
 */
include 'php/dao/daoConnection.php';
$iddepto = $_GET['iddepto'];

$daoConnection = new DAO;
$daoConnection->conectar();

$sql = "SELECT * FROM municipios WHERE municipio_departamento = '" . $iddepto . "' ORDER BY municipio_municipio";
$query = $daoConnection->consulta($sql);
?>
<select name="ciudad" id="cit">
    <?php while ($row = mysql_fetch_array($query)) { ?>
        <option value="<?= $row['municipio_id'] ?>"><?= utf8_encode($row['municipio_municipio']) ?></option>
    <?php } ?>
</select>