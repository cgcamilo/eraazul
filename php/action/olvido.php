<?php session_start();?>
<?php
include 'includeUser.php';

$email = $_POST['email'];
$p1 =  $_POST['p1'] != ""  ? $_POST['p1'] : "index.php?";
$p2 =  $_POST['p2'] != ""  ? $_POST['p2'] : "index.php?";

$location = "location: ./../../".$p1."&";

if($email == "" ){
    header($location."&errorL1");
    exit;
}

$userDAO = new userDAO;
$thisUser = $userDAO->getUserByLogin($email);

if( $thisUser == null){
    header($location."&errorO1");
    exit;
}


//nuevo pass
$pass = cRandom(6);
$passCrypt = mhash(MHASH_MD5, $pass);
$thisUser->setPass($passCrypt);

$userDAO->updateUserPass($thisUser);

$body = "<h3>Notificaci&oacute;n de cambio de contrase&ntilde;a LA ERA AZUL  </h3>\n\n";
$body .= "A petici&oacute;n tuya te enviamos una nueva contrase&ntilde;a<br />\n";
$body .= "<b>Email:</b> ".text2HTML($thisUser->getLogin())."<br />\n";
$body .= "<b>Contrase&ntilde;a:</b> ".text2HTML($pass)."<br /><br />\n";
$body .= "Gracias.<br />\n";


$mail2 = new sendCMail($thisUser->getLogin(), "system@laerazul.com.co", "datos acceso", $body, "text/html");
$mail2->sendMail();

header($location."&newPass");
exit;

?>