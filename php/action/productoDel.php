<?php
include 'include.php';

$id = $_GET['id'];
$idCat1 = $_GET['idCat1'];

if($id == ""){
    header("location: ./../../admin/menuAdmin.php?s=productos");
    exit;
}

$DAO = new productoDAO();
$producto = $DAO->getById($id);
@unlink('./../../imagenes/productos/'.$producto->getImg());
@unlink('./../../imagenes/productos/'.$producto->getImg1());
@unlink('./../../imagenes/productos/'.$producto->getImg2());
@unlink('./../../imagenes/productos/'.$producto->getImg3());
@unlink('./../../imagenes/productos/'.$producto->getImg4());
@unlink('./../../imagenes/productos/'.$producto->getImg5());
$DAO->delete($id);

//borro sus relaciones
$pRelDAO = new proRelDAO();
$pRels = $pRelDAO->getsByIdPro("id", "asc", $id);
foreach ($pRels as $pRel){
    $pRelDAO->delete($pRel->getId());
}

//everything fine!
header("location: ./../../admin/menuAdmin.php?s=productos&del&idCat1=".$idCat1);
exit;

?>