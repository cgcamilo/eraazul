<?php session_start();?>
<?php


if( isset($_SESSION['admin']) ){
    header("location: ./../../admin/menuAdmin.php");
    exit;
}

include '../dao/daoConnection.php';
include '../dao/usersDAO.php';
include '../entities/user.php';

$login = $_POST['usuario'];
$pass = $_POST['pass'];


$location = "location: ./../../admin/index.php?";

if($login == "" || $pass == ""){
    header($location."&error1");
    exit;
}

$userDAO = new userDAO;
$thisUser = $userDAO->getAdmin();


if($thisUser->getLogin() != $login){
    header($location."&error2L");
    exit;
}

//$passCrypt = mhash(MHASH_MD5, $pass);

/*if($thisUser->getPass() != $passCrypt){
    header($location."&error2P");
    exit;
}*/


//everything fine!
$_SESSION['admin'] = serialize($thisUser);
$_SESSION['chatAdmin'] = serialize($thisUser);

header("location: ./../../admin/menuAdmin.php");
exit;

?>