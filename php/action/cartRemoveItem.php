<?php
session_start();

/*
 *
 * Author: Camilo Cifuentes
 * Web: www.ccamilo.com
 * Email: info@ccamilo.com
 *
 */

include 'includeUser.php';

$location = "location: ./../../cart.php?remove";

$id = $_GET['id'];

$carrito = unserialize($_SESSION['carrito']);
unset ($carrito[$id]);

$total = 0;
foreach ($carrito as $producto){
    $lineaValor = $producto->getCantidad()*$producto->getPrecio();
    $total += $lineaValor;
}

$_SESSION['carrito'] = serialize($carrito);
$_SESSION['total'] = serialize($total);

//everything fine!
header($location);
exit;

?>
