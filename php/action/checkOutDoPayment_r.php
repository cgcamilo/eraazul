<?php
/*
 *
 * Author: Camilo Cifuentes
 * Web: www.ccamilo.com
 * Email: info@ccamilo.com
 *
 */

include 'includeUser.php';


$carrito = unserialize($_SESSION['carrito']);
$total = unserialize($_SESSION['total']);

if(count($carrito) == 0 ){
    $location = "location: ./../../cart.php?";
    header($location.'&errorEmpty');
    exit;
}

if( !isset($_SESSION['usuario']) ){
   $location = "location: ./../../cart.php?";
    header($location.'&user');
    exit;
}
$thisUser = unserialize($_SESSION['usuario']);

if( !isset($_SESSION['order']) ){
    $location = "location: ./../../shipping_billing.php?";
    header($location.'&errorEmpty');
    exit;
}

$order = unserialize($_SESSION['order']);
if( !$order->full() ){
    $location = "location: ./../../checkout.php?";
    header($location.'&falta');
    exit;
}

$aspectoDAO = new AspectsDAO;
if( $order->getSCity() == "Bogot&aacute;" || $order->getSCity() == "Bogotá")
    $shipping = $aspectoDAO->getAspect('shipping');
else
    $shipping = $aspectoDAO->getAspect('shipping2');

$order = unserialize($_SESSION['order']);
$order->setSubTotal($total);
$order->setShipping($shipping->getValue());
$order->setTotal(($total+$shipping->getValue()));
$order->setUserId($thisUser->getId());

//save and dispatch
$orderDAO = new orderDAO;
$orderLineDAO = new orderLinesDAO;
$code = cRandom(5);

//save order
$orderDAO->save($order);
$id = $orderDAO->getLastId();
$orderSaved = $orderDAO->getById($id);
$orderSaved->setCode($id.'.'.$code);
$orderSaved->setMethod("Pagos Online");
$orderDAO->update($orderSaved);

foreach ($carrito as $producto){
    $orderLine = new orderLines;
    $orderLine->setIdOrder($id);
    $orderLine->setIdProduct($producto->getId());
    $orderLine->setQuantity($producto->getCantidad());
    $orderLineDAO->save($orderLine);
}



//dispatch
$llave_encripcion = "13099602076";
$usuarioId = 71936;
$valor = ($total+$shipping->getValue());
$valor = number_format($valor,  2, '.', '');
$refVenta = $id.'.'.$code;
$moneda = "COP";

$firma= "$llave_encripcion~$usuarioId~$refVenta~$valor~$moneda";
$firma_codificada = md5($firma);

$PagoOnLineData = array();
$PagoOnLineData[0] = "descripcion=Pago en LA ERA AZUL";
$PagoOnLineData[1] = "usuarioId=".$usuarioId;
$PagoOnLineData[2] = "url_respuesta=http://".$_SERVER['SERVER_NAME']."/erazul/gracias.php";
$PagoOnLineData[3] = "url_confirmacion=http://".$_SERVER['SERVER_NAME']."/erazul/php/action/ipnPagosOnline.php";
$PagoOnLineData[4] = "refVenta=".$refVenta;
$PagoOnLineData[5] = "valor=".$valor;
$PagoOnLineData[6] = "moneda=".$moneda;
$PagoOnLineData[7] = "prueba=0";
$PagoOnLineData[8] = "emailComprador=".$thisUser->getLogin();
$PagoOnLineData[9] = "firma=".$firma_codificada;

$stringToSend = "?";
$stringToSend .= implode("&", $PagoOnLineData);
header("Location: https://gateway.pagosonline.net/apps/gateway/index.html".$stringToSend);
exit;

?>