<?php session_start();?>
<?php


if( !isset($_SESSION['admin']) ){
    header("location: ./../../admin/index.php");
    exit;
}

include '../dao/daoConnection.php';
include '../dao/orderDAO.php';
include '../dao/orderLinesDAO.php';
include '../entities/orders.php';
include '../entities/orderLines.php';

include '../functions/text2HTML.php';

$id = $_POST['id'];
$state = $_POST['state'];
$code = $_POST['code'];


if($id == ""){
    header("location: ./../../admin/menuAdmin.php?s=pedisosEdit&id=".$id."&error1");
    exit;
}

//elimino el pedido
$orderDAO = new orderDAO;
$order = $orderDAO->getById($id);
$order->setCode($code);
$order->setState($state);
$orderDAO->update($order);

//everything fine!
header("location: ./../../admin/menuAdmin.php?s=pedisosEdit&id=".$id."&ok");
exit;

?>