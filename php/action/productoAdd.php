<?php
include 'include.php';

foreach ($_POST as $key => $value) {
    if($value == "on")
        $value = true;
    ${$key}= text2HTML($value);
}



if( $nombre == "" || $precio == "" || $stock == "" || $ref == ""){
    header("location: ./../../admin/menuAdmin.php?s=productosAdd&error1&".POSTSAsString());
    exit;
}

if(  !is_numeric($precio) || !is_numeric($stock) ){
    header("location: ./../../admin/menuAdmin.php?s=productosAdd&error3&".POSTSAsString());
    exit;
}

$fileNameImg = $_FILES['img']['name'];
$fileNameImg1 = $_FILES['img1']['name'];
$fileNameImg2 = $_FILES['img2']['name'];
$fileNameImg3 = $_FILES['img3']['name'];
$fileNameImg4 = $_FILES['img4']['name'];
$fileNameImg5 = $_FILES['img5']['name'];




if( $fileNameImg == ""){
    header("location: ./../../admin/menuAdmin.php?s=productosAdd&error2&".POSTSAsString());
    exit;
}

$DAO = new productoDAO();
$producto = new producto();

$producto->setNombre($nombre);
$producto->setNombreSimple(strtolower(text2NoAccent($nombre)));
$producto->setRef($ref);
$producto->setIdCat1($idCat1);
$producto->setIdCat2($idCat2);
$producto->setDatos($datos);
$producto->setDatosTecnicos($datosTecnicos);
$producto->setCorta($corta);
$producto->setStock($stock);
$producto->setPrecio($precio);

$producto->setNovedad($novedad);
$producto->setRegalar($regalar);
$producto->setVendido($vendido);
$producto->setRecomendado($recomendado);
$producto->setPopular($popular);
$producto->setPromocion($promocion);

$producto->setIva($iva);
$producto->setDescuento($descuento);
$producto->setPrecioDescuento($precioDescuento);

for($i=1;$i<=5;$i++){
    $producto->{"setIdCat1".$i}(${"idCat1".$i});
}

//guardo la imagen ppal
if($fileNameImg != ""){
    $r = cRandom(4);
    $destino = './../../imagenes/productos/'.$r.'.'.$fileNameImg;
    $image = new SimpleImage();
    $image->load($_FILES['img']['tmp_name']);
    if($idCat1 != 3)
        $image->resize(113, 180);
    else
        $image->resizeToWidth(122);
    $image->save($destino);
    $producto->setImg($r.'.'.$fileNameImg);

}

//si introdujo alguna de las 5 imagenes opciones, creo su thumb y se la guardo
for($i = 1; $i <= 5 ; $i ++){
    if( ${"fileNameImg".$i} == "" )
        continue;
    $key = cRandom(4);
    $destino = './../../imagenes/productos/'.$key.'.'.${"fileNameImg".$i};
    $destinoTH = './../../imagenes/productos/thumb_'.$key.'.'.${"fileNameImg".$i};
    //guardo la imagen.. si ok, guardo el thumb
    if(move_uploaded_file( $_FILES[ 'img'.$i ][ 'tmp_name' ], $destino)){
        //cambio el tamaño de la imagen
        $image2 = new SimpleImage();
        $image2->load($destino);
        $image2->resizeToWidth(226);
        $image2->save($destinoTH);

        $producto->{"setImg".$i}($key.'.'.${"fileNameImg".$i});
    }
}



$DAO->save($producto);
$id = $DAO->getLastId();



//everything fine!
header("location: ./../../admin/menuAdmin.php?s=productos&ok&idCat1=".$idCat1.'&id='.$id);
exit;

?>