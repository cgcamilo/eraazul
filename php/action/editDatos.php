<?php
include 'includeUser.php';

$location = "location: ./../../myaccount.php?";
if( !isset($_SESSION['usuario']) ){
    header($location.'&user');
    exit;
}

foreach ($_POST as $key => $value) {
    if($value == 'on')
        $value = true;
    ${$key}= $value;
}

$usuario = unserialize($_SESSION['usuario']);

$userDAO = new userDAO;
$thisUser = $userDAO->getUserByLogin($usuario->getLogin());

if( $thisUser == null){
    header($location."&user");
    exit;
}

$thisUser->setApellidos($apellidos);
$thisUser->setNombre($nombre);
$thisUser->setCiudad($ciudad);
$thisUser->setDepto($depto);
$thisUser->setDir($dir);
$thisUser->setEmpresa($empresa);
$thisUser->setFax($fax);
$thisUser->setNacimiento($nacimiento);
$thisUser->setPais($pais);
$thisUser->setRecibir($recibir);
$thisUser->setTel($tel);

$userDAO->update($thisUser);

$_SESSION['usuario'] = serialize($thisUser);

header($location.'&okEdit');
exit;

?>