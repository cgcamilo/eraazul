<?php session_start();?>
<?php
ini_set('memory_limit', '-1');

if( !isset($_SESSION['admin']) ){
    header("location: ./../../admin/index.php");
    exit;
}

foreach (glob("../dao/*.php") as $filename){
    include $filename;
}

foreach (glob("../entities/*.php") as $filename){
    include $filename;
}

foreach (glob("../functions/*.php") as $filename){
    include $filename;
}
?>
