<?php
include 'includeUser.php';

$location = "location: ./../../myaccount.php?";
if( !isset($_SESSION['usuario']) ){
    header($location.'&user');
    exit;
}

foreach ($_POST as $key => $value) {
    ${$key}= $value;
}

$usuario = unserialize($_SESSION['usuario']);

$userDAO = new userDAO;
$thisUser = $userDAO->getUserByLogin($usuario->getLogin());

if( $thisUser == null){
    header($location."&user");
    exit;
}

if( mhash(MHASH_MD5, $passOld) !=  $thisUser->getPass()){
    header($location.'&oldPass#pass');
    exit;
}

if( $pass1 !=  $pass2){
    header($location.'&missPass#pass');
    exit;
}

//nuevo pass
$passCrypt = mhash(MHASH_MD5, $pass1);
$thisUser->setPass($passCrypt);

$userDAO->updateUserPass($thisUser);


header($location.'&okPass#pass');
exit;

?>