<?php
include 'include.php';

$idCat1 = $_POST['idCat1'];

$location = "location: ./../../admin/menuAdmin.php?s=productosImportar&idCat1=".$idCat1;

$fileName = $_FILES['file']['name'];
$fileType = $_FILES['file']['type'];
$fileSize = $_FILES['file']['size'];

if($fileName == ""){
    header($location.'&error');
    exit;
}

$destinoXSL = '../../docs/'.$fileName;
if(!move_uploaded_file( $_FILES[ 'file' ][ 'tmp_name' ], $destinoXSL)){
    header($location.'&error');
    exit;
}



set_time_limit ( 0 );
require_once '../functions/Excel/reader.php';


$data = new Spreadsheet_Excel_Reader();
$data->setOutputEncoding('CP1251');
$data->read($destinoXSL);

error_reporting(E_ALL ^ E_NOTICE);

$counter = 0;

$productoDAO = new productoDAO;

for ($i = 1; $i <= $data->sheets[0]['numRows']; $i++) {
	for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
            if($i != 1){

                $data->sheets[0]['cells'][$i][$j] = text2HTML($data->sheets[0]['cells'][$i][$j]);

                if($j==1){
                    $ref = $data->sheets[0]['cells'][$i][$j];
                }
                if($j==2)
                    $cantidad  = $data->sheets[0]['cells'][$i][$j];
            }
        }

        $producto = $productoDAO->getByRef($ref);
        if( $producto != null  ){
            $producto->setStock($cantidad);
            $productoDAO->updateStock($producto);
            $counter++;
        }


}

header($location.'&ok='.$counter);
exit;

?>
