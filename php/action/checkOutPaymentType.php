<?php
include 'includeUser.php';


$carrito = unserialize($_SESSION['carrito']);

if(count($carrito) == 0 ){
    $location = "location: ./../../cart.php?";
    header($location.'&errorEmpty');
    exit;
}

if( !isset($_SESSION['order']) ){
    $location = "location: ./../../shipping_billing.php?";
    header($location.'&errorEmpty');
    exit;
}

$order = unserialize($_SESSION['order']);
$order->setMethod($_POST['method']);
$_SESSION['order'] = serialize($order);


//everything fine!
$location = "location: ./../../confirmation.php";
header($location);
exit;

?>
