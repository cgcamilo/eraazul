<?php
session_start();

/*
 *
 * Author: Camilo Cifuentes
 * Web: www.ccamilo.com
 * Email: info@ccamilo.com
 *
 */

include 'includeUser.php';

$location = "location: ./../../cart.php?update";

$id = $_GET['id'];
$q = $_GET['q'];

$carrito = unserialize($_SESSION['carrito']);
if(isset($carrito[$id])){
    $carrito[$id]->setCantidad($q);
}

$total = 0;
foreach ($carrito as $producto){
    $lineaValor = $producto->getCantidad()*$producto->getPrecio();
    $total += $lineaValor;
}

$_SESSION['carrito'] = serialize($carrito);
$_SESSION['total'] = serialize($total);

//everything fine!
header($location);
exit;

?>
