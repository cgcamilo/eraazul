<?php
/*
 *
 * Author: Camilo Cifuentes
 * Web: www.ccamilo.com
 * Email: info@ccamilo.com
 *
 */
include 'includeUser.php';

$pedidoDAO = new orderDAO();

$codigo = $_POST['ref_venta'];
$estado = $_POST['estado_pol'];
$pedido = $pedidoDAO->getByCode($codigo);
if($pedido != null){
    
    switch ($estado){
    case '1':
        $estadoTxt = 'Sin abrir';
        break;
    case '2':
        $estadoTxt = 'Abierta';
        break;
    case '4':
        $estadoTxt = 'Pagada y abonada';
        break;
    case '5':
        $estadoTxt = 'Cancelada';
        break;
    case '6':
        $estadoTxt = 'Rechazada';
        break;
    case '7':
        $estadoTxt = 'En validación';
        break;
    case '8':
        $estadoTxt = 'Reversada';
        break;
    case '9':
        $estadoTxt = 'Reversada fraudulenta';
        break;
    case '10':
        $estadoTxt = 'Enviada ent. Financiera';
        break;
    case '11':
        $estadoTxt = 'Capturando datos tarjeta de crédito';
        break;
    case '12':
        $estadoTxt = 'Capturando datos tarjeta de crédito';
        break;
    case '13':
        $estadoTxt = 'Esperando confirmación sistema PSE';
        break;
    case '14':
        $estadoTxt = 'Confirmando pago Efecty';
        break;
    case '15':
        $estadoTxt = 'Impreso';
        break;
    case '16':
        $estadoTxt = 'Debito ACH Registrado';
        break;
    
    default:
        $estadoTxt = $estado;
        break;
    }

    //compruebo firma
    $llave_encripcion = "13099602076";
    $usuarioId = 71936;
    $valor = $_POST['valor'];
    $refVenta = $codigo;
    $moneda = "COP";

    $firma= "$llave_encripcion~$usuarioId~$refVenta~$valor~$moneda~$estado";
    $firma_codificada = md5($firma);


    if(strtoupper($firma_codificada) == strtoupper(($_POST['firma'])) ) {
        $pedido->setState($estadoTxt);
        $pedidoDAO->update($pedido);
    }else{
        $pedido->setState($estadoTxt.'<br />[La firma digital no coincide, verificar transacci&oacute;n] ');
        $pedidoDAO->update($pedido);
    }

    //disminuir cantidades
    $orderLineDAO = new orderLinesDAO;
    $lines = $orderLineDAO->getsByPedido("id", "asc", $pedido->getId());

    $productoDAO = new productoDAO();
    foreach ($lines as $line){
        $producto = $productoDAO->getById($line->getIdProduct());
        if($producto == null)
            continue;
        $producto->setCantidad($producto->getCantidad()-$line->getQuantity());
        $productoDAO->update($producto);
    }
}
?>