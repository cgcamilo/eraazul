<?php
session_start();

/*
 *
 * Author: Camilo Cifuentes
 * Web: www.ccamilo.com
 * Email: info@ccamilo.com
 *
 */

include 'includeUser.php';

$location = "location: ./../../cart.php?";

$id = $_GET['id'];

$carrito = unserialize($_SESSION['carrito']);

if(count($carrito) == 0 ){
    header($location.'&errorEmpty');
    exit;
}

if( !isset($_SESSION['usuario']) ){
    header($location.'&user');
    exit;
}


//everything fine!
$location = "location: ./../../checkout.php";
header($location);
exit;

?>
