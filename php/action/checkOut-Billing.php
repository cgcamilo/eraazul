<?php
include 'includeUser.php';

$location = "location: ./../../cart.php?";

$carrito = unserialize($_SESSION['carrito']);

if(count($carrito) == 0 ){
    header($location.'&errorEmpty');
    exit;
}

if( !isset($_SESSION['usuario']) ){
    header($location.'&user');
    exit;
}


if( !isset($_SESSION['order']) ){
    $location = "location: ./../../checkout.php?";
    header($location.'&order');
    exit;
}

$order = unserialize($_SESSION['order']);
foreach ($_POST as $key => $value) {
    if($value == 'on')
        $value = true;
    $$key = $value;
}


if($mismo == "on"){
    $order->setSAddress($order->getBAddress());
    $order->setSState($order->getBState());
    $order->setSCity($order->getBCity());
    $order->setSCountry($order->getBCountry());
    $order->setSPhone($order->getBPhone());
    $order->setSZip($order->getBZip());
}else{
    $order->setSAddress($dir2);
    $order->setSState($barrio2);
    $order->setSCity($ciudad);
    $order->setSCountry($pais2);
    $order->setSPhone($tel2);
    $order->setSZip($iddepto2);
}

$_SESSION['order'] = serialize($order);

if( !$order->full()){
    $location = "location: ./../../checkout.php?error";
    header($location.'&falta&'.$order->getMiss());
    exit;
}


$location = "location: ./../../pago.php?";

header($location);
exit;

?>