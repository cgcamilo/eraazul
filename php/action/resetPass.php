<?php session_start();?>
<?php

include '../dao/daoConnection.php';
include '../dao/userDAO.php';
include '../entities/user.php';
include '../functions/mail.php';
include '../functions/random.php';

$email = $_POST['email'];
$email2 = $_POST['email2'];

$location = "location: ./../../recordar_contrasena.php?";

if($email == "" || $email2 == ""){
    header($location."&error1");
    exit;
}

if( $email != $email2 ){
    header($location."&error2");
    exit;
}

$userDAO = new userDAO;
$thisUser = $userDAO->getUserByLogin($email);

if( $thisUser == null){
    header($location."&error3");
    exit;
}


//nuevo pass
$pass = cRandom(6);
$passCrypt = mhash(MHASH_MD5, $pass);
$thisUser->setPass($passCrypt);

$userDAO->updateUserPass($thisUser);


header("location: ./../../recordar_contrasena.php?ok");
exit;

?>