<?php session_start();?>
<?php


if( !isset($_SESSION['admin']) ){
    header("location: ./../../admin/index.php");
    exit;
}

include '../dao/daoConnection.php';
include '../dao/orderDAO.php';
include '../dao/orderLinesDAO.php';
include '../entities/orders.php';
include '../entities/orderLines.php';

include '../functions/text2HTML.php';

$id = $_GET['id'];


if($id == ""){
    header("location: ./../../admin/menuAdmin.php?s=pedidos&error1");
    exit;
}

//elimino el pedido
$orderDAO = new orderDAO;
$orderDAO->delete($id);

//elimino sus lineas
$orderLineDAO = new orderLinesDAO;
$lines = $orderLineDAO->getsByPedido("id", "asc", $id);
foreach ($lines as $line){
    $orderLineDAO->delete($line->getId());
}



//everything fine!
header("location: ./../../admin/menuAdmin.php?s=pedidos&del");
exit;

?>