<?php
function timeLeft($fecha1, $hora1){


	list($dia1,$mes1,$ano1) = explode("-",$fecha1);
    list($hora1,$min1,$sec1) = explode(":",$hora1.":00");

    //timestamp de la fecha dada
    $timestamp1 = mktime($hora1,$min1,$sec1,$mes1,$dia1,$ano1);

    //timestamop de la hora actual
    $fechaActual = date(d."-".m."-".Y);
	$horaActual = date(H.":".i.":".s);
    
    list($horaA,$minA,$secA) = explode(":",$horaActual);
    list($diaA,$mesA,$anoA) = explode("-",$fechaActual);
    //$timestamp2 = mktime($horaA,$minA,$secA,$mesA,$diaA,$anoA);
    $timestamp2 = time();

    //diferencia de timestamp
    $segundos_diferencia = $timestamp1 - $timestamp2;
    return timeLeft2($segundos_diferencia);
/*
    if( $segundos_diferencia <= 60 ){
        echo "Menos de un minuto";
        return true;
    }

    $minutos_diferencia = $segundos_diferencia / (60);
    if( $minutos_diferencia <= 60 ){
        echo number_format($minutos_diferencia)."m";
        return true;
    }

    $horas_diferencia = $segundos_diferencia / (60*60);
    if( $horas_diferencia <= 24 ){
        echo number_format($horas_diferencia)."h";
        return true;
    }

    $dias_diferencia = $segundos_diferencia / (60*60*24);
    if( $dias_diferencia <= 24 ){
        echo number_format($dias_diferencia,0)."d ";
        return true;
    }
*/
}

function timeLeft2($segundos){
   $segundos = abs($segundos);
   $r_segundos = $segundos % 60;
   $minutos = ($segundos - $r_segundos) / 60;
   $r_minutos = $minutos % 60;
   $horas = ($minutos - $r_minutos) / 60;
   $r_horas = $horas % 24;
   $dias = ($horas - $r_horas) / 24;

   if ($dias>0) {
      echo $dias . "d " . $r_horas. "h " .$r_minutos. "m";
   }else{
      if ($horas>0){
         echo  $horas. " : " .$r_minutos. " : ".$r_segundos;
      }else{
         if ($minutos>0){
            echo  "00 : ".$minutos. " : " .$r_segundos;
         }else{
             echo  "00 : 00 : ".$segundos;
         }
      }
   }

}

function secondsLeft($fecha1, $hora1){


	list($dia1,$mes1,$ano1) = explode("-",$fecha1);
    list($hora1,$min1,$sec1) = explode(":",$hora1);

    //timestamp de la fecha dada
    $timestamp1 = mktime($hora1,$min1,$sec1,$mes1,$dia1,$ano1);

    //timestamop de la hora actual
    $fechaActual = date(d."-".m."-".Y);
	$horaActual = date(H.":".i.":".s);

    list($horaA,$minA,$secA) = explode(":",$horaActual);
    list($diaA,$mesA,$anoA) = explode("-",$fechaActual);
    $timestamp2 = mktime($horaA,$minA,$secA,$mesA,$diaA,$anoA);


    //diferencia de timestamp
    $segundos_diferencia = $timestamp1 - $timestamp2;
    return $segundos_diferencia;
}

function addSecs($fecha1, $hora1, $ext){
    //$fecha1 = "26-02-2010";
    //$hora1 = "15:40";
    list($dia1,$mes1,$ano1) = explode("-",$fecha1);
    list($hora1,$min1,$sec1) = explode(":",$hora1);
    //timestamp de la fecha dada
    $segundos = mktime($hora1,$min1,$sec1,$mes1,$dia1,$ano1);

    //echo "Now is ".date("H:i:s", $segundos);
    //echo "<br />";
    $segundos += $ext;

    //echo "20 secs later is ".date("H:i", $segundos);
    return date("d-m-Y*H:i", $segundos);
}

?>
