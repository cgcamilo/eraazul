<?php

/*
 * By Camilo Cifuentes    (  http://cc-easysolutions.netne.net/  )
 */

/*
 * $fecha must be aa-dd-aaaa
 * $hora must be hh:mm
 *
 * return true if the given date is later than the actual sever date
 */
function checkAfterActuaDate($fecha, $hora){


	list($dia1,$mes1,$ano1) = explode("-",$fecha);
	list($hora1,$min1) = explode(":",$hora);

	$timestamp1 = mktime($hora1,$min1,0,$mes1,$dia1,$ano1);

	$fechaActual = date(d."-".m."-".Y);
	$horaActual = date(H.":".i);

	list($dia2,$mes2,$ano2) = explode("-",$fechaActual);
	list($min2,$hora2) = explode(":",$horaActual);

	$timestamp2 = mktime($hora2,$min2,0,$mes2,$dia2,$ano2);

	$segundos_diferencia = $timestamp1 - $timestamp2;

	if( $segundos_diferencia <= 0 )
		return false;
	else
		return true;

}

/*
 * Valida hasta el 2030!!
 */
function checkAfterActuaDate2($fecha, $hora){
        $fecha_actual = strtotime(date("d-m-Y H:i:00",time()));
        $fecha_entrada = strtotime($fecha." ".$hora.":00");

        if($fecha_actual > $fecha_entrada){
            return false;
        }else{
            return true;
        }
}


function ShowDate(){

    $mes = date(n);
    $dia = date(l);
    $numMes = date(j);

    switch ($mes){
        default:
         case 1:
            $mes = 'Enero';
            break;
        case 2:
            $mes='Febrero';
            break;
        case 3:
            $mes='Marzo';
            break;
        case 4:
            $mes='Abril';
            break;
        case 5:
            $mes='Mayo';
            break;
        case 6:
            $mes='Junio';
            break;
        case 7:
            $mes='Julio';
            break;
        case 8:
            $mes='Agosto';
            break;
        case 9:
            $mes='Septiembre';
            break;
        case 10:
            $mes='Octubre';
            break;
        case 11:
            $mes='Noviembre';
            break;
        case 12:
            $mes='Diciembre';
            break;
    }

    switch ($dia){
        default:
        case 'Monday':
            $dia='Lunes';
            break;
        case 'Tuesday':
            $dia='Martes';
            break;
        case 'Wednesday':
            $dia='Miercoles';
            break;
        case 'Thursday':
            $dia='Jueves';
            break;
        case 'Friday':
            $dia='Viernes';
            break;
        case 'Saturday':
            $dia = 'Sabado';
            break;
        case 'Sunday':
            $dia = 'Domingo';
            break;
    }

    $anno = date(Y);
    return $dia." ".$numMes." de ".$mes." de ".$anno;
}


?>
