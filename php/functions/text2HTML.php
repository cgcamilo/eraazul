<?php

/*
 * By Camilo Cifuentes    (  http://www.siteYourBusiness.com/  )
 */

function text2HTML($mensaje){
    $mensaje = str_replace("á","&aacute;",$mensaje);
    $mensaje = str_replace("é","&eacute;",$mensaje);
    $mensaje = str_replace("í","&iacute;",$mensaje);
    $mensaje = str_replace("ó","&oacute;",$mensaje);
    $mensaje = str_replace("ú","&uacute;",$mensaje);
    $mensaje = str_replace("ñ","&ntilde;",$mensaje);
    $mensaje = str_replace("¿","&iquest;",$mensaje);

    $mensaje = str_replace("Á","&Aacute;",$mensaje);
    $mensaje = str_replace("É","&Eacute;",$mensaje);
    $mensaje = str_replace("Í","&Iacute;",$mensaje);
    $mensaje = str_replace("Ó","&Oacute;",$mensaje);
    $mensaje = str_replace("Ú","&Uacute;",$mensaje);
    $mensaje = str_replace("Ñ","&Ntilde;",$mensaje);
    return $mensaje;
}

function text2NoAccent($mensaje){
    $mensaje = str_replace("á","a",$mensaje);
    $mensaje = str_replace("é","e",$mensaje);
    $mensaje = str_replace("í","i",$mensaje);
    $mensaje = str_replace("ó","o",$mensaje);
    $mensaje = str_replace("ú","u",$mensaje);

    $mensaje = str_replace("Á","a",$mensaje);
    $mensaje = str_replace("É","e",$mensaje);
    $mensaje = str_replace("Í","i",$mensaje);
    $mensaje = str_replace("Ó","o",$mensaje);
    $mensaje = str_replace("Ú","u",$mensaje);

    $mensaje = str_replace("&aacute;","a",$mensaje);
    $mensaje = str_replace("&eacute;","e",$mensaje);
    $mensaje = str_replace("&iacute;","i",$mensaje);
    $mensaje = str_replace("&oacute;","o",$mensaje);
    $mensaje = str_replace("&uacute;","u",$mensaje);

    $mensaje = str_replace("&Aacute;","a",$mensaje);
    $mensaje = str_replace("&Eacute;","e",$mensaje);
    $mensaje = str_replace("&Iacute;","i",$mensaje);
    $mensaje = str_replace("&Oacute;","o",$mensaje);
    $mensaje = str_replace("&Uacute;","u",$mensaje);
    return $mensaje;
}
?>
