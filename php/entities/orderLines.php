<?php


class orderLines{
    private $id;
    private $idOrder;
    private $idProduct;
    private $quantity;
    private $size;


    function __construct(){
        $this->id = 0;
        $this->idUsuario = 0;
    }

    //GET'S & SET'S
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getIdOrder() {
        return $this->idOrder;
    }

    public function setIdOrder($idOrder) {
        $this->idOrder = $idOrder;
    }

    public function getIdProduct() {
        return $this->idProduct;
    }

    public function setIdProduct($idProduct) {
        $this->idProduct = $idProduct;
    }

    public function getQuantity() {
        return $this->quantity;
    }

    public function setQuantity($quantity) {
        $this->quantity = $quantity;
    }

    public function getSize() {
        return $this->size;
    }

    public function setSize($size) {
        $this->size = $size;
    }



}

?>