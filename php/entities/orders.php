<?php

/*
 *
 * Author: Camilo Cifuentes
 * Web: www.ccamilo.com
 * Email: info@ccamilo.com
 *
 */

class order{
    private $id;
    private $state;
    private $date;
    private $method;
    private $code;
    private $userId;

    private $subTotal;
    private $shipping;
    private $total;
    private $divisa;

    private $bName;
    private $bLastName;
    private $bAddress;
    private $bCity;
    private $bState;
    private $bZip;
    private $bCountry;
    private $bPhone;
    private $bEmail;

    private $sName;
    private $sLastName;
    private $sAddress;
    private $sCity;
    private $sState;
    private $sZip;
    private $sCountry;
    private $sPhone;
    private $sEmail;

    private $miss;


    function __construct(){
        $this->id = 0;
        $this->date = 'none';
        $this->state = 'pending' ;
        $this->method = 'Pgos Online' ;
        $this->code = 'pending' ;
        $this->userId = '0' ;
        $this->subTotal = '0' ;
        $this->shipping = '0' ;
        $this->total = '0' ;
        $this->divisa = 'COP' ;

        $this->bCity = 'x' ;
        $this->bCountry = 'x' ;
        $this->bState = 'x' ;
        $this->sCity = 'x' ;
        $this->sCountry = 'x' ;
        $this->sState = 'x' ;
        $this->bLastName = 'x' ;
        $this->sLastName = 'x' ;

    }

    //GET'S & SET'S
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getBName() {
        return $this->bName;
    }

    public function setBName($bName) {
        $this->bName = $bName;
    }

    public function getBLastName() {
        return $this->bLastName;
    }

    public function setBLastName($bLastName) {
        $this->bLastName = $bLastName;
    }
    
    public function getBCompany() {
        return $this->bLastName;
    }

    public function setBCompany($company){
        $this->bLastName = $company;
    }

    public function getBAddress() {
        return $this->bAddress;
    }

    public function setBAddress($bAddress) {
        $this->bAddress = $bAddress;
    }

    public function getBCity() {
        return $this->bCity;
    }

    public function setBCity($bCity) {
        $this->bCity = $bCity;
    }

    public function getBState() {
        return $this->bState;
    }

    public function setBState($bState) {
        $this->bState = $bState;
    }

    public function getBZip() {
        return $this->bZip;
    }

    public function setBZip($bZip) {
        $this->bZip = $bZip;
    }

    public function getBCountry() {
        return $this->bCountry;
    }

    public function setBCountry($bCountry) {
        $this->bCountry = $bCountry;
    }

    public function getBPhone() {
        return $this->bPhone;
    }

    public function setBPhone($bPhone) {
        $this->bPhone = $bPhone;
    }

    public function getBEmail() {
        return $this->bEmail;
    }

    public function setBEmail($bEmail) {
        $this->bEmail = $bEmail;
    }

    public function getSName() {
        return $this->sName;
    }

    public function setSName($sName) {
        $this->sName = $sName;
    }

    public function getSLastName() {
        return $this->sLastName;
    }

    public function setSLastName($sLastName) {
        $this->sLastName = $sLastName;
    }

    public function getSCompany() {
        return $this->sLastName;
    }

    public function setSCompany($company){
        $this->sLastName = $company;
    }

    public function getSAddress() {
        return $this->sAddress;
    }

    public function setSAddress($sAddress) {
        $this->sAddress = $sAddress;
    }

    public function getSCity() {
        return $this->sCity;
    }

    public function setSCity($sCity) {
        $this->sCity = $sCity;
    }

    public function getSState() {
        return $this->sState;
    }

    public function setSState($sState) {
        $this->sState = $sState;
    }

    public function getSZip() {
        return $this->sZip;
    }

    public function setSZip($sZip) {
        $this->sZip = $sZip;
    }

    public function getSCountry() {
        return $this->sCountry;
    }

    public function setSCountry($sCountry) {
        $this->sCountry = $sCountry;
    }

    public function getSPhone() {
        return $this->sPhone;
    }

    public function setSPhone($sPhone) {
        $this->sPhone = $sPhone;
    }

    public function getSEmail() {
        return $this->sEmail;
    }

    public function setSEmail($sEmail) {
        $this->sEmail = $sEmail;
    }

    public function getState() {
        return $this->state;
    }

    public function setState($state) {
        $this->state = $state;
    }

    public function getMethod() {
        return $this->method;
    }

    public function setMethod($method) {
        $this->method = $method;
    }

    public function getDate() {
        return $this->date;
    }

    public function getCode() {
        return $this->code;
    }

    public function setCode($code) {
        $this->code = $code;
    }
    public function getSubTotal() {
        return $this->subTotal;
    }

    public function setSubTotal($subTotal) {
        $this->subTotal = $subTotal;
    }

    public function getShipping() {
        return $this->shipping;
    }

    public function setShipping($shipping) {
        $this->shipping = $shipping;
    }

    public function getTotal() {
        return $this->total;
    }

    public function setTotal($total) {
        $this->total = $total;
    }

    public function getUserId() {
        return $this->userId;
    }

    public function setUserId($userId) {
        $this->userId = $userId;
    }


    public function setDate($rd) {
        $fechaYhora = explode(" ", $rd);
        $fecha = explode("-", $fechaYhora[0]);
        $hora = explode(":", $fechaYhora[1]);
        $this->date = $fecha[2].'/'.$fecha[1].'/'.$fecha[0];
    }
    public function getDivisa() {
        return $this->divisa;
    }

    public function setDivisa($divisa) {
        $this->divisa = $divisa;
    }

    public function full(){
       foreach (get_object_vars($this) as $key => $val) {
           $this->miss = $key;
           if($key == 'id' || $key == 'miss' || $key == 'bLastName'  ||
                   $key == 'sLastName' ||
                   $key == 'sCountry' || $key == 'bCountry')
               continue;
            if( $val == "" || $val == null )
                return false;
       }
       return true;
    }

    public function getMiss(){
        return $this->miss;
    }



}

?>