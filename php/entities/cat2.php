<?php


class cat2{
    private $id;
    private $idCat1;
    private $nombre;

    function __construct(){
        $this->id = 0;
    }

    //GET'S & SET'S
    public function getIdCat1() {
        return $this->idCat1;
    }

    public function setIdCat1($idCat1) {
        $this->idCat1 = $idCat1;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }


}

?>