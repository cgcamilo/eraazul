<?php


class mensaje{
    private $id;
    private $nombre;
    private $corta;
    private $img;
    private $fecha;
    private $dia;
    private $mes;


    function __construct(){
        $this->id = 0;
    }

    //GET'S & SET'S
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function getCorta() {
        return $this->corta;
    }

    public function setCorta($corta) {
        $this->corta = $corta;
    }

    public function getImg() {
        return $this->img;
    }

    public function setImg($img) {
        $this->img = $img;
    }

    public function getFecha() {
        return $this->fecha;
    }

    public function setFecha($rd){
        $fechaYhora = explode(" ", $rd);
        $fecha = explode("-", $fechaYhora[0]);
        $hora = explode(":", $fechaYhora[1]);
        $this->fecha = $fecha[0].'-'.$fecha[1].'-'.$fecha[2];
        $this->dia = $fecha[2];
    }

    public function getDia(){
        return $this->dia;
    }

    public function getFechaSQL(){
        list($dia, $mes, $ano)= explode("/", $this->fecha);
        return $ano.'-'.$mes.'-'.$dia;
    }

    public function getMes(){
        list($dia, $mes, $ano)= explode("/", $this->fecha);
        switch ($mes){
            case '01': return ' ENE '; break;
            case '1': return ' ENE '; break;
            case '02': return ' FEB '; break;
            case '2': return ' FEB '; break;
            case '03': return ' MAR '; break;
            case '3': return ' MAR '; break;
            case '04': return ' ABR '; break;
            case '4': return ' ABR '; break;
            case '05': return ' MAY '; break;
            case '5': return ' MAY '; break;
            case '06': return ' JUN '; break;
            case '6': return ' JUN '; break;
            case '07': return ' JUL '; break;
            case '7': return ' JUL '; break;
            case '08': return ' AGO '; break;
            case '8': return ' AGO '; break;
            case '09': return ' SEP '; break;
            case '9': return ' SEP '; break;
            case '10': return ' OCT '; break;
            case '11': return ' NOV '; break;
            case '12': return ' DIC '; break;
                default : return ' '.$mes.' '; break;
        }
    }

    public function getMesAno(){
        list($dia, $mes, $ano)= explode("/", $this->fecha);
        switch ($mes){
            case '01': return ' Enero '.$ano; break;
            case '02': return ' Febrero '.$ano; break;
            case '03': return ' Marzo '.$ano; break;
            case '04': return ' Abril '.$ano; break;
            case '05': return ' Mayo '.$ano; break;
            case '06': return ' Junio '.$ano; break;
            case '07': return ' Julio '.$ano; break;
            case '08': return ' Agosto '.$ano; break;
            case '09': return ' Septiembre '.$ano; break;
            case '10': return ' Octubre '.$ano; break;
            case '11': return ' Noviembre '.$ano; break;
            case '12': return ' Diciembre '.$ano; break;
                default : return ' '.$mes.' '; break;
        }
    }

    function getCortaShortNoTags($l = 400){
        $noHTMLText = strip_tags($this->corta);
        if( strlen($noHTMLText) > $l ){
            return substr($noHTMLText, 0, $l).'...';
        }
        else
            return $noHTMLText;
    }


}

?>