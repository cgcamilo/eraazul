<?php


class user{
    private $id;
    private $login;
    private $pass;
    private $nombre;
    private $isAdmin;
    private $apellidos;
    private $tel;
    private $dir;
    private $ciudad;
    private $pais;
    private $depto;
    private $fax;
    private $empresa;
    private $nacimiento;
    private $recibir;
    


    function __construct(){
        $this->id = 0;
        $this->login = 'null';
        $this->isAdmin = false;
    }

    //GET'S & SET'S
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getLogin() {
        return $this->login;
    }

    public function setLogin($login) {
        $this->login = $login;
    }

    public function getPass() {
        return $this->pass;
    }

    public function setPass($pass) {
        $this->pass = $pass;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($name) {
        $this->nombre = $name;
    }

    public function getIsAdmin() {
        return $this->isAdmin;
    }

    public function setIsAdmin($isAdmin) {
        $this->isAdmin = $isAdmin;
    }

    public function getApellidos() {
        return $this->apellidos;
    }

    public function setApellidos($apellidos) {
        $this->apellidos = $apellidos;
    }

    public function getTel() {
        return $this->tel;
    }

    public function setTel($tel) {
        $this->tel = $tel;
    }

    public function getDir() {
        return $this->dir;
    }

    public function setDir($dir) {
        $this->dir = $dir;
    }

    public function getCiudad() {
        return $this->ciudad;
    }

    public function setCiudad($ciudad) {
        $this->ciudad = $ciudad;
    }

    public function getPais() {
        return $this->pais;
    }

    public function setPais($pais) {
        $this->pais = $pais;
    }

    public function getDepto() {
        return $this->depto;
    }

    public function setDepto($depto) {
        $this->depto = $depto;
    }

    public function getFax() {
        return $this->fax;
    }

    public function setFax($fax) {
        $this->fax = $fax;
    }

    public function getEmpresa() {
        return $this->empresa;
    }

    public function setEmpresa($empresa) {
        $this->empresa = $empresa;
    }

    public function getNacimiento() {
        return $this->nacimiento;
    }

    public function setNacimiento($nacimiento) {
        $this->nacimiento = $nacimiento;
    }

    public function getRecibir() {
        return $this->recibir;
    }

    public function setRecibir($recibir) {
        $this->recibir = $recibir;
    }



}

?>