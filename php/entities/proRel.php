<?php


class proRel{
    private $id;
    private $idProducto;
    private $idProductoRel;


    function __construct(){
        $this->id = 0;
    }

    //GET'S & SET'S
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getIdProducto() {
        return $this->idProducto;
    }

    public function setIdProducto($idProducto) {
        $this->idProducto = $idProducto;
    }

    public function getIdProductoRel() {
        return $this->idProductoRel;
    }

    public function setIdProductoRel($idProductoRel) {
        $this->idProductoRel = $idProductoRel;
    }



}

?>