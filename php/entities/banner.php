<?php


class banner{
    private $id;
    private $nombre;
    private $url;
    private $img;


    function __construct(){
        $this->id = 0;
    }

    //GET'S & SET'S
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function getUrl() {
        return $this->url;
    }

    public function setUrl($url) {
        $this->url = $url;
    }
    public function getImg() {
        return $this->img;
    }

    public function setImg($img) {
        $this->img = $img;
    }


}

?>