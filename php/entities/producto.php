<?php


class producto{

    private $id;
    private $idCat1;
    private $idCat2;
    private $ref;
    private $nombre;
    private $corta;
    private $datos;
    private $datosTecnicos;
    private $precio;
    private $stock;

    private $cantidad;

    private $novedad;
    private $regalar;
    private $vendido;
    private $recomendado;
    private $popular;
    private $promocion;

    private $img;
    private $img1;
    private $img2;
    private $img3;
    private $img4;
    private $img5;

    private $iva;
    private $descuento;
    private $precioDescuento;

    private $idCat11;
    private $idCat12;
    private $idCat13;
    private $idCat14;
    private $idCat15;

    private $nombreSimple;

    function __construct(){
        $this->id = 0;
    }

    //GET'S & SET'S
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getIdCat1() {
        return $this->idCat1;
    }

    public function setIdCat1($idCat1) {
        $this->idCat1 = $idCat1;
    }

    public function getIdCat2() {
        return $this->idCat2;
    }

    public function setIdCat2($idCat2) {
        $this->idCat2 = $idCat2;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function getCorta() {
        return $this->corta;
    }

    public function setCorta($corta) {
        $this->corta = $corta;
    }

    public function getDatos() {
        return $this->datos;
    }

    public function setDatos($datos) {
        $this->datos = $datos;
    }

    public function getDatosTecnicos() {
        return $this->datosTecnicos;
    }

    public function setDatosTecnicos($datosTecnicos) {
        $this->datosTecnicos = $datosTecnicos;
    }

    public function getPrecio() {
        return $this->precio;
    }

    public function setPrecio($precio) {
        $this->precio = $precio;
    }

    public function getCantidad() {
        return $this->cantidad;
    }

    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }

    public function getStock() {
        return $this->stock;
    }

    public function setStock($stock) {
        $this->stock = $stock;
    }

    public function getNovedad() {
        return $this->novedad;
    }

    public function setNovedad($novedad) {
        $this->novedad = $novedad;
    }

    public function getRegalar() {
        return $this->regalar;
    }

    public function setRegalar($regalar) {
        $this->regalar = $regalar;
    }

    public function getVendido() {
        return $this->vendido;
    }

    public function setVendido($vendido) {
        $this->vendido = $vendido;
    }

    public function getRecomendado() {
        return $this->recomendado;
    }

    public function setRecomendado($recomendado) {
        $this->recomendado = $recomendado;
    }

    public function getPopular() {
        return $this->popular;
    }

    public function setPopular($popular) {
        $this->popular = $popular;
    }

    public function getPromocion() {
        return $this->promocion;
    }

    public function setPromocion($promocion) {
        $this->promocion = $promocion;
    }

    public function getImg() {
        return $this->img;
    }

    public function setImg($img) {
        $this->img = $img;
    }

    public function getImg1() {
        return $this->img1;
    }

    public function setImg1($img1) {
        $this->img1 = $img1;
    }

    public function getImg2() {
        return $this->img2;
    }

    public function setImg2($img2) {
        $this->img2 = $img2;
    }

    public function getImg3() {
        return $this->img3;
    }

    public function setImg3($img3) {
        $this->img3 = $img3;
    }

    public function getImg4() {
        return $this->img4;
    }

    public function setImg4($img4) {
        $this->img4 = $img4;
    }

    public function getImg5() {
        return $this->img5;
    }

    public function setImg5($img5) {
        $this->img5 = $img5;
    }

    public function getPrecioFormato(){
        return number_format($this->precio,  2, ',', '.');
    }

    public function getPrecioFormatoDescuento(){
        return number_format($this->precioDescuento,  2, ',', '.');
    }

    public function getRef() {
        return $this->ref;
    }

    public function setRef($ref) {
        $this->ref = $ref;
    }


    function getCortaShortNoTags($l = 400){
        $noHTMLText = strip_tags($this->corta);
        if( strlen($noHTMLText) > $l ){
            return substr($noHTMLText, 0, $l).'...';
        }
        else
            return $noHTMLText;
    }

    public function getIva() {
        return $this->iva;
    }

    public function setIva($iva) {
        $this->iva = $iva;
    }

    public function getDescuento() {
        return $this->descuento;
    }

    public function setDescuento($descuento) {
        $this->descuento = $descuento;
    }

    public function getPrecioDescuento() {
        return $this->precioDescuento;
    }

    public function setPrecioDescuento($precioDescuento) {
        $this->precioDescuento = $precioDescuento;
    }

    public function getIdCat11() {
        return $this->idCat11;
    }

    public function setIdCat11($idCat11) {
        $this->idCat11 = $idCat11;
    }

    public function getIdCat12() {
        return $this->idCat12;
    }

    public function setIdCat12($idCat12) {
        $this->idCat12 = $idCat12;
    }

    public function getIdCat13() {
        return $this->idCat13;
    }

    public function setIdCat13($idCat13) {
        $this->idCat13 = $idCat13;
    }

    public function getIdCat14() {
        return $this->idCat14;
    }

    public function setIdCat14($idCat14) {
        $this->idCat14 = $idCat14;
    }

    public function getIdCat15() {
        return $this->idCat15;
    }

    public function setIdCat15($idCat15) {
        $this->idCat15 = $idCat15;
    }

    public function getNombreSimple() {
        return $this->nombreSimple;
    }

    public function setNombreSimple($nombreSimple) {
        $this->nombreSimple = $nombreSimple;
    }



}

?>