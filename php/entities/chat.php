<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Class chat{

    private $id;
    private $usuario;
    private $mensaje;
    private $fecha;
    private $codUsuario;
    
    function __construct() {
        
    }
    
    public function getId() {
        return $this->id;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function getMensaje() {
        return $this->mensaje;
    }

    public function getFecha() {
        return $this->fecha;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    public function setMensaje($mensaje) {
        $this->mensaje = $mensaje;
    }

    public function setFecha($fecha) {
        $this->fecha = $fecha;
    }

    public function getCodUsuario() {
        return $this->codUsuario;
    }

    public function setCodUsuario($codUsuario) {
        $this->codUsuario = $codUsuario;
    }



    
}

?>
