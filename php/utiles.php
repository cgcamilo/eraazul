<?php
function currentPage() {
 $gets = '';
 foreach ($_GET as $key => $vble){
     if( $key != "error"  && $key != "lang" && $key != "pais")
     $gets .= $key.'='.$vble.'&';
 }

 return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1).'?'.$gets;
}

function POSTSAsString($array = array()){
    $arrayGets = array();
    foreach ($_POST as $key => $vbl){
        if(in_array($key, $array) )
                continue;
        $arrayGets[$key] = $key.'='.urlencode($vbl);
    }
    $stringGets = implode("&", $arrayGets);
    return $stringGets;
}

function fullUpper($string){
  return strtr(strtoupper($string), array(
      "à" => "À",
      "è" => "È",
      "ì" => "Ì",
      "ò" => "Ò",
      "ù" => "Ù",
      "á" => "Á",
      "é" => "É",
      "í" => "Í",
      "ó" => "Ó",
      "Û" => "Ú",
      "À" => "Á",
      "Ì" => "Í",
      "Ê" => "Ú",
      "Ô" => "Ó"
    ));
}

function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function getCortaShort($txt, $leng = 30){
    $noHTMLText = strip_tags($txt);
    if( strlen($noHTMLText) > $leng ){
        return substr($noHTMLText, 0, $leng).'...';
    }
    else
        return $noHTMLText;
}

function porcentaje($cantidad,$porciento,$decimales){
    return number_format($cantidad*$porciento/100 ,$decimales);
}
?>