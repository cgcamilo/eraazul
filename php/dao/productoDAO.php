<?php

class productoDAO{

    public $daoConnection;

	function __construct(){
            $this->daoConnection = new DAO;
            $this->daoConnection->conectar();
	}

    function save($producto){

        $querty =   'insert into productos
                    (idCat1, idCat2,nombre,corta, datos, datosTecnicos,
                    stock, precio,
                    novedad, regalar, vendido, recomendado, popular, promocion,
                    img, img1, img2, img3, img4, img5, ref, iva, descuento, precioDescuento,
                    idCat21,idCat22,idCat23,idCat24,idCat25, nombreSimple)
                    values(
                    "'.mysql_real_escape_string($producto->getIdCat1()).'",
                    "'.mysql_real_escape_string($producto->getIdCat2()).'",
                    "'.mysql_real_escape_string($producto->getNombre()).'",
                    "'.$producto->getCorta().'",
                    "'.$producto->getDatos().'",
                    "'.$producto->getDatosTecnicos().'",
                    "'.mysql_real_escape_string($producto->getStock()).'",
                    "'.mysql_real_escape_string($producto->getPrecio()).'",
                    "'.mysql_real_escape_string($producto->getNovedad()).'",
                    "'.mysql_real_escape_string($producto->getRegalar()).'",
                    "'.mysql_real_escape_string($producto->getVendido()).'",
                    "'.mysql_real_escape_string($producto->getRecomendado()).'",
                    "'.mysql_real_escape_string($producto->getPopular()).'",
                    "'.mysql_real_escape_string($producto->getPromocion()).'",
                    "'.mysql_real_escape_string($producto->getImg()).'",
                    "'.mysql_real_escape_string($producto->getImg1()).'",
                    "'.mysql_real_escape_string($producto->getImg2()).'",
                    "'.mysql_real_escape_string($producto->getImg3()).'",
                    "'.mysql_real_escape_string($producto->getImg4()).'",
                    "'.mysql_real_escape_string($producto->getImg5()).'",
                    "'.mysql_real_escape_string($producto->getRef()).'",
                    "'.mysql_real_escape_string($producto->getIva()).'",
                    "'.mysql_real_escape_string($producto->getDescuento()).'",
                    "'.mysql_real_escape_string($producto->getPrecioDescuento()).'",
                    "'.mysql_real_escape_string($producto->getIdCat11()).'",
                    "'.mysql_real_escape_string($producto->getIdCat12()).'",
                    "'.mysql_real_escape_string($producto->getIdCat13()).'",
                    "'.mysql_real_escape_string($producto->getIdCat14()).'",
                    "'.mysql_real_escape_string($producto->getIdCat15()).'",
                    "'.mysql_real_escape_string($producto->getNombreSimple()).'"
                    )';


        $result = mysql_query($querty, $this->daoConnection->Conexion_ID);
		if (!$result){
            echo 'Ooops (save-productos): '.mysql_error();
            return false;
        }

        return true;

    }

    function getLastId(){
        return mysql_insert_id($this->daoConnection->Conexion_ID);
    }


    function gets($order, $orderType, $l=0, $h=1000){

        $sql = 'SELECT * from productos  ';
        $sql .= 'order by '.$order.' '.$orderType.' ';
        $sql .= 'LIMIT '.$l.', '.$h;

        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

         $lista=array();

        if($numregistros == 0){
            return $lista;
        }

        for($i = 0; $i < $numregistros ; $i++){
            $producto = new producto;
            $producto->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
            $producto->setIdCat1($this->daoConnection->ObjetoConsulta2[$i][1]);
            $producto->setIdCat2($this->daoConnection->ObjetoConsulta2[$i][2]);
            $producto->setNombre($this->daoConnection->ObjetoConsulta2[$i][3]);
            $producto->setCorta($this->daoConnection->ObjetoConsulta2[$i][4]);
            $producto->setDatos($this->daoConnection->ObjetoConsulta2[$i][5]);
            $producto->setDatosTecnicos($this->daoConnection->ObjetoConsulta2[$i][6]);
            $producto->setStock($this->daoConnection->ObjetoConsulta2[$i][7]);
            $producto->setPrecio($this->daoConnection->ObjetoConsulta2[$i][8]);
            $producto->setNovedad($this->daoConnection->ObjetoConsulta2[$i][9]);
            $producto->setRegalar($this->daoConnection->ObjetoConsulta2[$i][10]);
            $producto->setVendido($this->daoConnection->ObjetoConsulta2[$i][11]);
            $producto->setRecomendado($this->daoConnection->ObjetoConsulta2[$i][12]);
            $producto->setPopular($this->daoConnection->ObjetoConsulta2[$i][13]);
            $producto->setPromocion($this->daoConnection->ObjetoConsulta2[$i][14]);
            $producto->setImg($this->daoConnection->ObjetoConsulta2[$i][15]);
            $producto->setImg1($this->daoConnection->ObjetoConsulta2[$i][16]);
            $producto->setImg2($this->daoConnection->ObjetoConsulta2[$i][17]);
            $producto->setImg3($this->daoConnection->ObjetoConsulta2[$i][18]);
            $producto->setImg4($this->daoConnection->ObjetoConsulta2[$i][19]);
            $producto->setImg5($this->daoConnection->ObjetoConsulta2[$i][20]);
            $producto->setRef($this->daoConnection->ObjetoConsulta2[$i][21]);
            $producto->setIva($this->daoConnection->ObjetoConsulta2[$i][22]);
            $producto->setDescuento($this->daoConnection->ObjetoConsulta2[$i][23]);
            $producto->setPrecioDescuento($this->daoConnection->ObjetoConsulta2[$i][24]);
            $producto->setIdCat11($this->daoConnection->ObjetoConsulta2[$i][25]);
            $producto->setIdCat12($this->daoConnection->ObjetoConsulta2[$i][26]);
            $producto->setIdCat13($this->daoConnection->ObjetoConsulta2[$i][27]);
            $producto->setIdCat14($this->daoConnection->ObjetoConsulta2[$i][28]);
            $producto->setIdCat15($this->daoConnection->ObjetoConsulta2[$i][29]);
            $producto->setNombreSimple($this->daoConnection->ObjetoConsulta2[$i][30]);

            $lista[$i] = $producto;
        }


        return $lista;
    }


    function search($order, $orderType,  $l, $h, $sqlWhere){

        $sql = 'SELECT * from productos  ';
        $sql .= 'WHERE '.$sqlWhere.' ';

        $sql .= 'order by '.$order.' '.$orderType.' ';
        $sql .= 'LIMIT '.$l.', '.$h;

        //echo $sql;

        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

         $lista=array();

        if($numregistros == 0){
            return $lista;
        }

        for($i = 0; $i < $numregistros ; $i++){
            $producto = new producto;
            $producto->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
            $producto->setIdCat1($this->daoConnection->ObjetoConsulta2[$i][1]);
            $producto->setIdCat2($this->daoConnection->ObjetoConsulta2[$i][2]);
            $producto->setNombre($this->daoConnection->ObjetoConsulta2[$i][3]);
            $producto->setCorta($this->daoConnection->ObjetoConsulta2[$i][4]);
            $producto->setDatos($this->daoConnection->ObjetoConsulta2[$i][5]);
            $producto->setDatosTecnicos($this->daoConnection->ObjetoConsulta2[$i][6]);
            $producto->setStock($this->daoConnection->ObjetoConsulta2[$i][7]);
            $producto->setPrecio($this->daoConnection->ObjetoConsulta2[$i][8]);
            $producto->setNovedad($this->daoConnection->ObjetoConsulta2[$i][9]);
            $producto->setRegalar($this->daoConnection->ObjetoConsulta2[$i][10]);
            $producto->setVendido($this->daoConnection->ObjetoConsulta2[$i][11]);
            $producto->setRecomendado($this->daoConnection->ObjetoConsulta2[$i][12]);
            $producto->setPopular($this->daoConnection->ObjetoConsulta2[$i][13]);
            $producto->setPromocion($this->daoConnection->ObjetoConsulta2[$i][14]);
            $producto->setImg($this->daoConnection->ObjetoConsulta2[$i][15]);
            $producto->setImg1($this->daoConnection->ObjetoConsulta2[$i][16]);
            $producto->setImg2($this->daoConnection->ObjetoConsulta2[$i][17]);
            $producto->setImg3($this->daoConnection->ObjetoConsulta2[$i][18]);
            $producto->setImg4($this->daoConnection->ObjetoConsulta2[$i][19]);
            $producto->setImg5($this->daoConnection->ObjetoConsulta2[$i][20]);
            $producto->setRef($this->daoConnection->ObjetoConsulta2[$i][21]);
            $producto->setIva($this->daoConnection->ObjetoConsulta2[$i][22]);
            $producto->setDescuento($this->daoConnection->ObjetoConsulta2[$i][23]);
            $producto->setPrecioDescuento($this->daoConnection->ObjetoConsulta2[$i][24]);
            $producto->setIdCat11($this->daoConnection->ObjetoConsulta2[$i][25]);
            $producto->setIdCat12($this->daoConnection->ObjetoConsulta2[$i][26]);
            $producto->setIdCat13($this->daoConnection->ObjetoConsulta2[$i][27]);
            $producto->setIdCat14($this->daoConnection->ObjetoConsulta2[$i][28]);
            $producto->setIdCat15($this->daoConnection->ObjetoConsulta2[$i][29]);
            $producto->setNombreSimple($this->daoConnection->ObjetoConsulta2[$i][30]);
            $lista[$i] = $producto;
        }


        return $lista;
    }
    

    function getById($id){

        $sql = 'SELECT * from productos WHERE id = "'.$id.'"';


        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

        $producto = new producto;

        if($numregistros == 0){
            return null;
        }

        $i = 0;
        $producto->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
            $producto->setIdCat1($this->daoConnection->ObjetoConsulta2[$i][1]);
            $producto->setIdCat2($this->daoConnection->ObjetoConsulta2[$i][2]);
            $producto->setNombre($this->daoConnection->ObjetoConsulta2[$i][3]);
            $producto->setCorta($this->daoConnection->ObjetoConsulta2[$i][4]);
            $producto->setDatos($this->daoConnection->ObjetoConsulta2[$i][5]);
            $producto->setDatosTecnicos($this->daoConnection->ObjetoConsulta2[$i][6]);
            $producto->setStock($this->daoConnection->ObjetoConsulta2[$i][7]);
            $producto->setPrecio($this->daoConnection->ObjetoConsulta2[$i][8]);
            $producto->setNovedad($this->daoConnection->ObjetoConsulta2[$i][9]);
            $producto->setRegalar($this->daoConnection->ObjetoConsulta2[$i][10]);
            $producto->setVendido($this->daoConnection->ObjetoConsulta2[$i][11]);
            $producto->setRecomendado($this->daoConnection->ObjetoConsulta2[$i][12]);
            $producto->setPopular($this->daoConnection->ObjetoConsulta2[$i][13]);
            $producto->setPromocion($this->daoConnection->ObjetoConsulta2[$i][14]);
            $producto->setImg($this->daoConnection->ObjetoConsulta2[$i][15]);
            $producto->setImg1($this->daoConnection->ObjetoConsulta2[$i][16]);
            $producto->setImg2($this->daoConnection->ObjetoConsulta2[$i][17]);
            $producto->setImg3($this->daoConnection->ObjetoConsulta2[$i][18]);
            $producto->setImg4($this->daoConnection->ObjetoConsulta2[$i][19]);
            $producto->setImg5($this->daoConnection->ObjetoConsulta2[$i][20]);
            $producto->setRef($this->daoConnection->ObjetoConsulta2[$i][21]);
            $producto->setIva($this->daoConnection->ObjetoConsulta2[$i][22]);
            $producto->setDescuento($this->daoConnection->ObjetoConsulta2[$i][23]);
            $producto->setPrecioDescuento($this->daoConnection->ObjetoConsulta2[$i][24]);
            $producto->setIdCat11($this->daoConnection->ObjetoConsulta2[$i][25]);
            $producto->setIdCat12($this->daoConnection->ObjetoConsulta2[$i][26]);
            $producto->setIdCat13($this->daoConnection->ObjetoConsulta2[$i][27]);
            $producto->setIdCat14($this->daoConnection->ObjetoConsulta2[$i][28]);
            $producto->setIdCat15($this->daoConnection->ObjetoConsulta2[$i][29]);
            $producto->setNombreSimple($this->daoConnection->ObjetoConsulta2[$i][30]);
        return $producto;
    }

    function getByRef($ref){

        $sql = 'SELECT * from productos WHERE ref = "'.$ref.'"';


        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

        $producto = new producto;

        if($numregistros == 0){
            return null;
        }

        $i = 0;
        $producto->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
            $producto->setIdCat1($this->daoConnection->ObjetoConsulta2[$i][1]);
            $producto->setIdCat2($this->daoConnection->ObjetoConsulta2[$i][2]);
            $producto->setNombre($this->daoConnection->ObjetoConsulta2[$i][3]);
            $producto->setCorta($this->daoConnection->ObjetoConsulta2[$i][4]);
            $producto->setDatos($this->daoConnection->ObjetoConsulta2[$i][5]);
            $producto->setDatosTecnicos($this->daoConnection->ObjetoConsulta2[$i][6]);
            $producto->setStock($this->daoConnection->ObjetoConsulta2[$i][7]);
            $producto->setPrecio($this->daoConnection->ObjetoConsulta2[$i][8]);
            $producto->setNovedad($this->daoConnection->ObjetoConsulta2[$i][9]);
            $producto->setRegalar($this->daoConnection->ObjetoConsulta2[$i][10]);
            $producto->setVendido($this->daoConnection->ObjetoConsulta2[$i][11]);
            $producto->setRecomendado($this->daoConnection->ObjetoConsulta2[$i][12]);
            $producto->setPopular($this->daoConnection->ObjetoConsulta2[$i][13]);
            $producto->setPromocion($this->daoConnection->ObjetoConsulta2[$i][14]);
            $producto->setImg($this->daoConnection->ObjetoConsulta2[$i][15]);
            $producto->setImg1($this->daoConnection->ObjetoConsulta2[$i][16]);
            $producto->setImg2($this->daoConnection->ObjetoConsulta2[$i][17]);
            $producto->setImg3($this->daoConnection->ObjetoConsulta2[$i][18]);
            $producto->setImg4($this->daoConnection->ObjetoConsulta2[$i][19]);
            $producto->setImg5($this->daoConnection->ObjetoConsulta2[$i][20]);
            $producto->setRef($this->daoConnection->ObjetoConsulta2[$i][21]);
            $producto->setIva($this->daoConnection->ObjetoConsulta2[$i][22]);
            $producto->setDescuento($this->daoConnection->ObjetoConsulta2[$i][23]);
            $producto->setPrecioDescuento($this->daoConnection->ObjetoConsulta2[$i][24]);
            $producto->setIdCat11($this->daoConnection->ObjetoConsulta2[$i][25]);
            $producto->setIdCat12($this->daoConnection->ObjetoConsulta2[$i][26]);
            $producto->setIdCat13($this->daoConnection->ObjetoConsulta2[$i][27]);
            $producto->setIdCat14($this->daoConnection->ObjetoConsulta2[$i][28]);
            $producto->setIdCat15($this->daoConnection->ObjetoConsulta2[$i][29]);
            $producto->setNombreSimple($this->daoConnection->ObjetoConsulta2[$i][30]);
        return $producto;
    }

    function delete($id){

        $sql = 'Delete from productos WHERE id = '.$id.' ';
        $this->daoConnection->consulta($sql);
    }

    function updateSpecial($producto){


        $querty =   'UPDATE
                     productos
                    SET
                    novedad =
                    "'.mysql_real_escape_string($producto->getNovedad()).'",
                    regalar =
                    "'.mysql_real_escape_string($producto->getRegalar()).'",
                    vendido =
                    "'.mysql_real_escape_string($producto->getVendido()).'",
                    recomendado =
                    "'.mysql_real_escape_string($producto->getRecomendado()).'",
                    descuento =
                    "'.mysql_real_escape_string($producto->getDescuento()).'",
                    popular =
                    "'.mysql_real_escape_string($producto->getPopular()).'",
                    promocion =
                    "'.mysql_real_escape_string($producto->getPromocion()).'"
                    WHERE id =
                    '.mysql_real_escape_string($producto->getId()).'
                    ';


            $result = mysql_query($querty, $this->daoConnection->Conexion_ID);

		if (!$result){
                    echo 'Ooops (update-productoso): '.mysql_error();
                    return false;
                }
        return true;
    }

    function update($producto){


        $querty =   'UPDATE
                     productos
                    SET
                    nombre =
                    "'.mysql_real_escape_string($producto->getNombre()).'",
                    ref =
                    "'.mysql_real_escape_string($producto->getRef()).'",
                    corta =
                    "'.$producto->getCorta().'",
                    idCat2 =
                    "'.mysql_real_escape_string($producto->getIdCat2()).'",
                    idCat21 =
                    "'.mysql_real_escape_string($producto->getIdCat11()).'",
                    idCat22 =
                    "'.mysql_real_escape_string($producto->getIdCat12()).'",
                    idCat23 =
                    "'.mysql_real_escape_string($producto->getIdCat13()).'",
                    idCat24 =
                    "'.mysql_real_escape_string($producto->getIdCat14()).'",
                    idCat25 =
                    "'.mysql_real_escape_string($producto->getIdCat15()).'",
                    datos =
                    "'.$producto->getDatos().'",
                    datosTecnicos =
                    "'.mysql_real_escape_string($producto->getDatosTecnicos()).'",
                    stock =
                    "'.mysql_real_escape_string($producto->getStock()).'",
                    precio =
                    "'.mysql_real_escape_string($producto->getPrecio()).'",
                    novedad =
                    "'.mysql_real_escape_string($producto->getNovedad()).'",
                    regalar =
                    "'.mysql_real_escape_string($producto->getRegalar()).'",
                    vendido =
                    "'.mysql_real_escape_string($producto->getVendido()).'",
                    recomendado =
                    "'.mysql_real_escape_string($producto->getRecomendado()).'",
                    iva =
                    "'.mysql_real_escape_string($producto->getIva()).'",
                    descuento =
                    "'.mysql_real_escape_string($producto->getDescuento()).'",
                    precioDescuento =
                    "'.mysql_real_escape_string($producto->getPrecioDescuento()).'",
                    popular =
                    "'.mysql_real_escape_string($producto->getPopular()).'",
                    promocion =
                    "'.mysql_real_escape_string($producto->getPromocion()).'",
                    nombreSimple =
                    "'.mysql_real_escape_string($producto->getNombreSimple()).'",
                    img =
                    "'.mysql_real_escape_string($producto->getImg()).'",
                    img1 =
                    "'.mysql_real_escape_string($producto->getImg1()).'",
                    img2 =
                    "'.mysql_real_escape_string($producto->getImg2()).'",
                    img3 =
                    "'.mysql_real_escape_string($producto->getImg3()).'",
                    img4 =
                    "'.mysql_real_escape_string($producto->getImg4()).'",
                    img5 =
                    "'.mysql_real_escape_string($producto->getImg5()).'"
                    WHERE id =
                    '.mysql_real_escape_string($producto->getId()).'
                    ';

            
            $result = mysql_query($querty, $this->daoConnection->Conexion_ID);

		if (!$result){
                    echo 'Ooops (update-productoso): '.mysql_error();
                    return false;
                }
        return true;
    }

    function updateNombreSimple($producto){


        $querty =   'UPDATE
                     productos
                    SET
                    nombreSimple =
                    "'.mysql_real_escape_string($producto->getNombreSimple()).'"
                    WHERE id =
                    '.mysql_real_escape_string($producto->getId()).'
                    ';


            $result = mysql_query($querty, $this->daoConnection->Conexion_ID);

		if (!$result){
                    echo 'Ooops (update-productoso): '.mysql_error();
                    return false;
                }

        return true;
    }

    function updateStock($producto){


        $querty =   'UPDATE
                     productos
                    SET
                    stock =
                    "'.mysql_real_escape_string($producto->getStock()).'"
                    WHERE id =
                    '.mysql_real_escape_string($producto->getId()).'
                    ';


            $result = mysql_query($querty, $this->daoConnection->Conexion_ID);

		if (!$result){
                    echo 'Ooops (update-productoso): '.mysql_error();
                    return false;
                }

        return true;
    }

    function total($opt = 0, $campo = 0, $valor = 0){

        if($opt == 0)
            $sql = 'select count(*) from productos;';
        if($opt == 1)
            $sql = 'select count(*) from productos where '.$campo.' LIKE "%'.$valor.'%";';
        if($opt == 2)
            $sql = 'select count(*) from productos where '.$campo.' = "'.$valor.'";';

        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();

        return $this->daoConnection->ObjetoConsulta2[0][0];
    }
}

?>