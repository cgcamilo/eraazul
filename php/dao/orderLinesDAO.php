<?php

class orderLinesDAO{

    public $daoConnection;

	function __construct(){
		$this->daoConnection = new DAO;
        $this->daoConnection->conectar();
	}

    function save($lineaPedido){

        $newLineaPedido = new orderLines;
        $newLineaPedido = $lineaPedido;

        $querty =   "insert into orderlines
                    (idOrder, idProduct, quantity, size)
                    values(
                    \"".mysql_real_escape_string($newLineaPedido->getIdOrder())."\",
                    \"".mysql_real_escape_string($newLineaPedido->getIdProduct())."\",
                    \"".mysql_real_escape_string($newLineaPedido->getQuantity())."\",
                    \"".mysql_real_escape_string($newLineaPedido->getSize())."\"
                    )";

        $result = mysql_query($querty, $this->daoConnection->Conexion_ID);
		if (!$result){
            echo 'Ooops (saveproducto): '.mysql_error();
            return false;
        }

        return true;

    }

    function getLastId(){
        return mysql_insert_id($this->daoConnection->Conexion_ID);
    }



    function gets($order, $orderType){

        $sql = 'SELECT * from orderlines  ';
        $sql .= 'order by '.$order.' '.$orderType.' ';

		$this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

         $listaCat=array();

        if($numregistros == 0){
            return $listaCat;
        }

        for($i = 0; $i < $numregistros ; $i++){
            $newLineaPedido = new orderLines;
            $newLineaPedido->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
            $newLineaPedido->setIdOrder($this->daoConnection->ObjetoConsulta2[$i][1]);
            $newLineaPedido->setIdProduct($this->daoConnection->ObjetoConsulta2[$i][2]);
            $newLineaPedido->setQuantity($this->daoConnection->ObjetoConsulta2[$i][3]);
            $newLineaPedido->setSize($this->daoConnection->ObjetoConsulta2[$i][4]);
            $listaCat[$i] = $newLineaPedido;
        }


        return $listaCat;
    }

    function getsByPedido($order, $orderType, $pedidoId){

        $sql = 'SELECT * from orderlines WHERE  idOrder = "'.$pedidoId.'"  ';
        $sql .= 'order by '.$order.' '.$orderType.' ';

		$this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

         $listaCat=array();

        if($numregistros == 0){
            return $listaCat;
        }

        for($i = 0; $i < $numregistros ; $i++){
            $newLineaPedido = new orderLines;
            $newLineaPedido->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
            $newLineaPedido->setIdOrder($this->daoConnection->ObjetoConsulta2[$i][1]);
            $newLineaPedido->setIdProduct($this->daoConnection->ObjetoConsulta2[$i][2]);
            $newLineaPedido->setQuantity($this->daoConnection->ObjetoConsulta2[$i][3]);
            $newLineaPedido->setSize($this->daoConnection->ObjetoConsulta2[$i][4]);
            $listaCat[$i] = $newLineaPedido;
        }


        return $listaCat;
    }


    function getById($id){

        $sql = 'SELECT * from orderlines WHERE id = "'.$id.'"';

		$this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

        $newLineaPedido = new orderLines;

        if($numregistros == 0){
            return null;
        }

        $i=0;
        $newLineaPedido->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
        $newLineaPedido->setIdOrder($this->daoConnection->ObjetoConsulta2[$i][1]);
        $newLineaPedido->setIdProduct($this->daoConnection->ObjetoConsulta2[$i][2]);
        $newLineaPedido->setQuantity($this->daoConnection->ObjetoConsulta2[$i][3]);
        $newLineaPedido->setSize($this->daoConnection->ObjetoConsulta2[$i][4]);
        return $newLineaPedido;
    }


    function delete($id){

        $sql = 'Delete from orderlines WHERE id = '.$id.' ';

		$this->daoConnection->consulta($sql);
    }


    function total($opt = 0, $campo = 0, $valor = 0){

		if($opt == 0)
			$sql = 'select count(*) from orderlines;';
		if($opt == 1)
			$sql = 'select count(*) from orderlines where '.$campo.' LIKE "%'.$valor.'%";';

        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();

		return $this->daoConnection->ObjetoConsulta2[0][0];
	}


}

?>
