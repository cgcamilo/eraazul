<?php

class publicidadDAO{

    public $daoConnection;

	function __construct(){
            $this->daoConnection = new DAO;
            $this->daoConnection->conectar();
	}

    function save($publicidad){

        $querty =   'insert into publicidad
                    (nombre, url, img)
                    values(
                    "'.mysql_real_escape_string($publicidad->getNombre()).'",
                    "'.mysql_real_escape_string($publicidad->getUrl()).'",
                    "'.mysql_real_escape_string($publicidad->getImg()).'"
                    )';

                    //echo $querty;
        $result = mysql_query($querty, $this->daoConnection->Conexion_ID);
		if (!$result){
            echo 'Ooops (savepublicidado): '.mysql_error();
            return false;
        }

        return true;

    }

    function getLastId(){
        return mysql_insert_id($this->daoConnection->Conexion_ID);
    }



    function gets($order, $orderType, $l=0, $h=1000){

        $sql = 'SELECT * from publicidad  ';
        $sql .= 'order by '.$order.' '.$orderType.' ';
        $sql .= 'LIMIT '.$l.', '.$h;

        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

         $lista=array();

        if($numregistros == 0){
            return $lista;
        }

        for($i = 0; $i < $numregistros ; $i++){
            $banner = new banner;
            $banner->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
            $banner->setNombre($this->daoConnection->ObjetoConsulta2[$i][1]);
            $banner->setUrl($this->daoConnection->ObjetoConsulta2[$i][2]);
            $banner->setImg($this->daoConnection->ObjetoConsulta2[$i][3]);
            $lista[$i] = $banner;
        }


        return $lista;
    }


    function getById($id){

        $sql = 'SELECT * from publicidad WHERE id = "'.$id.'"';


        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

        $banner = new banner;

        if($numregistros == 0){
            return null;
        }

        $i = 0;
        $banner->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
        $banner->setNombre($this->daoConnection->ObjetoConsulta2[$i][1]);
        $banner->setUrl($this->daoConnection->ObjetoConsulta2[$i][2]);
        $banner->setImg($this->daoConnection->ObjetoConsulta2[$i][3]);
        return $banner;
    }



    function delete($id){

        $sql = 'Delete from publicidad WHERE id = '.$id.' ';
        $this->daoConnection->consulta($sql);
    }

    function update($banner){


        $querty =   'UPDATE
                     publicidad
                    SET
                    nombre =
                    "'.mysql_real_escape_string($banner->getNombre()).'",
                    url =
                    "'.mysql_real_escape_string($banner->getUrl()).'",
                    img =
                    "'.mysql_real_escape_string($banner->getImg()).'"
                    WHERE id =
                    '.mysql_real_escape_string($banner->getId()).'
                    ';

            //echo $querty.'<br />';
            $result = mysql_query($querty, $this->daoConnection->Conexion_ID);

		if (!$result){
                    echo 'Ooops (update-publicidado): '.mysql_error();
                    return false;
                }

        return true;
    }

    function total($opt = 0, $campo = 0, $valor = 0){

        if($opt == 0)
            $sql = 'select count(*) from publicidad;';
        if($opt == 1)
            $sql = 'select count(*) from publicidad where '.$campo.' LIKE "%'.$valor.'%";';
        if($opt == 2)
            $sql = 'select count(*) from publicidad where '.$campo.' = "'.$valor.'";';

        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();

        return $this->daoConnection->ObjetoConsulta2[0][0];
    }
}

?>