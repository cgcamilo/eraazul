<?php

class cat2DAO{

    public $daoConnection;

	function __construct(){
            $this->daoConnection = new DAO;
            $this->daoConnection->conectar();
	}

    function save($cat2){

        $querty =   'insert into cat2
                    (idcat1, nombre)
                    values(
                    "'.mysql_real_escape_string($cat2->getIdCat1()).'",
                    "'.mysql_real_escape_string($cat2->getNombre()).'"
                    )';

                    //echo $querty;
        $result = mysql_query($querty, $this->daoConnection->Conexion_ID);
		if (!$result){
            echo 'Ooops (savecat2o): '.mysql_error();
            return false;
        }

        return true;

    }

    function getLastId(){
        return mysql_insert_id($this->daoConnection->Conexion_ID);
    }



    function gets($order, $orderType, $l=0, $h=1000){

        $sql = 'SELECT * from cat2  ';
        $sql .= 'order by '.$order.' '.$orderType.' ';
        $sql .= 'LIMIT '.$l.', '.$h;

        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

         $lista=array();

        if($numregistros == 0){
            return $lista;
        }

        for($i = 0; $i < $numregistros ; $i++){
            $cat2 = new cat2;
            $cat2->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
            $cat2->setIdCat1($this->daoConnection->ObjetoConsulta2[$i][1]);
            $cat2->setNombre($this->daoConnection->ObjetoConsulta2[$i][2]);
            $lista[$i] = $cat2;
        }


        return $lista;
    }


    function getsByIdCat1($order, $orderType, $idCat1, $l=0, $h=1000){

        $sql = 'SELECT * from cat2  WHERE idcat1 = "'.$idCat1.'" ';
        $sql .= 'order by '.$order.' '.$orderType.' ';
        $sql .= 'LIMIT '.$l.', '.$h;

        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

         $lista=array();

        if($numregistros == 0){
            return $lista;
        }

        for($i = 0; $i < $numregistros ; $i++){
            $cat2 = new cat2;
            $cat2->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
            $cat2->setIdCat1($this->daoConnection->ObjetoConsulta2[$i][1]);
            $cat2->setNombre($this->daoConnection->ObjetoConsulta2[$i][2]);
            $lista[$i] = $cat2;
        }


        return $lista;
    }


    function getById($id){

        $sql = 'SELECT * from cat2 WHERE id = "'.$id.'"';


        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

        $cat2 = new cat2;

        if($numregistros == 0){
            return null;
        }

        $i = 0;
        $cat2->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
        $cat2->setIdCat1($this->daoConnection->ObjetoConsulta2[$i][1]);
        $cat2->setNombre($this->daoConnection->ObjetoConsulta2[$i][2]);
        return $cat2;
    }



    function delete($id){

        $sql = 'Delete from cat2 WHERE id = '.$id.' ';
        $this->daoConnection->consulta($sql);
    }

    function update($cat2){


        $querty =   'UPDATE
                     cat2
                    SET
                    nombre =
                    "'.mysql_real_escape_string($cat2->getNombre()).'"
                    WHERE id =
                    '.mysql_real_escape_string($cat2->getId()).'
                    ';

            //echo $querty.'<br />';
            $result = mysql_query($querty, $this->daoConnection->Conexion_ID);

		if (!$result){
                    echo 'Ooops (update-cat2o): '.mysql_error();
                    return false;
                }

        return true;
    }

    function total($opt = 0, $campo = 0, $valor = 0){

        if($opt == 0)
            $sql = 'select count(*) from cat2;';
        if($opt == 1)
            $sql = 'select count(*) from cat2 where '.$campo.' LIKE "%'.$valor.'%";';
        if($opt == 2)
            $sql = 'select count(*) from cat2 where '.$campo.' = "'.$valor.'";';

        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();

        return $this->daoConnection->ObjetoConsulta2[0][0];
    }
}

?>