<?php

class mensajeDAO{

    public $daoConnection;

	function __construct(){
            $this->daoConnection = new DAO;
            $this->daoConnection->conectar();
	}

    function save($mensaje){

        $querty =   'insert into mensaje
                    (nombre, corta, img,fecha)
                    values(
                    "'.mysql_real_escape_string($mensaje->getNombre()).'",
                    "'.mysql_real_escape_string($mensaje->getCorta()).'",
                    "'.mysql_real_escape_string($mensaje->getImg()).'",
                    "'.mysql_real_escape_string($mensaje->getFecha()).'"
                    )';

                 //  echo $querty;
        $result = mysql_query($querty, $this->daoConnection->Conexion_ID);
		if (!$result){
            echo 'Ooops (savemensajeo): '.mysql_error();
            return false;
        }

        return true;

    }

    function getLastId(){
        return mysql_insert_id($this->daoConnection->Conexion_ID);
    }



    function gets($order, $orderType, $l=0, $h=1000){

        $sql = 'SELECT * from mensaje  ';
        $sql .= 'order by '.$order.' '.$orderType.' ';
        $sql .= 'LIMIT '.$l.', '.$h;

        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

         $lista=array();

        if($numregistros == 0){
            return $lista;
        }

        for($i = 0; $i < $numregistros ; $i++){
            $mensaje = new mensaje;
            $mensaje->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
            $mensaje->setNombre($this->daoConnection->ObjetoConsulta2[$i][1]);
            $mensaje->setCorta($this->daoConnection->ObjetoConsulta2[$i][2]);
            $mensaje->setImg($this->daoConnection->ObjetoConsulta2[$i][3]);
            $mensaje->setFecha($this->daoConnection->ObjetoConsulta2[$i][4]);
            $lista[$i] = $mensaje;
        }


        return $lista;
    }

    function getsFecha($order, $orderType, $fecha1, $fecha2, $l=0, $h=1000){

        $sql = 'SELECT * from mensaje  ';
        $sql .= 'WHERE fecha between "'.$fecha1.' 00:00:00" AND "'.$fecha2.' 23:59:50"';
        $sql .= 'order by '.$order.' '.$orderType.' ';
        $sql .= 'LIMIT '.$l.', '.$h;

        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

         $lista=array();

        if($numregistros == 0){
            return $lista;
        }

        for($i = 0; $i < $numregistros ; $i++){
            $mensaje = new mensaje;
            $mensaje->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
            $mensaje->setNombre($this->daoConnection->ObjetoConsulta2[$i][1]);
            $mensaje->setCorta($this->daoConnection->ObjetoConsulta2[$i][2]);
            $mensaje->setImg($this->daoConnection->ObjetoConsulta2[$i][3]);
            $mensaje->setFecha($this->daoConnection->ObjetoConsulta2[$i][4]);
            $lista[$i] = $mensaje;
        }


        return $lista;
    }
    function getByFecha($id){

        $sql = 'SELECT * from mensaje WHERE fecha = "'.$id.'"';
        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

        $mensaje = new mensaje;

        if($numregistros == 0){
            return null;
        }

        $i = 0;
        $mensaje->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
        $mensaje->setNombre($this->daoConnection->ObjetoConsulta2[$i][1]);
        $mensaje->setCorta($this->daoConnection->ObjetoConsulta2[$i][2]);
        $mensaje->setImg($this->daoConnection->ObjetoConsulta2[$i][3]);
        $mensaje->setFecha($this->daoConnection->ObjetoConsulta2[$i][4]);
        return $mensaje;
    }


    function getById($id){

        $sql = 'SELECT * from mensaje WHERE id = "'.$id.'"';


        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

        $mensaje = new mensaje;

        if($numregistros == 0){
            return null;
        }

        $i = 0;
        $mensaje->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
        $mensaje->setNombre($this->daoConnection->ObjetoConsulta2[$i][1]);
        $mensaje->setCorta($this->daoConnection->ObjetoConsulta2[$i][2]);
        $mensaje->setImg($this->daoConnection->ObjetoConsulta2[$i][3]);
        $mensaje->setFecha($this->daoConnection->ObjetoConsulta2[$i][4]);
        return $mensaje;
    }



    function delete($id){

        $sql = 'Delete from mensaje WHERE id = '.$id.' ';
        $this->daoConnection->consulta($sql);
    }

    function update($mensaje){


        $querty =   'UPDATE
                     mensaje
                    SET
                    nombre =
                    "'.mysql_real_escape_string($mensaje->getNombre()).'",
                    corta =
                    "'.mysql_real_escape_string($mensaje->getCorta()).'",
                    img =
                    "'.mysql_real_escape_string($mensaje->getImg()).'",
					 fecha =
                    "'.mysql_real_escape_string($mensaje->getFecha()).'"
                    WHERE id =
                    '.mysql_real_escape_string($mensaje->getId()).'
                    ';

            $result = mysql_query($querty, $this->daoConnection->Conexion_ID);

		if (!$result){
                    echo 'Ooops (update-mensajeo): '.mysql_error();
                    return false;
                }

        return true;
    }

    function total($opt = 0, $campo = 0, $valor = 0){

        if($opt == 0)
            $sql = 'select count(*) from mensaje;';
        if($opt == 1)
            $sql = 'select count(*) from mensaje where '.$campo.' LIKE "%'.$valor.'%";';
        if($opt == 2)
            $sql = 'select count(*) from mensaje where '.$campo.' = "'.$valor.'";';

        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();

        return $this->daoConnection->ObjetoConsulta2[0][0];
    }
}

?>