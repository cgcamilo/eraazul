<?php

class orderDAO{

    public $daoConnection;

	function __construct(){
		$this->daoConnection = new DAO;
        $this->daoConnection->conectar();
	}

    function save($order){

        $newOrder = new order;
        $newOrder = $order;

        $querty =   "insert into orders 
                    (bname, blastname, baddress, bcity, bstate, bzip,
                    bcountry, bphone, bemail,
                    sname, slastname, saddress, scity, sstate, szip,
                    scountry, sphone, semail, method, subtotal, shipping, total, idUser, divisa)
                    values(
                    \"".mysql_real_escape_string($newOrder->getBName())."\",
                    \"".mysql_real_escape_string($newOrder->getBLastName())."\",
                    \"".mysql_real_escape_string($newOrder->getBAddress())."\",
                    \"".mysql_real_escape_string($newOrder->getBCity())."\",
                    \"".mysql_real_escape_string($newOrder->getBState())."\",
                    \"".mysql_real_escape_string($newOrder->getBZip())."\",
                    \"".mysql_real_escape_string($newOrder->getBCountry())."\",
                    \"".mysql_real_escape_string($newOrder->getBPhone())."\",
                    \"".mysql_real_escape_string($newOrder->getBEmail())."\",
                    \"".mysql_real_escape_string($newOrder->getSName())."\",
                    \"".mysql_real_escape_string($newOrder->getSLastName())."\",
                    \"".mysql_real_escape_string($newOrder->getSAddress())."\",
                    \"".mysql_real_escape_string($newOrder->getSCity())."\",
                    \"".mysql_real_escape_string($newOrder->getSState())."\",
                    \"".mysql_real_escape_string($newOrder->getSZip())."\",
                    \"".mysql_real_escape_string($newOrder->getSCountry())."\",
                    \"".mysql_real_escape_string($newOrder->getSPhone())."\",
                    \"".mysql_real_escape_string($newOrder->getSEmail())."\",
                    \"".mysql_real_escape_string($newOrder->getMethod())."\",
                    \"".mysql_real_escape_string($newOrder->getSubTotal())."\",
                    \"".mysql_real_escape_string($newOrder->getShipping())."\",
                    \"".mysql_real_escape_string($newOrder->getTotal())."\",
                    \"".mysql_real_escape_string($newOrder->getUserId())."\",
                    \"".mysql_real_escape_string($newOrder->getDivisa())."\"
                    )";

        $result = mysql_query($querty, $this->daoConnection->Conexion_ID);
		if (!$result){
            echo 'Ooops (saveOrder): '.mysql_error();
            return false;
        }

        return true;

    }

    function getLastId(){
        return mysql_insert_id($this->daoConnection->Conexion_ID);
    }


    function gets($order, $orderType){

        $sql = 'SELECT * from orders  ';
        $sql .= 'order by '.$order.' '.$orderType.' ';

		$this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

         $listaCat=array();

        if($numregistros == 0){
            return $listaCat;
        }

        for($i = 0; $i < $numregistros ; $i++){
            $newOrder = new order;
            $newOrder->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
            $newOrder->setState($this->daoConnection->ObjetoConsulta2[$i][1]);
            
            $newOrder->setBName($this->daoConnection->ObjetoConsulta2[$i][2]);
            $newOrder->setBLastName($this->daoConnection->ObjetoConsulta2[$i][3]);
            $newOrder->setBAddress($this->daoConnection->ObjetoConsulta2[$i][4]);
            $newOrder->setBCity($this->daoConnection->ObjetoConsulta2[$i][5]);
            $newOrder->setBState($this->daoConnection->ObjetoConsulta2[$i][6]);
            $newOrder->setBZip($this->daoConnection->ObjetoConsulta2[$i][7]);
            $newOrder->setBCountry($this->daoConnection->ObjetoConsulta2[$i][8]);
            $newOrder->setBPhone($this->daoConnection->ObjetoConsulta2[$i][9]);
            $newOrder->setBEmail($this->daoConnection->ObjetoConsulta2[$i][10]);

            $newOrder->setSName($this->daoConnection->ObjetoConsulta2[$i][11]);
            $newOrder->setSLastName($this->daoConnection->ObjetoConsulta2[$i][12]);
            $newOrder->setSAddress($this->daoConnection->ObjetoConsulta2[$i][13]);
            $newOrder->setSCity($this->daoConnection->ObjetoConsulta2[$i][14]);
            $newOrder->setSState($this->daoConnection->ObjetoConsulta2[$i][15]);
            $newOrder->setSZip($this->daoConnection->ObjetoConsulta2[$i][16]);
            $newOrder->setSCountry($this->daoConnection->ObjetoConsulta2[$i][17]);
            $newOrder->setSPhone($this->daoConnection->ObjetoConsulta2[$i][18]);
            $newOrder->setSEmail($this->daoConnection->ObjetoConsulta2[$i][19]);
            
            $newOrder->setDate($this->daoConnection->ObjetoConsulta2[$i][20]);
            $newOrder->setMethod($this->daoConnection->ObjetoConsulta2[$i][21]);
            $newOrder->setCode($this->daoConnection->ObjetoConsulta2[$i][22]);
            $newOrder->setSubTotal($this->daoConnection->ObjetoConsulta2[$i][23]);
            $newOrder->setShipping($this->daoConnection->ObjetoConsulta2[$i][24]);
            $newOrder->setTotal($this->daoConnection->ObjetoConsulta2[$i][25]);
            $newOrder->setUserId($this->daoConnection->ObjetoConsulta2[$i][26]);
            $newOrder->setDivisa($this->daoConnection->ObjetoConsulta2[$i][27]);

            $listaCat[$i] = $newOrder;
        }


        return $listaCat;
    }

    function getsByUser($order, $orderType, $userId){

        $sql = 'SELECT * from orders WHERE  idUser = "'.$userId.'"  ';
        $sql .= 'order by '.$order.' '.$orderType.' ';

		$this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

         $listaCat=array();

        if($numregistros == 0){
            return $listaCat;
        }

        for($i = 0; $i < $numregistros ; $i++){
            $newOrder = new order;
            $newOrder->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
            $newOrder->setState($this->daoConnection->ObjetoConsulta2[$i][1]);

            $newOrder->setBName($this->daoConnection->ObjetoConsulta2[$i][2]);
            $newOrder->setBLastName($this->daoConnection->ObjetoConsulta2[$i][3]);
            $newOrder->setBAddress($this->daoConnection->ObjetoConsulta2[$i][4]);
            $newOrder->setBCity($this->daoConnection->ObjetoConsulta2[$i][5]);
            $newOrder->setBState($this->daoConnection->ObjetoConsulta2[$i][6]);
            $newOrder->setBZip($this->daoConnection->ObjetoConsulta2[$i][7]);
            $newOrder->setBCountry($this->daoConnection->ObjetoConsulta2[$i][8]);
            $newOrder->setBPhone($this->daoConnection->ObjetoConsulta2[$i][9]);
            $newOrder->setBEmail($this->daoConnection->ObjetoConsulta2[$i][10]);

            $newOrder->setSName($this->daoConnection->ObjetoConsulta2[$i][11]);
            $newOrder->setSLastName($this->daoConnection->ObjetoConsulta2[$i][12]);
            $newOrder->setSAddress($this->daoConnection->ObjetoConsulta2[$i][13]);
            $newOrder->setSCity($this->daoConnection->ObjetoConsulta2[$i][14]);
            $newOrder->setSState($this->daoConnection->ObjetoConsulta2[$i][15]);
            $newOrder->setSZip($this->daoConnection->ObjetoConsulta2[$i][16]);
            $newOrder->setSCountry($this->daoConnection->ObjetoConsulta2[$i][17]);
            $newOrder->setSPhone($this->daoConnection->ObjetoConsulta2[$i][18]);
            $newOrder->setSEmail($this->daoConnection->ObjetoConsulta2[$i][19]);

            $newOrder->setDate($this->daoConnection->ObjetoConsulta2[$i][20]);
            $newOrder->setMethod($this->daoConnection->ObjetoConsulta2[$i][21]);
            $newOrder->setCode($this->daoConnection->ObjetoConsulta2[$i][22]);
            $newOrder->setSubTotal($this->daoConnection->ObjetoConsulta2[$i][23]);
            $newOrder->setShipping($this->daoConnection->ObjetoConsulta2[$i][24]);
            $newOrder->setTotal($this->daoConnection->ObjetoConsulta2[$i][25]);
            $newOrder->setUserId($this->daoConnection->ObjetoConsulta2[$i][26]);
            $newOrder->setDivisa($this->daoConnection->ObjetoConsulta2[$i][27]);
            $listaCat[$i] = $newOrder;
        }


        return $listaCat;
    }

    function getByCode($code){

        $sql = 'SELECT * from orders WHERE code = "'.$code.'"';

		$this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

        $newOrder = new order;

        if($numregistros == 0){
            return null;
        }

        $i=0;
        $newOrder->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
        $newOrder->setState($this->daoConnection->ObjetoConsulta2[$i][1]);

        $newOrder->setBName($this->daoConnection->ObjetoConsulta2[$i][2]);
        $newOrder->setBLastName($this->daoConnection->ObjetoConsulta2[$i][3]);
        $newOrder->setBAddress($this->daoConnection->ObjetoConsulta2[$i][4]);
        $newOrder->setBCity($this->daoConnection->ObjetoConsulta2[$i][5]);
        $newOrder->setBState($this->daoConnection->ObjetoConsulta2[$i][6]);
        $newOrder->setBZip($this->daoConnection->ObjetoConsulta2[$i][7]);
        $newOrder->setBCountry($this->daoConnection->ObjetoConsulta2[$i][8]);
        $newOrder->setBPhone($this->daoConnection->ObjetoConsulta2[$i][9]);
        $newOrder->setBEmail($this->daoConnection->ObjetoConsulta2[$i][10]);

        $newOrder->setSName($this->daoConnection->ObjetoConsulta2[$i][11]);
        $newOrder->setSLastName($this->daoConnection->ObjetoConsulta2[$i][12]);
        $newOrder->setSAddress($this->daoConnection->ObjetoConsulta2[$i][13]);
        $newOrder->setSCity($this->daoConnection->ObjetoConsulta2[$i][14]);
        $newOrder->setSState($this->daoConnection->ObjetoConsulta2[$i][15]);
        $newOrder->setSZip($this->daoConnection->ObjetoConsulta2[$i][16]);
        $newOrder->setSCountry($this->daoConnection->ObjetoConsulta2[$i][17]);
        $newOrder->setSPhone($this->daoConnection->ObjetoConsulta2[$i][18]);
        $newOrder->setSEmail($this->daoConnection->ObjetoConsulta2[$i][19]);

        $newOrder->setDate($this->daoConnection->ObjetoConsulta2[$i][20]);
        $newOrder->setMethod($this->daoConnection->ObjetoConsulta2[$i][21]);
        $newOrder->setCode($this->daoConnection->ObjetoConsulta2[$i][22]);
        $newOrder->setSubTotal($this->daoConnection->ObjetoConsulta2[$i][23]);
        $newOrder->setShipping($this->daoConnection->ObjetoConsulta2[$i][24]);
        $newOrder->setTotal($this->daoConnection->ObjetoConsulta2[$i][25]);
        $newOrder->setUserId($this->daoConnection->ObjetoConsulta2[$i][26]);
        $newOrder->setDivisa($this->daoConnection->ObjetoConsulta2[$i][27]);
        return $newOrder;
    }


    function getById($id){

        $sql = 'SELECT * from orders WHERE id = "'.$id.'"';

		$this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

        $newOrder = new order;

        if($numregistros == 0){
            return null;
        }

        $i=0;
        $newOrder->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
        $newOrder->setState($this->daoConnection->ObjetoConsulta2[$i][1]);

        $newOrder->setBName($this->daoConnection->ObjetoConsulta2[$i][2]);
        $newOrder->setBLastName($this->daoConnection->ObjetoConsulta2[$i][3]);
        $newOrder->setBAddress($this->daoConnection->ObjetoConsulta2[$i][4]);
        $newOrder->setBCity($this->daoConnection->ObjetoConsulta2[$i][5]);
        $newOrder->setBState($this->daoConnection->ObjetoConsulta2[$i][6]);
        $newOrder->setBZip($this->daoConnection->ObjetoConsulta2[$i][7]);
        $newOrder->setBCountry($this->daoConnection->ObjetoConsulta2[$i][8]);
        $newOrder->setBPhone($this->daoConnection->ObjetoConsulta2[$i][9]);
        $newOrder->setBEmail($this->daoConnection->ObjetoConsulta2[$i][10]);

        $newOrder->setSName($this->daoConnection->ObjetoConsulta2[$i][11]);
        $newOrder->setSLastName($this->daoConnection->ObjetoConsulta2[$i][12]);
        $newOrder->setSAddress($this->daoConnection->ObjetoConsulta2[$i][13]);
        $newOrder->setSCity($this->daoConnection->ObjetoConsulta2[$i][14]);
        $newOrder->setSState($this->daoConnection->ObjetoConsulta2[$i][15]);
        $newOrder->setSZip($this->daoConnection->ObjetoConsulta2[$i][16]);
        $newOrder->setSCountry($this->daoConnection->ObjetoConsulta2[$i][17]);
        $newOrder->setSPhone($this->daoConnection->ObjetoConsulta2[$i][18]);
        $newOrder->setSEmail($this->daoConnection->ObjetoConsulta2[$i][19]);

        $newOrder->setDate($this->daoConnection->ObjetoConsulta2[$i][20]);
        $newOrder->setMethod($this->daoConnection->ObjetoConsulta2[$i][21]);
        $newOrder->setCode($this->daoConnection->ObjetoConsulta2[$i][22]);
        $newOrder->setSubTotal($this->daoConnection->ObjetoConsulta2[$i][23]);
        $newOrder->setShipping($this->daoConnection->ObjetoConsulta2[$i][24]);
        $newOrder->setTotal($this->daoConnection->ObjetoConsulta2[$i][25]);
        $newOrder->setUserId($this->daoConnection->ObjetoConsulta2[$i][26]);
        $newOrder->setDivisa($this->daoConnection->ObjetoConsulta2[$i][27]);
        return $newOrder;
    }

    function update($order){


        $querty =   'UPDATE
                     orders
                    SET
                    code =
                    "'.mysql_real_escape_string($order->getCode()).'",
                    state =
                    "'.mysql_real_escape_string($order->getState()).'"
                    WHERE id =
                    '.mysql_real_escape_string($order->getId()).'
                    ';
        //echo $querty.'<br />';
        $result = mysql_query($querty, $this->daoConnection->Conexion_ID);
		if (!$result){
            echo 'Ooops (updateOrder): '.mysql_error();
            return false;
        }

        return true;
    }


    function delete($id){

        $sql = 'Delete from orders WHERE id = '.$id.' ';

		$this->daoConnection->consulta($sql);
    }

    function totalAmount(){
        $sql = 'select sum(total) from  orders ';
        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();
        $newOrder = new order;
        if($numregistros == 0){
            return 00;
        }
        $i=0;
        return $this->daoConnection->ObjetoConsulta2[0][0];
    }

    function totalAmountUser($idUser){
        $sql = 'select sum(total) from  orders WHERE idUser = "'.$idUser.'"';
        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();
        $newOrder = new order;
        if($numregistros == 0){
            return 00;
        }
        $i=0;
        return $this->daoConnection->ObjetoConsulta2[0][0];
    }

    function total($opt = 0, $campo = 0, $valor = 0){

		if($opt == 0)
			$sql = 'select count(*) from orders;';
		if($opt == 1)
			$sql = 'select count(*) from orders where '.$campo.' LIKE "%'.$valor.'%";';

        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();

		return $this->daoConnection->ObjetoConsulta2[0][0];
	}


}

?>