<?php
class userDAO{

    public $daoConnection;

    function __construct(){
        $this->daoConnection = new DAO;
        $this->daoConnection->conectar();
    }


    function save($user){

        $querty =   'insert into usuarios
                    (login,pass,nombre,apellidos,
                    tel,dir,ciudad,depto,pais,fax,empresa,nacimiento,recibir)
                    values(
                    "'.mysql_real_escape_string($user->getLogin()).'",
                    "'.mysql_real_escape_string($user->getPass()).'",
                    "'.mysql_real_escape_string($user->getNombre()).'",
                    "'.mysql_real_escape_string($user->getApellidos()).'",
                    "'.mysql_real_escape_string($user->getTel()).'",
                    "'.mysql_real_escape_string($user->getDir()).'",
                    "'.mysql_real_escape_string($user->getCiudad()).'",
                    "'.mysql_real_escape_string($user->getDepto()).'",
                    "'.mysql_real_escape_string($user->getPais()).'",
                    "'.mysql_real_escape_string($user->getFax()).'",
                    "'.mysql_real_escape_string($user->getEmpresa()).'",
                    "'.mysql_real_escape_string($user->getNacimiento()).'",
                    "'.mysql_real_escape_string($user->getRecibir()).'"
                    )';

                   // echo $querty;
        $result = mysql_query($querty, $this->daoConnection->Conexion_ID);
		if (!$result){
            echo 'Ooops (savecat2o): '.mysql_error();
            return false;
        }

        return true;

    }

    function getLastId(){
        return mysql_insert_id($this->daoConnection->Conexion_ID);
    }

    function getUser($id){

        $newUser = new user;

        $sql = 'SELECT * from usuarios where id = "'.mysql_real_escape_string($id).'"';

		$this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

        if($numregistros == 0){
            return $newUser;
        }

        $i = 0;
        $j = 0;
        $newUser->setId($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setLogin($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setPass($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setIsAdmin($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setNombre($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setApellidos($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setTel($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setDir($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setCiudad($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setDepto($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setPais($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setFax($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setEmpresa($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setNacimiento($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setRecibir($this->daoConnection->ObjetoConsulta2[$i][$j++]);

        return $newUser;

    }

    function getUserByLogin($login){

        $newUser = new user;

        $sql = 'SELECT * from usuarios where login = "'.mysql_real_escape_string($login).'"';

		$this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

        if($numregistros == 0){
            return null;
        }

        $i = 0;
        $j = 0;
        $newUser->setId($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setLogin($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setPass($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setIsAdmin($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setNombre($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setApellidos($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setTel($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setDir($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setCiudad($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setDepto($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setPais($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setFax($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setEmpresa($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setNacimiento($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setRecibir($this->daoConnection->ObjetoConsulta2[$i][$j++]);

        //$userToPoblate = $newUser;
        return $newUser;

    }


    function getById($id){

        $newUser = new user;

        $sql = 'SELECT * from usuarios where id = "'.mysql_real_escape_string($id).'"';

		$this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

        if($numregistros == 0){
            return null;
        }

        $i = 0;
        $j = 0;
        $newUser->setId($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setLogin($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setPass($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setIsAdmin($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setNombre($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setApellidos($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setTel($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setDir($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setCiudad($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setDepto($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setPais($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setFax($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setEmpresa($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setNacimiento($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setRecibir($this->daoConnection->ObjetoConsulta2[$i][$j++]);

        //$userToPoblate = $newUser;
        return $newUser;

    }

    function getAdmin(){

        $newUser = new user;

        $sql = 'SELECT * from usuarios where isAdmin = "1"';

		$this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

        if($numregistros == 0){
            return null;
        }

        $i = 0;
        $j = 0;
        $newUser->setId($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setLogin($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setPass($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setIsAdmin($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setNombre($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setApellidos($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setTel($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setDir($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setCiudad($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setDepto($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setPais($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setFax($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setEmpresa($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setNacimiento($this->daoConnection->ObjetoConsulta2[$i][$j++]);
        $newUser->setRecibir($this->daoConnection->ObjetoConsulta2[$i][$j++]);

        //$userToPoblate = $newUser;
        return $newUser;

    }

 
    function getUsers($order, $orderType, $ll, $hl){

        $sql = 'SELECT * from usuarios order by '.$order.' '.$orderType.' ';
        $sql .= 'LIMIT '.$ll.','.$hl;

		$this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

        if($numregistros == 0){
            return $newUser;
        }
        
        //$listaUsers[];
        $listaUsers=array();

        for($i = 0; $i < $numregistros ; $i++){
            $newUser = new user;
            $j = 0;
            $newUser->setId($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setLogin($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setPass($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setIsAdmin($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setNombre($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setApellidos($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setTel($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setDir($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setCiudad($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setDepto($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setPais($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setFax($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setEmpresa($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setNacimiento($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setRecibir($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $listaUsers[$i] = $newUser;
        }


        return $listaUsers;
    }

    function getUsersSearch($order, $orderType, $ll, $hl, $param, $value){

        $sql = 'SELECT * from usuarios WHERE ';
        $sql .= $param.' LIKE "%'.mysql_real_escape_string($value).'%" ';
        $sql .= 'order by '.$order.' '.$orderType.' ';
        $sql .= 'LIMIT '.$ll.','.$hl;

		$this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

        if($numregistros == 0){
            return $newUser;
        }

        //$listaUsers[];
        $listaUsers=array();

        for($i = 0; $i < $numregistros ; $i++){
            $newUser = new user;
            $j = 0;
            $newUser->setId($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setLogin($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setPass($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setIsAdmin($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setNombre($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setApellidos($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setTel($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setDir($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setCiudad($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setDepto($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setPais($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setFax($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setEmpresa($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setNacimiento($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $newUser->setRecibir($this->daoConnection->ObjetoConsulta2[$i][$j++]);
            $listaUsers[$i] = $newUser;
        }


        return $listaUsers;
    }

    function deleteUser($id){

        $sql = 'delete from usuarios where id="'.$id.'" ';

		$this->daoConnection->consulta($sql);
    }

    function updateUserPass($user){
        $querty =   "UPDATE
                     usuarios
                    SET
                    pass =
                    \"".mysql_real_escape_string($user->getPass())."\"
                    WHERE id =
                    ".mysql_real_escape_string($user->getId())."
                    ";
        //echo $querty.'<br />';
        $result = mysql_query($querty, $this->daoConnection->Conexion_ID);
		if (!$result){
            echo 'Ooops (updateUserPass): '.mysql_error();
            return false;
        }

        return true;
    }


    function update($user){

        $querty =   'UPDATE
                     usuarios
                    SET
                    nombre =
                    "'.mysql_real_escape_string($user->getNombre()).'",
                    apellidos =
                    "'.mysql_real_escape_string($user->getApellidos()).'",
                    pass =
                    "'.mysql_real_escape_string($user->getPass()).'",
                    tel =
                    "'.mysql_real_escape_string($user->getTel()).'",
                    dir =
                    "'.mysql_real_escape_string($user->getDir()).'",
                    ciudad =
                    "'.mysql_real_escape_string($user->getCiudad()).'",
                    depto =
                    "'.mysql_real_escape_string($user->getDepto()).'",
                    pais =
                    "'.mysql_real_escape_string($user->getPais()).'",
                    fax =
                    "'.mysql_real_escape_string($user->getFax()).'",
                    empresa =
                    "'.mysql_real_escape_string($user->getEmpresa()).'",
                    nacimiento =
                    "'.mysql_real_escape_string($user->getNacimiento()).'",
                    recibir =
                    "'.mysql_real_escape_string($user->getRecibir()).'"
                    WHERE id =
                    '.mysql_real_escape_string($user->getId()).'
                    ';


            $result = mysql_query($querty, $this->daoConnection->Conexion_ID);

		if (!$result){
                    echo 'Ooops (update-productoso): '.mysql_error();
                    return false;
                }

        return true;
    }

    function total($opt = 0, $campo = 0, $valor = 0){
        
        if($opt == 0)
                $sql = 'select count(*) from usuarios;';
        if($opt == 1)
                $sql = 'select count(*) from usuarios where '.$campo.' LIKE "%'.$valor.'%";';
        if($opt == 2)
                $sql = 'select count(*) from usuarios where '.$campo.' = "'.$valor.'";';

        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        
        return $this->daoConnection->ObjetoConsulta2[0][0];
    }
    
}
?>
