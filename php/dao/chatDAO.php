<?php
class chatDAO{

    public $daoConnection;

	function __construct(){
		$this->daoConnection = new DAO;
        $this->daoConnection->conectar();
	}

    function save($chat){

        $newchat = new chat;
        $newchat = $chat;

        $querty =   "insert into chat
                    (usuario, mensaje, fecha, codigoUsuario)
                    values(
                    \"".mysql_real_escape_string($newchat->getUsuario())."\",
                    \"".mysql_real_escape_string($newchat->getMensaje())."\",
                    \"".mysql_real_escape_string($newchat->getFecha())."\",
                    \"".mysql_real_escape_string($newchat->getCodUsuario())."\"
                    )";

        $result = mysql_query($querty, $this->daoConnection->Conexion_ID);
		if (!$result){
            echo 'Ooops (savechat): '.mysql_error();
            return false;
        }

        return true;

    }

    function getLastId(){
        return mysql_insert_id($this->daoConnection->Conexion_ID);
    }

    function getById($id){

        $newchat = new chat;

        $sql = 'SELECT * from chat where id = "'.mysql_real_escape_string($id).'"';

		$this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

        if($numregistros == 0){
            return null;
        }

        $i = 0;
        $newchat->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
        $newchat->setUsuario($this->daoConnection->ObjetoConsulta2[$i][1]);
        $newchat->setMensaje($this->daoConnection->ObjetoConsulta2[$i][2]);
        $newchat->setFecha($this->daoConnection->ObjetoConsulta2[$i][3]);
        $newchat->setCodUsuario($this->daoConnection->ObjetoConsulta2[$i][4]);
        

        //$documentoToPoblate = $newDocumento;

        return $newchat;

    }


    function update($chat){
        $uchat = new chat;
        $uchat = $chat;

        $querty =   "UPDATE
                     chat
                    SET
                     nombre =
                    \"".mysql_real_escape_string($uchat->getTitulo())."\",
                    tipo =
                    \"".mysql_real_escape_string($uchat->getLugar())."\",
                    codigoUsuario =
                    \"".mysql_real_escape_string($uchat->getCodUsuario())."\",    
                    WHERE id =
                    ".mysql_real_escape_string($uchat->getId())."
                    ";
        //echo $querty.'<br />';
        $result = mysql_query($querty, $this->daoConnection->Conexion_ID);
		if (!$result){
            echo 'Ooops (updatechat): '.mysql_error();
            return false;
        }

        return true;
    }

    function delete($id){

        $sql = 'Delete from chat WHERE id = '.$id.' ';

		$this->daoConnection->consulta($sql);
    }
    
    function deleteConversacion($cod){

        $sql = 'Delete from chat WHERE codigoUsuario = '.$cod.' ';

		$this->daoConnection->consulta($sql);
    }
    


    function total($opt = 0, $campo = 0, $valor = 0){

		if($opt == 0)
			$sql = 'select count(*) from chat;';
		if($opt == 1)
			$sql = 'select count(*) from chat where '.$campo.' LIKE "%'.$valor.'%";';

        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();

		return $this->daoConnection->ObjetoConsulta2[0][0];
	}


function gets($order, $orderType, $l = 0, $h = 20){

        $sql = 'SELECT * from chat order by id DESC ' ;
        $sql .= 'LIMIT '.$l.', '.$h;

		$this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

        $lista=array();

        if($numregistros == 0){
            return $lista;
        }

        //$lista[];


        for($i = 0; $i < $numregistros ; $i++){
            $newchat = new chat;

            $newchat->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
            $newchat->setUsuario($this->daoConnection->ObjetoConsulta2[$i][1]);
            $newchat->setMensaje($this->daoConnection->ObjetoConsulta2[$i][2]);
            $newchat->setFecha($this->daoConnection->ObjetoConsulta2[$i][3]);
            $newchat->setCodUsuario($this->daoConnection->ObjetoConsulta2[$i][4]);
            $lista[$i] = $newchat;
        }


        return $lista;
    }
    
    
    
    function getConversacion($cod, $l = 0, $h = 20){

        $sql = 'SELECT * from chat where codigoUsuario = '.$cod.'  order by id DESC ' ;
        $sql .= 'LIMIT '.$l.', '.$h;

       	$this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

        $lista=array();

        if($numregistros == 0){
            return $lista;
        }

        //$lista[];


        for($i = 0; $i < $numregistros ; $i++){
            $newchat = new chat;

            $newchat->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
            $newchat->setUsuario($this->daoConnection->ObjetoConsulta2[$i][1]);
            $newchat->setMensaje($this->daoConnection->ObjetoConsulta2[$i][2]);
            $newchat->setFecha($this->daoConnection->ObjetoConsulta2[$i][3]);
            $newchat->setCodUsuario($this->daoConnection->ObjetoConsulta2[$i][4]);
            $lista[$i] = $newchat;
        }


        return $lista;
    }
    
    function getUsuarios($l = 0, $h = 20){

        $sql = 'SELECT DISTINCT codigoUsuario, usuario, fecha FROM chat WHERE usuario != "Consejero" GROUP BY codigoUsuario ';
        $sql .= 'LIMIT '.$l.', '.$h;

       	$this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

        $lista=array();

        if($numregistros == 0){
            return $lista;
        }

        //$lista[];


        for($i = 0; $i < $numregistros ; $i++){
            $newchat = new chat;

            $newchat->setCodUsuario($this->daoConnection->ObjetoConsulta2[$i][0]);
            $newchat->setUsuario($this->daoConnection->ObjetoConsulta2[$i][1]);
            $newchat->setFecha($this->daoConnection->ObjetoConsulta2[$i][2]);

            $lista[$i] = $newchat;
        }


        return $lista;
    }
    
    
    function borrarUltimo(){

        $sql = 'select min(id) from chat';
        $this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $id = $this->daoConnection->ObjetoConsulta2[0][0];
        
        $sql = 'Delete from chat WHERE id = '.$id;
        $this->daoConnection->consulta($sql);
    }

    function getsDelmes($mes, $l = 0, $h = 200){

        $sql = 'SELECT * FROM chat WHERE fecha LIKE "%'.$mes.'/20%" LIMIT 0, 30 ';

		$this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();

        $lista=array();

        if($numregistros == 0){
            return $lista;
        }

        //$lista[];


        for($i = 0; $i < $numregistros ; $i++){
            $newchat = new chat;

            $newchat->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
            $newchat->setTitulo($this->daoConnection->ObjetoConsulta2[$i][1]);
            $newchat->setLugar($this->daoConnection->ObjetoConsulta2[$i][2]);
            $newchat->setFecha($this->daoConnection->ObjetoConsulta2[$i][3]);
            $newchat->setHora($this->daoConnection->ObjetoConsulta2[$i][4]);
            $newchat->setDescripcion($this->daoConnection->ObjetoConsulta2[$i][5]);
            $newchat->setImagen($this->daoConnection->ObjetoConsulta2[$i][6]);
            $newchat->setOtro($this->daoConnection->ObjetoConsulta2[$i][7]);

            $lista[$i] = $newchat;
        }


        return $lista;
    }

function getPorFecha($fecha){

        $sql = "SELECT * FROM chat WHERE fecha = '$fecha'";

		$this->daoConnection->consulta($sql);
        $this->daoConnection->leerVarios();
        $numregistros = $this->daoConnection->numregistros();



        if($numregistros == 0){
            return null;
        }

        //$lista[];


            $i=0;
            $newchat = new chat;

            $newchat->setId($this->daoConnection->ObjetoConsulta2[$i][0]);
            $newchat->setTitulo($this->daoConnection->ObjetoConsulta2[$i][1]);
            $newchat->setLugar($this->daoConnection->ObjetoConsulta2[$i][2]);
            $newchat->setFecha($this->daoConnection->ObjetoConsulta2[$i][3]);
            $newchat->setHora($this->daoConnection->ObjetoConsulta2[$i][4]);
            $newchat->setDescripcion($this->daoConnection->ObjetoConsulta2[$i][5]);
            $newchat->setImagen($this->daoConnection->ObjetoConsulta2[$i][6]);
            $newchat->setOtro($this->daoConnection->ObjetoConsulta2[$i][7]);





        return $newchat;;
    }
	
	

}
?>
