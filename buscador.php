<?php
include 'includes.php';

$itemsPerPage = 10;
$orden = isset($_GET['orden']) ? $_GET['orden'] : "nombre";
$page = isset($_GET['page']) ? $_GET['page'] : 0;
$idCat1 = $_GET['idCat1'];
$s = text2HTML($_GET['s']);
$s2 = text2NoAccent($s);
$getsPage = 's='.$s.'&idCat1='.$idCat1;

$productoDAO = new productoDAO();


if($idCat1 == 0){
    $productos = $productoDAO->search($orden, "asc",$page*$itemsPerPage, $itemsPerPage, ' (nombreSimple LIKE "%'.mysql_real_escape_string($s2).'%" OR nombre LIKE "%'.mysql_real_escape_string($s).'%"  )  OR datos LIKE "%'.mysql_real_escape_string($s).'%"  ');
    $total = count($productoDAO->search($orden, "asc",0, 500, '  (nombreSimple LIKE "%'.mysql_real_escape_string($s2).'%" OR nombre LIKE "%'.mysql_real_escape_string($s).'%"  )  OR datos LIKE "%'.mysql_real_escape_string($s).'%"  ') );
}else{
    $productos = $productoDAO->search($orden, "asc",$page*$itemsPerPage, $itemsPerPage, ' ( (nombreSimple LIKE "%'.mysql_real_escape_string($s2).'%" OR nombre LIKE "%'.mysql_real_escape_string($s).'%"  ) OR datos LIKE "%'.mysql_real_escape_string($s).'%" )  AND idCat1 = "'.$idCat1.'"');
    $total = count($productoDAO->search($orden, "asc",0, 5000, ' ((nombreSimple LIKE "%'.mysql_real_escape_string($s2).'%" OR nombre LIKE "%'.mysql_real_escape_string($s).'%"  )  OR datos LIKE "%'.mysql_real_escape_string($s).'%" ) AND idCat1 = "'.$idCat1.'"'));
    $cat1DAO = new cat1DAO();
    $cat1S = $cat1DAO->getById($idCat1);
}


$totalPages = (int)($total/$itemsPerPage);
if( ($total%$itemsPerPage) != 0)
        $totalPages++;

$pageNext = $page + 1;
$pagePrev = $page - 1;
if( $pageNext >= $totalPages )
    $pageNext = 0;
if( $pagePrev < 0 )
    $pagePrev = 0;


$p1 = 'buscador.php?'.$getsPage;
$p2 = 'buscador.php?'.$getsPage;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>La Era Azul - Libros y Accesorios</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<link type="image/x-icon" href="era_azul.ico" rel="shortcut icon" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/large.css" rel="stylesheet" type="text/css" />
<?php
    //Xajax
    $xajax->printJavascript("xajax/");
?>
<script type="text/javascript" src="js/mootools-core-1.3.1-full-compat-yc.js"></script>
<script type="text/javascript" src="js/sexyforms.v1.3.mootools.min.js"></script>
<script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link   type="text/css" rel="stylesheet" href="sexyforms/gamma/sexyforms-gamma.css"  media="all" id="theme" />
<script type="text/javascript">
window.addEvent('domready', function() {
  $$(".sexyform input", ".sexyform select", ".sexyform textarea").each(function(el) {
    el.DoSexy();
  });
});
</script>

<script src="js/jquery.tools.min.js"></script>
</head>
<body> 
<div id="header">
  <div class="gutter">
    <div class="inner">
      <div id="htop">
        <?php include("includes/bar.php"); ?>
        <?php include("includes/logo.php"); ?>
      </div>
      <?php include("includes/menu.php"); ?>
    </div>
  </div>
</div>
<div id="wrapper">
  <div id="wrapperbg">
    <div class="inner clearfix">
      <div id="main">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="215" valign="top">
              <?php if($idCat1 != 0) include("includes/tematico.php"); ?>
              <?php include("includes/publicidad.php"); ?></td>
            <td valign="top">
            <div id="m_tot">
                <div class="pagination">
                    <?php if( $page == 0 ){ ?>
                    <span class="disabled">Prev</span>
                    <?php }else{ ?>
                    <a href="?page=<?php echo $pagePrev.'&'.$getsPage;?>" class="prev">Prev</a>
                    <?php } ?>
                    <?php
                    if( $totalPages > 10 && ($page > 5) )
                            echo '...';
                    //en caso de haber menos de 10 paginas...
                    for($i = 0; $i<$totalPages; $i++){
                        $is = $i+1;
                        if($page == $i){
                    ?>
                    <span class="current"><?php echo $is;?></span>
                    <?php
                    }else{
                    if( $totalPages > 10 && ($i-5 > $page || $i+5 < $page ) )
                        continue;
                    ?>
                    <a href="?page=<?php echo $i.'&'.$getsPage;?>"><?php echo $is;?></a>
                    <?php
                    }
                    }

                    if( $totalPages > 10 && ($page+6 < $totalPages) )
                            echo '...';

                    ?>
                    <?php if( $page+1 == $totalPages ){ ?>
                    <span class="disabled">Next</span>
                    <?php }else{ ?>
                    <a href="?page=<?php echo $pageNext.'&'.$getsPage;?>" class="next">Next</a>
                    <?php } ?>
                </div>
              <div id="contenido" class="content">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><h1>Buscador</h1></td>
                    <td><div id="ordenar">
                            <form id="form1" name="form1" method="get" action="" class="sexyform">
                                      <label for="ordenarpor"></label>
                                      <select name="orden" id="ordenarpor">
                                          <option value="vendido" <?php if($orden == 'vendido') echo 'selected'; ?>>Más vendido</option>
                                          <option value="novedad" <?php if($orden == 'novedad') echo 'selected'; ?>>Novedades</option>
                                          <option value="nombre" <?php if($orden == 'nombre') echo 'selected'; ?>>Nombre</option>
                                          <option value="precio" <?php if($orden == 'precio') echo 'selected'; ?>>Precio</option>
                                      </select>
                                      <input type="hidden" name="idCat1" value="<?php echo $idCat1;?>"/>
                                      <input type="hidden" name="s" value="<?php echo $s;?>"/>
                                      <input type="submit" value="Ordenar"/>
                          </form>
                                    </div></td>
                  </tr>
                </table>
                  <?php foreach ($productos as $producto){ ?>
                <div class="producto_buscador">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    
                    <tr>
                      <td width="13%" valign="top">
                          <div><img src="imagenes/productos/<?php echo $producto->getImg();?>" width="80"/></div>
                        </td>
                        <td width="87%">
                          <h5><a class="obra" href="producto.php?id=<?php echo $producto->getId();?>"><?php echo $producto->getNombre();?></a> </h5>
                          <p>Precio: <span class="pri">$<?php echo $producto->getPrecioFormato();?></span></p>
                          <div class="buy">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="10%">
                                    <form id="form1" name="form1" method="post" action="" class="sexyform">
                                  <label for="qty_1"></label>
                                  <input style="width:50px" name="qty_1" type="text" id="qty_<?php echo $producto->getId();?>" value="1" />
                                </form>
                                </td>
                                <td width="90%">
                                    <span id="noti<?php echo $producto->getId();?>">
                                    <a href="#" onclick="xajax_addToCart(<?php echo $producto->getId();?>, document.getElementById('qty_<?php echo $producto->getId();?>').value,'noti<?php echo $producto->getId();?>');return false;" >
                                        <img src="img/bt_buy.png" border="0" />
                                    </a>
                                    </span>
                                </td>
                              </tr>
                            </table>
                          </div>
                        </td>
                      </tr>
                  </table>
                </div>
                <?php } ?>
                </div>
            </div>
            <div class="pagination">
                    <?php if( $page == 0 ){ ?>
                    <span class="disabled">Prev</span>
                    <?php }else{ ?>
                    <a href="?page=<?php echo $pagePrev.'&'.$getsPage;?>" class="prev">Prev</a>
                    <?php } ?>
                    <?php
                    if( $totalPages > 10 && ($page > 5) )
                            echo '...';
                    //en caso de haber menos de 10 paginas...
                    for($i = 0; $i<$totalPages; $i++){
                        $is = $i+1;
                        if($page == $i){
                    ?>
                    <span class="current"><?php echo $is;?></span>
                    <?php
                    }else{
                    if( $totalPages > 10 && ($i-5 > $page || $i+5 < $page ) )
                        continue;
                    ?>
                    <a href="?page=<?php echo $i.'&'.$getsPage;?>"><?php echo $is;?></a>
                    <?php
                    }
                    }

                    if( $totalPages > 10 && ($page+6 < $totalPages) )
                            echo '...';

                    ?>
                    <?php if( $page+1 == $totalPages ){ ?>
                    <span class="disabled">Next</span>
                    <?php }else{ ?>
                    <a href="?page=<?php echo $pageNext.'&'.$getsPage;?>" class="next">Next</a>
                    <?php } ?>
                </div>
              </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include("includes/footer.php"); ?>
</div>




<!--Carrito -->
<?php include("includes/mcart.php"); ?>



<!--Registro o login -->
<?php include("includes/relog.php"); ?>
<!--Funtions -->
<script type="text/javascript" src="js/jquery.hoverIntent.minified.js"></script> 
<script type="text/javascript" src="js/funtionm.js"></script> 
<script> 
$(document).ready(function() { 
	var triggers = $(".modalInput").overlay({ 
		mask: {
			color: '#ebecff',
			loadSpeed: 200,
			opacity: 0.9
		}
	});
});
</script>
</body>
</html>