<?php
$cat1DAO = new cat1DAO();
$cat2DAO = new cat2DAO();

$cats1 = $cat1DAO->gets("id", "asc");
?>
<div id="hbot">
        <ul id="topnav">
        <?php
            foreach($cats1 as $cat1){
                $cats2 = $cat2DAO->getsByIdCat1("nombre", "asc", $cat1->getId());
        ?>
        <li> 
            <a href="productos.php?param=idCat1&value=<?php echo $cat1->getId();?>" class="revistas"><?php echo $cat1->getNombre();?></a>
            <?php if(count($cats2) > 0){ ?>
            <div class="sub"> 
                <ul>
                    <?php $j=0; foreach($cats2 as $cat2){ ?>
                    <li>
                        <a href="productos.php?param=idCat2&value=<?php echo $cat2->getId();?>">
                            <?php echo $cat2->getNombre();?>
                        </a>
                    </li>
                    <?php $j++;if($j%15 == 0) echo '</ul><ul>'; } ?>
                </ul> 
            </div>
            <?php } ?>
        </li>
        <?php } ?>
        <li><a href="promo.php" class="promociones">Promociones</a></li>
    </ul>
</div>