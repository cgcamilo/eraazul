<?php
$aspectoDAO = new AspectsDAO();
$email = $aspectoDAO->getAspect('email');
$tel1 = $aspectoDAO->getAspect('tel1');
$tel2 = $aspectoDAO->getAspect('tel2');
?>
<div id="footer">
  <div class="inner clearfix">
  <div id="footitem">
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td width="22%" valign="top" class="blkfoot">
                <div class="foot_tit">Mapa del sitio</div>
                <div class="foot_cont"><a href="sitemap.php"><img src="img/sitemap.png" width="58" height="28" /></a></div>
                  <div id="autor"><p>©2011 La Era Azul</p></div>
                  <div class="footer_autor"><span id="ahorranito"></span><a href="http://www.imaginamos.com">Diseño Web: </a><a href="http://www.imaginamos.com">imagin<span>a</span>mos.com</a></div>                  </td>
              <td width="28%" valign="top" class="blkfoot">
                <div class="foot_tit">Redes Sociales</div>
                <div class="social">
                  <ul>
                    <li><a href="http://www.facebook.com/pages/La-Era-Azul-Libros-y-Accesorios/133994063359736" target="_blank"><img src="img/icons/ico_facebook.png"/></a></li>
                    <li><a href="#" target="_blank"><img src="img/icons/ico_twitter.png" /></a></li>
                    <li><a href="#" target="_blank"><img src="img/icons/ico_algo.png" /></a></li>
                    <li><a href="#" target="_blank"><img src="img/icons/ico_flickr.png" /></a></li>
                    <li><a href="#" target="_blank"><img src="img/icons/ico_youtube.png" /></a></li>
                  </ul>
                  </div>                </td>
            <td width="28%" valign="top" class="blkfoot"><img src="img/pagosonline.png"/></td>
                <td width="22%" valign="top" class="blkfootin">
                  <div class="ico_phone"></div>
                  <div class="foot_inf">
                    <p><strong>Línea Directa</strong></p>
                    <p><?php echo $tel1->getValue();?></p>
                    <p><strong>Resto del país</strong></p>
                    <p><?php echo $tel2->getValue();?></p>
                    <p><strong>Email:</strong></p>
                    <p><a href="mailto:<?php echo $email->getValue();?>"><?php echo $email->getValue();?></a></p>
              </div>                 </td>
            </tr>
      </table>
    </div>
  </div>
</div>