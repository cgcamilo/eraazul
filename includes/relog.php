<div class="modal" id="modalreglog"> 
<table width="900" align="center" border="0" cellspacing="10" cellpadding="0">
  <tbody><tr>
    <td width="45%" valign="top">
        <form action="./php/action/login.php" name="login" id="login" method="post">
        <table class="fondoTabla" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td colspan="2" align="center" style="border-bottom:solid 1px #CCC"><h1>Ingresa a tu cuenta</h1></td>
          </tr>
          <tr>
          <td>&nbsp;</td>
          </tr>
          <tr>
            <td width="35%" align="right" class="TextoNormal"><strong>Correo electrónico</strong></td>
            <td width="65%" style="padding: 10px 0px 5px 5px;" class="TextoNormal">
            	<input type="text" name="usuario" value="<?php echo $_GET['usuario'];?>" class="campo">            </td>
          </tr>
          <tr>
            <td align="right" class="TextoNormal"><strong>Contraseña</strong></td>
            <td class="TextoNormal" style="padding-left: 5px;">
                    <input type="password" name="pass" class="campo">            </td>
          </tr>
          <tr>
            <td align="right">&nbsp;</td>
            <td>
              <input name="button" type="submit" class="bto" id="button" value="Iniciar Sesión" /></td>
          </tr>
		   <tr>
            <td align="center" class="TextoNormal" colspan="2" style="padding:10px;"><strong>¿Olvido su contraseña?</strong>
                   <a href="#" id="recordar_link">Recuperar Contraseña</a></td>
          </tr>
        </tbody></table>
        <input type="hidden" name="p1" value="<?php echo $p1;?>">
        <input type="hidden" name="p2" value="<?php echo $p2;?>">
      </form>
        <div id="recordar-form" style="display:none">
          <form action="./php/action/olvido.php" method="post">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="right" class="TextoNormal"><strong>Correo electrónico</strong></td>
                  <td width="65%" style="padding: 10px 0px 5px 5px;" class="TextoNormal">
                  <input type="text" name="email" id="emailrec" class="campo"/></td>
                 </tr>
              <tr>
                <td>&nbsp;</td>
                  <td>
                    <input type="submit" name="button3" id="button3" value="Recuperar" class="bto"/></td>
                 </tr>
              </table>
              <input type="hidden" name="p1" value="<?php echo $p1;?>">
              <input type="hidden" name="p2" value="<?php echo $p2;?>">
          </form>
        </div>
<script>
$("#recordar_link").click(function () {
$("#recordar-form").fadeIn("slow");
});
</script>
    </td>
    <td width="50%">
        <form action="./php/action/register.php" name="nuevoUsuario" id="nuevoUsuario" method="post">
        <table class="fondoTabla" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td colspan="2" align="center" style="border-bottom:solid 1px #CCC"><h1>Crea una nueva cuenta</h1></td>
          </tr>
          <tr>
          <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2" class="TextoNormal" style="padding:0px 15px 0px 15px;">Escribe tus datos personales.
              Después, determina la contraseña para tu cuenta.</td>
          </tr>
          <tr>
            <td width="43%" align="right" class="TextoNormal"><strong>Nombre</strong></td>
            <td width="57%" style="padding: 10px 0px 5px 5px;" class="TextoNormal">
           	<input type="text" name="nombre" value="<?php echo $_GET['nombre'];?>" class="campo">            </td>
          </tr>
          <tr>
            <td align="right" class="TextoNormal"><strong>Apellidos</strong></td>
            <td class="TextoNormal" style="padding: 0px 0px 5px 5px;">
              <input type="text" name="apellidos" value="<?php echo $_GET['apellidos'];?>" class="campo">           </td>
          </tr>
          <tr>
            <td align="right" class="TextoNormal"><strong>Fecha de Nacimiento (dd-mm-aaaa)</strong></td>
            <td class="TextoNormal" style="padding: 0px 0px 5px 5px;"><input type="date" min="1900-01-01" max="2012-01-01" name="nacimiento" id="datePicker1" value="<?php echo $_GET['nacimiento'];?>" class="campo">
             </td>
          </tr>
          <tr>
            <td align="right" class="TextoNormal"><strong>Correo electrónico</strong></td>
            <td class="TextoNormal" style="padding: 0px 0px 5px 5px;">
              <input type="text" name="login" value="<?php echo $_GET['login'];?>" class="campo">            </td>
          </tr>
          <tr>
            <td align="right" class="TextoNormal"><strong>Contraseña</strong></td>
            <td class="TextoNormal" style="padding-left: 5px;">
                    <input type="password" name="pass" value="" class="campo">            </td>
          </tr>
          <tr>
            <td align="right" class="TextoNormal"><strong>Confirmar Contraseña</strong></td>
            <td style="padding:5px 0px 0px 5px;">
              <input type="password" name="pass2" value="" class="campo">            </td>
          </tr>
          <tr>
            <td align="right" class="TextoNormal"><span style="padding:5px 0px 0px 5px;">
              <input type="checkbox" name="recibir" id="deseo" />
            </span></td>
            <td style="padding:5px 0px 0px 5px;"><label for="deseo">deseo recibir a mi mail las promociones que la era azul tiene para mi</label></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>
              <input name="button2" type="submit" class="bto" id="button2" value="Crear Cuenta" /></td>
          </tr>
        </tbody></table>
        <input type="hidden" name="p1" value="<?php echo $p1;?>">
        <input type="hidden" name="p2" value="<?php echo $p2;?>">
	</form>
    </td>
  </tr>
</tbody></table>
</div>

<script>
// the french localization
$.tools.dateinput.localize("es",  {
   months:        'enero,febrero,marzo,abril,mayo,junio,julio,agosto,' +
                   	'septiembre,octubre,noviembre,diciembre',
   shortMonths:   'ene,feb,mar,abr,may,jun,jul,ago,sep,oct,nov,dic',
   days:          'domingo,lunes,martes,miércoles,jueves,viernes,sábado',
   shortDays:     'dom,lun,mar,mié,jue,vie,sáb'
});
// dateinput and it's configuration
$('#datePicker1').dateinput({ trigger: true, format: 'dd-mm-yyyy', selectors: true, lang: 'es', yearRange: [-100, -0], offset: [2, 0] });
</script>