<?php
//session_start();
$carrito = unserialize($_SESSION['carrito']);
$total = unserialize($_SESSION['total']);
//@include '../php/entities/producto.php';
?>
<h2>Carrito de Compras</h2>
   <form action="" method="get">
     <div class="modcont">
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
           <?php foreach ($carrito as $producto){ ?>
         <tr>
           <td width="17%">
             <label for="qty"></label>
             x <?php echo $producto->getCantidad();?>
           </td>
           <td width="6%">
               <a href="./php/action/cartRemoveItem.php?id=<?php echo $producto->getId();?>"><img src="img/icons/ico_cancel.png" width="16" height="16" /></a>
           </td>
           <td width="51%">
               <a href="producto.php?id=<?php echo $producto->getId();?>"><?php echo $producto->getNombre();?></a>
           </td>
           <td width="26%">$<?php echo $producto->getPrecioFormato();?> c/u</td>
          </tr>
          <?php } ?>
         <tr>
           <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
         <tr>
           <td><strong>SUBTOTAL</strong></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><strong>$<?php echo number_format($total,0,',','.');?></strong></td>
          </tr>
        </table>
     </div>
   </form>
<div class="btcart"><a href="cart.php" target="_self">VER CARRO COMPLETO</a> </div>