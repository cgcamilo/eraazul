<?php
$cat1DAO = new cat1DAO();
$cat2DAO = new cat2DAO();

$cats1 = $cat1DAO->gets("id", "asc");

if( !isset($_SESSION['usuario']) )
    include 'includes/bar_out.php';
    
$usuario = unserialize($_SESSION['usuario']);
?>
<div class="bar">
  <div class="bartit">Bienvenido(a): <?php echo $usuario->getNombre(); ?>
      |
      <a href="myaccount.php">Mi cuenta</a>
      |
      <a href="./php/action/logout.php">Salir</a>
      |
      <a href="contacto.php">Contáctenos</a></div>
  <div class="barcont">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="76%" valign="top">
          <div id="search">
            <form action="buscador.php" method="get" class="sexyform">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td valign="top">
                      <input style="width:240px" name="s" value="<?php echo $_GET['s'];?>" type="text" id="textfield"/></td>
                <td valign="top">
                  
                      <div style="position:relative">
                      <select style="width:180px" name="idCat1" id="prodsel">
                        <option>Todos los productos</option>
                        <?php foreach($cats1 as $cat1){ ?>
                        <option value="<?php echo $cat1->getId();?>"><?php echo $cat1->getNombre();?></option>
                        <?php } ?>
                      </select>
                      </div></td>
                <td valign="top">
                      <input type="submit" name="button" id="button" value="Buscar" icon="img/icons/zoom.png"/></td>
                </tr>
              </table>
            </form>
        </div>                </td>
        <td width="24%" valign="top">
          
          <div id="menulogcart">
              <ul><li>
                      <a class="btrig modalInput" href="index.php" rel="#modalcart">
                          <div id="countcart">
                              (<span id="totalItems"><?php echo count(unserialize($_SESSION['carrito']));?></span> items) $<span id="subtotal"><?php echo number_format(unserialize($_SESSION['total']) , 0, ',', '.');?></span>
                          </div>
                          <img src="img/bt_cart.png" alt />
                      </a>
                  </li>
            </ul>
        </div>
        </td>
      </tr>
    </table>
  </div>
</div>