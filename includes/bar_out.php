<?php
$cat1DAO = new cat1DAO();
$cat2DAO = new cat2DAO();

$cats1 = $cat1DAO->gets("id", "asc");


?>
<div class="bar">
  <div class="bartit">
      <?php if( isset($_GET['errorL1']) ){ ?>
      <span style="color: red">Introduce todos los datos</span>
      <?php } ?>
      <?php if( isset($_GET['errorL2']) ){ ?>
      <span style="color: red">Usuario/Contraseña incorrectos</span>
      <?php } ?>
      <?php if( isset($_GET['errorO1']) ){ ?>
      <span style="color: red">Email incorrecto</span>
      <?php } ?>
      <?php if( isset($_GET['errorR1']) ){ ?>
      <span style="color: red">Introduce todos los datos para el registro</span>
      <?php } ?>
      <?php if( isset($_GET['errorR2']) ){ ?>
      <span style="color: red">Ya existe un usuario con ese email</span>
      <?php } ?>
      <?php if( isset($_GET['errorR3']) ){ ?>
      <span style="color: red">Verifica que las contraseñas son iguales</span>
      <?php } ?>
      <?php if( isset($_GET['newPass']) ){ ?>
      <span style="color: green">Te hemos enviado un email con tu nueva contraseña</span>
      <?php } ?>
      <a href="#" class="modalInput" rel="#modalreglog">Ingresar</a> | <a href="#" class="modalInput" rel="#modalreglog">Registrarse</a> | <a href="contacto.php">Contáctenos</a></div>
  <div class="barcont">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="76%" valign="top">
          <div id="search">
            <form action="buscador.php" method="get" class="sexyform">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td valign="top">
                      <input style="width:240px" name="s" value="<?php echo $_GET['s'];?>" type="text" id="textfield"/></td>
                <td valign="top">
                  
                      <div style="position:relative">
                      <select style="width:180px" name="idCat1" id="prodsel">
                        <option value="0">Todos los productos</option>
                        <?php foreach($cats1 as $cat1){ ?>
                        <option value="<?php echo $cat1->getId();?>" <?php if($_GET['idCat1'] == $cat1->getId()) echo 'selected'; ?> ><?php echo $cat1->getNombre();?></option>
                        <?php } ?>
                      </select>
                      </div></td>
                <td valign="top">
                      <input type="submit" name="button" id="button" value="Buscar" icon="img/icons/zoom.png"/></td>
                </tr>
              </table>
            </form>
        </div>                </td>
        <td width="24%" valign="top">
          
          <div id="menulogcart">
              <ul><li>
                      <a class="btrig modalInput" href="index.php" rel="#modalcart">
                          <div id="countcart">
                              (<span id="totalItems"><?php echo count(unserialize($_SESSION['carrito']));?></span> items) $<span id="subtotal"><?php echo number_format(unserialize($_SESSION['total']) , 0, ',', '.');?></span>
                          </div>
                          <img src="img/bt_cart.png" alt />
                      </a>
                  </li>
            </ul>
        </div>
        </td>
      </tr>
    </table>
  </div>
</div>