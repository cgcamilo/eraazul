<?php
if( !isset($_SESSION['admin']) ){
    header("location: ./index.php");
    exit;
}

include("fckeditor/fckeditor.php") ;

$aspectoDAO = new AspectsDAO();
$tel1 = $aspectoDAO->getAspect('tel1');
$tel2 = $aspectoDAO->getAspect('tel2');
$email = $aspectoDAO->getAspect('email');
$shipping = $aspectoDAO->getAspect('shipping');
$shipping2 = $aspectoDAO->getAspect('shipping2');
?>
                    <div class="titulos">
                      <div class="titulos_texto1">Zona Conetactenos + Valor Envio<div class="cerrar"><a href="../php/action/logout.php"><img src="imagenes/contenido/cerrar.png" alt="Cerrar Sesi&oacute;n" border="0" /></a></div></div>
                      <div class="titulos_texto2">
                      </div>
                    </div>
                    <!-- FIN TITULOS -->
                    <div class="contenido_marco_sup"></div>
                    <div class="contenido_fondo">
                      <div class="subcontenido">

<div class="subtitulos">
 <table width="700">
    <tr>
        <td>
            Conetactenos & Envio
        </td>
    </tr>
 </table>
<div class="subtitulos_menu">
<!--  <form id="form_boton" ><input type="button" value="Adicionar Nuevo Equipo" id="nuevo" /></form> -->
</div>
</div>
<div class="subcontenido2">
<table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
</table>
	  <div id="buscador">
      <div class="rowElem"></div>
      </div>
      <div class="enunciados"></div>
      <form action="../php/action/conetactenos.php" method="post" enctype="multipart/form-data">
      <table border="0" align="l" width="600" border="0" cellspacing="4" cellpadding="4">
         <?php if( isset($_GET['error1']) ){?>
           <tr>
             <td colspan="4" class="EstiloRed">Tienes que introducir todos los datos</td>
             </tr>
        <?php }if( isset($_GET['ok']) ){?>
            <tr>
             <td colspan="4" class="EstiloGreen">Datos almacenados</td>
             </tr>
        <?php }if( isset($_GET['error3']) ){?>
            <tr>
             <td colspan="4" class="EstiloRed">Tienes que introducir la URL ó seleccionar un producto</td>
             </tr>
        <?php }?>
          <tr>
            <td colspan="2"><b><font style="font-size:large;color:#154894">Información Básica</font></b></td>
          </tr>
          <tr>
            <td  colspan="2">
                Línea Directa:<font style="font-size:x-small;color:red">*</font>
                <br />
                <input size="35" name="tel1" value="<?php echo $tel1->getValue();?>">
            </td>
          </tr>
          <tr>
            <td  colspan="2">
                Resto del País:<font style="font-size:x-small;color:red">*</font>
                <br />
                <input size="35" name="tel2" value="<?php echo $tel2->getValue();?>">
            </td>
          </tr>
          <tr>
            <td  colspan="2">
                Email:<font style="font-size:x-small;color:red">*</font>
                <br />
                <input size="35" name="email" value="<?php echo $email->getValue();?>">
            </td>
          </tr>
          <tr>
            <td  colspan="2">
                Valor Envio Bogotá:<font style="font-size:x-small;color:red">*</font>
                <br />
                <input size="35" name="shipping" value="<?php echo $shipping->getValue();?>"> COPS
            </td>
          </tr>
          <tr>
            <td  colspan="2">
                Valor Envio Resto del pais:<font style="font-size:x-small;color:red">*</font>
                <br />
                <input size="35" name="shipping2" value="<?php echo $shipping2->getValue();?>"> COPS
            </td>
          </tr>
          <tr>
            <td colspan="2" align="right">
                <input type="submit" value="Guardar">
            </td>
          </tr>
          <tr>
            <td colspan="2">
                <font style="font-size:x-small;color:red">
                    * campos obligatorios
                </font>
            </td>
          </tr>
      </table>
      </form>
    </div>
  </div>
  <!-- FIN SUBCONTENIDO -->
  <!-- FIN SUBCONTENIDO -->
</div>
<!-- FIN CONTENIDO FONDO -->
<div class="contenido_marco_inf"></div>
