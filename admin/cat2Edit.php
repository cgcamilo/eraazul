<?php
if( !isset($_SESSION['admin']) ){
    header("location: ./index.php");
    exit;
}

include("fckeditor/fckeditor.php") ;

$idCat1 = $_GET['idCat1'];
$id = $_GET['id'];

$cat1DAO = new cat1DAO();
$cat1 = $cat1DAO->getById($idCat1);

$cat2DAO = new cat2DAO();
$cat2 = $cat2DAO->getById($id);

?>
                    <div class="titulos">
                      <div class="titulos_texto1">Subcategorías de <b><?php echo $cat1->getNombre();?></b><div class="cerrar"><a href="../php/action/logout.php"><img src="imagenes/contenido/cerrar.png" alt="Cerrar Sesi&oacute;n" border="0" /></a></div></div>
                      <div class="titulos_texto2">
                      </div>
                    </div>
                    <!-- FIN TITULOS -->
                    <div class="contenido_marco_sup"></div>
                    <div class="contenido_fondo">
                      <div class="subcontenido">

<div class="subtitulos">
 <table width="700">
    <tr>
        <td>
            <a href="menuAdmin.php?s=cat2&idCat1=<?php echo $idCat1;?>">
            Subcategorías
            </a>
            >
            <b>Editar</b>
        </td>
    </tr>
 </table>
<div class="subtitulos_menu">
<!--  <form id="form_boton" ><input type="button" value="Adicionar Nuevo Equipo" id="nuevo" /></form> -->
</div>
</div>
<div class="subcontenido2">
<table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
</table>
	  <div id="buscador">
      <div class="rowElem"></div>
      </div>
      <div class="enunciados"></div>
      <form action="../php/action/cat2Edit.php" method="post" enctype="multipart/form-data">
      <table border="0" align="l" width="600" border="0" cellspacing="4" cellpadding="4">
         <?php if( isset($_GET['error1']) ){?>
           <tr>
             <td colspan="4" class="EstiloRed">Tienes que introducir todos los datos</td>
             </tr>
        <?php }if( isset($_GET['error2']) ){?>
            <tr>
             <td colspan="4" class="EstiloRed">Tienes que introducir la 'imagen'</td>
             </tr>
        <?php }if( isset($_GET['error3']) ){?>
            <tr>
             <td colspan="4" class="EstiloRed">Tienes que introducir la URL ó seleccionar un producto</td>
             </tr>
        <?php }if( isset($_GET['ok']) ){?>
            <tr>
             <td colspan="4" class="EstiloGreen">Datos almacenados</td>
             </tr>
        <?php }?>
          <tr>
            <td colspan="2"><b><font style="font-size:large;color:#154894">Información Básica</font></b></td>
          </tr>
          <tr>
            <td  colspan="2">
                Nombre:<font style="font-size:x-small;color:red">*</font>
                <br />
                <input size="35" name="nombre" value="<?php echo $cat2->getNombre();?>">
            </td>
          </tr>
          <tr>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td align="right">
                <input type="hidden" name="id" value="<?php echo $id;?>">
                <input type="hidden" name="idCat1" value="<?php echo $idCat1;?>">
                <input type="submit" value="Guardar">
            </td>
          </tr>
          <tr>
            <td colspan="2">
                <font style="font-size:x-small;color:red">
                    * campos obligatorios
                </font>
            </td>
          </tr>
      </table>
      </form>
    </div>
  </div>
  <!-- FIN SUBCONTENIDO -->
  <!-- FIN SUBCONTENIDO -->
</div>
<!-- FIN CONTENIDO FONDO -->
<div class="contenido_marco_inf"></div>
