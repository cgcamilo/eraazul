<?php
//TODO
//ver que es la nueva plantilla y hacer "REF" para que al añadir se devuelva a productos2.php

if( !isset($_SESSION['admin']) ){
    header("location: ./index.php");
    exit;
}

//novedad, regalar, vendido, recomendado, popular, promocion
if( isset($_GET['p']) )
    $p = $_GET['p'];
else
    $p = 'novedad';

$cat2DAO = new cat2DAO();

$cat1DAO = new cat1DAO();
$cat1 = $cat1DAO->getById($idCat1);

$productosDAO = new productoDAO();
$lista = $productosDAO->search("id", "asc", 0, 10000, $p.' = "1"');
$total = $productosDAO->total(2, $p, 1);

//idcatx
if( isset($_GET['idCat1X']) && $_GET['idCat1X'] != 0 )
{
    $lista2 = array();
    foreach ($lista as $it) {
        if( $it->getIdCat1() == $_GET['idCat1X'] )
                $lista2[] = $it;
    }

    $total = count($lista2);
    $lista = $lista2;
}
?>
                    <div class="titulos">
                        <div class="titulos_texto1">Productos (<b><?php echo $p;?></b>)<div class="cerrar"><a href="../php/action/logout.php"><img src="imagenes/contenido/cerrar.png" alt="Cerrar Sesi&oacute;n" border="0" /></a></div></div>
                      <div class="titulos_texto2">
                      </div>
                    </div>
                    <!-- FIN TITULOS -->
                    <div class="contenido_marco_sup"></div>
                    <div class="contenido_fondo">
                      <div class="subcontenido">

<div class="subtitulos">Un total de <?php echo $total;?> Productos en <?php echo $p;?><div class="subtitulos_menu">
<!--  <form id="form_boton" ><input type="button" value="Adicionar Nuevo Equipo" id="nuevo" /></form> -->
</div>
</div>
<div class="subcontenido2">

<table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
</table>
	  <div id="buscador">
      <div class="rowElem"></div>
      </div>
      <div class="enunciados">
          
          <form method="get" action="menuAdmin.php">
              <select name="idCat1">
                  <?php foreach($cat1s as $cat1){ ?>
                  <option value="<?php echo $cat1->getId();?>">
                    <?php echo $cat1->getNombre();?>
                  </option>
                  <?php } ?>
                  <input type="submit" value="añadir">
                  <input type="hidden" name="s" value="productosAdd2">
                  <input type="hidden" name="<?php echo $p?>" value="1">
                  <input type="hidden" name="p" value="<?php echo $p?>">
              </select>
          </form>
          <br />
          <form method="get" action="menuAdmin.php">
                    <input type="hidden" name="p" value="<?php echo $p?>">
                    <input type="hidden" name="s" value="productos2">
                <select name="idCat1X">
                  <option value="0">Todos</option>
                  <?php foreach($cat1s as $cat1){ ?>
                  <option value="<?php echo $cat1->getId();?>" <?php if($_GET['idCat1X'] == $cat1->getId() ) echo 'selected'; ?>>
                    <?php echo $cat1->getNombre();?>
                  </option>
                  <?php } ?>
                  <input type="submit" value="filtrar">
              </select>
            </form>
      </div>
    <form runat="server" action="../php/action/productoDel2Varios.php" id="overall" method="get">
      <table width="100%" class="yui" id="tableOne">
        <thead>
          <tr>
            <td colspan="3">
                
            </td>
            <td colspan="3" class="filter"><span class="rowElem">
              Buscar:
              <input id="filterBoxOne" value="" maxlength="30" size="30" type="text" onkeypress="return checkEnter(event)" />
              </span>
            </td>
          </tr>
          <?php if( isset($_GET['ok']) ){ ?>
            <tr>
                <td class="EstiloGreen" colspan="5">
                    <?php if($_GET['id'] != 0){ ?>
                    Producto añadido.

                    <br />
                    Haga click
                    <a href="menuAdmin.php?p=<?php echo $p?>&id=<?php echo $_GET['id'];?>&s=productosEdit2#relacionados">
                        <b>AQUI</b>
                    </a>
                    para añadir productos relacionados a el.
                    <?php }else echo 'error al crear el producto'; ?>
                </td>
            </tr>
          <?php }?>
          <?php if( isset($_GET['del']) ){ ?>
            <tr>
                <td class="EstiloGreen" colspan="5">Producto Eliminado</td>
            </tr>
          <?php }?>
          <tr>
            <th><a href='#' title="Click para ordenar">Nombre (ref)</a></th>
            <th><a href='#' title="Click para ordenar">Categoria /Sub-Categorías</a></th>
            <th><a href='#' title="Click para ordenar">Precio</a></th>
            <th><a href='#' title="Click para ordenar">Descuento</a></th>
            <th><a href='#' title="Click para ordenar">Stock</a></th>
            <th><a href='#' title="Click para ordenar">Accion</a></th>
          </tr>
        </thead>
        <tbody>
        <?php
        foreach ($lista as $item) {
        $dir = './../php/action/productoDel2.php?id='.$item->getId().'&p='.$p;
        ?>
          <tr>
            <td>
                <?php echo $item->getNombre();?> (<?php echo $item->getRef();?>)
                <br />
                <?php if($item->getNovedad()) echo '<img src="../imagenes/nov.png" atl="" />'; ?>
                <?php if($item->getRegalar()) echo '<img src="../imagenes/reg.png" atl="" />'; ?>
                <?php if($item->getVendido()) echo '<img src="../imagenes/ven.png" atl="" />'; ?>
                <?php if($item->getRecomendado()) echo '<img src="../imagenes/rec.png" atl="" />'; ?>
                <?php if($item->getPopular()) echo '<img src="../imagenes/pop.png" atl="" />'; ?>
                <?php if($item->getPromocion()) echo '<img src="../imagenes/promo.png" atl="" />'; ?>
            </td>
            <td>
                <?php
                $cat = $cat1DAO->getById($item->getIdCat1());
                if($cat != null)
                    echo $cat->getNombre();
                else
                    echo '<span style="color:red">No encontrada</span>';
                ?>
                /
                <?php
                    $cat2 = $cat2DAO->getById($item->getIdCat2());
                    if($cat2 != null)
                        echo $cat2->getNombre();
                    else
                        echo '<span style="color:red">No encontrada</span>';
                ?>
                <?php
                    for($i=1;$i<=5;$i++){
                        if($item->{"getIdCat1".$i}() != 0 ){
                            $cat2 = $cat2DAO->getById($item->{"getIdCat1".$i}());
                            if($cat2 != null)
                                echo '<br />'.$cat2->getNombre();
                        }
                    }
                ?>
            </td>
            <td>
                <?php if($item->getDescuento()){  ?>
                $<strike><?php echo $item->getPrecioFormatoDescuento(); ?></strike><br />
                <?php } ?>
                <?php echo $item->getPrecioFormato(); ?>
            </td>
            <td>
                <?php if($item->getDescuento() == 0){  ?>
                0%
                <?php }else{ echo (round( (100 - ($item->getPrecio()*100)/$item->getPrecioDescuento()), 2  ))."%"; } ?>
            </td>
            <td>
                <?php echo $item->getStock(); ?>
            </td>
            <td>
                <a href="javascript:confirmar('<?php echo $dir; ?>',' (<?php echo $item->getNombre();?>)')" title="Eliminar">Eliminar</a>
                |
                <a href="menuAdmin.php?s=productosEdit2&id=<?php echo $item->getId(); ?>&p=<?php echo $p;?>">Editar</a>
                <br />
                <input type="checkbox" name="prod-<?php echo $item->getId(); ?>" value="<?php echo $item->getId(); ?>"><br />
            </td>
          </tr>
      <?php } ?>
        </tbody>
<!--
        <tfoot>
          <tr id="pagerOne">
            <td colspan="5"><img src="jquery/jQueryTableSorterConPaging/_assets/img/first.png" class="first"/> <img src="jquery/jQueryTableSorterConPaging/_assets/img/prev.png" class="prev"/>
                <input type="text" class="pagedisplay"/>
                <img src="jquery/jQueryTableSorterConPaging/_assets/img/next.png" class="next"/> <img src="jquery/jQueryTableSorterConPaging/_assets/img/last.png" class="last"/>
                <select name="select" class="pagesize">
                  <option selected="selected"  value="10">10</option>
                  <option value="20">20</option>
                  <option value="30">30</option>
                  <option  value="40">40</option>
                </select>            </td>
          </tr>
        </tfoot>
-->
      </table>
        <input type="hidden" name="p" value="<?php echo $_GET['p'];?>">
        <div style="text-align: right">
            <input type="button" value="Seleccionar todos" onclick="checkedAll(); return false;">
            <input type="submit" value="Eliminar seleccionados" onclick="return confirmarForm2(overall, 'Esta seguro de que desea eliminar los elementos seleccionados?');">
        </div>
    </form>
      <br />
      <div style="font-size: x-small;">
          <img src="../imagenes/nov.png" alt="" /> = Novedad <br />
          <img src="../imagenes/pop.png" alt="" /> = Más Popular <br />
          <img src="../imagenes/rec.png" alt="" /> = Recomendado <br />
          <img src="../imagenes/reg.png" alt="" /> = Para Regalar <br />
          <img src="../imagenes/ven.png" alt="" /> = Más Vendido <br />
          <img src="../imagenes/promo.png" alt="" /> = Promoción <br />
      </div>
    </div>
  </div>
  <!-- FIN SUBCONTENIDO -->
  <!-- FIN SUBCONTENIDO -->
</div>
<!-- FIN CONTENIDO FONDO -->
<div class="contenido_marco_inf"></div>
<script>
    function checkEnter(e){
     e = e || event;
     return (e.keyCode || event.which || event.charCode || 0) !== 13;
    }
    checked = false;
      function checkedAll () {
        if (checked == false){checked = true}else{checked = false}
        for (var i = 0; i < document.getElementById('overall').elements.length; i++) {
          document.getElementById('overall').elements[i].checked = checked;
        }
      }
</script>