<?php

if( !isset($_SESSION['admin']) ){
    header("location: ./index.php");
    exit;
}

$gets = '';
foreach($_GET as $key => $value){
    $gets .= '&'.$key.'='.$value;
}

$orderDAO = new orderDAO;
$orderLineDAO = new orderLinesDAO();
$pedidos = $orderDAO->gets("id", "asc");
$total = $orderDAO->total();
$totalAmount = $orderDAO->totalAmount();

$clientID = ( isset($_GET['filtroUser']) ) ? $_GET['filtroUser'] : 0;
$desde = ( isset($_GET['desde']) && $_GET['desde'] != "" ) ? $_GET['desde'] : "01-01-1980";
$hasta = ( isset($_GET['hasta']) && $_GET['hasta'] != "" ) ? $_GET['hasta'] : "01-01-2030";

$totalAmountUser = $orderDAO->totalAmountUser($clientID);

$userDAO = new userDAO();
$productDAO = new productoDAO();
$cat1DAO = new cat1DAO();
$cat2DAO = new cat2DAO();

$users = $userDAO->getUsers("nombre", "asc", 0, 9999);
$cats1 = $cat1DAO->gets("nombre", "asc", 0, 9999);
$cats2 = $cat2DAO->gets("nombre", "asc", 0, 9999);



if( isset($_GET['filter']) && $_GET['filter'] != 0 ){
    
        for($i = count($pedidos) - 1; $i >= 0 ; $i--){
            if( $_GET['filtroUser'] != "" && $_GET['filtroUser'] != 0 ){
                $user = $userDAO->getById($pedidos[$i]->getUserId());
                if( $user->getId() != $_GET['filtroUser'] ){
                    unset($pedidos[$i]);
                    continue;
                }

            }

            if( $_GET['filtroCat1']  != "" && $_GET['filtroCat1']  != 0 ){

            $lines = $orderLineDAO->getsByPedido("id", "asc", $pedidos[$i]->getId());
            $eliminar = true;
            foreach($lines as $productLine){
                $p = $productDAO->getById($productLine->getIdProduct());
                if( $p == null ){
                    $p = new producto ();
                    $p->setIdCat1(0);
                }
                $cat1 = $cat1DAO->getById($p->getIdCat1());
                if( $cat1 == null ){
                    $cat1 = new cat1();
                }

                //filtro cat
                if( $cat1->getId() == $_GET['filtroCat1'] ){
                    $eliminar = false;
                }

                //filtrar por subcat2
                if( $_GET['filtroCat2']  != "" && $_GET['filtroCat2']  != 0 ){
                    $cat2 = $cat2DAO->getById($p->getIdCat2());
                    if( $cat2 == null ){
                        $cat2 = new cat2();
                    }

                    if( $cat2->getId() != $_GET['filtroCat2'] ){
                        $eliminar = true;
                    }
                    else
                        break;
                }
            }

            if( $eliminar )
                unset($pedidos[$i]);
        }
    }
}


$totalSeen = 0;
foreach ($pedidos as $pedido) {
    //filtro fechas
    if( isset($_GET['filter']) && $_GET['filter'] == 1 ){

        $order_date = strtotime(str_replace("/", "-", $pedido->getDate()));
        $init_date = strtotime(str_replace("/", "-", $desde));
        $end_date = strtotime(str_replace("/", "-", $hasta));
        if( $order_date > $end_date || $order_date < $init_date )
            continue;
    }
    $totalSeen += $pedido->getTotal();
}
?>
                    <div class="titulos">
                      <div class="titulos_texto1">Reportes<div class="cerrar"><a href="../php/action/logout.php"><img src="imagenes/contenido/cerrar.png" alt="Cerrar Sesi&oacute;n" border="0" /></a></div></div>
                      <div class="titulos_texto2">
                      </div>
                    </div>
                    <!-- FIN TITULOS -->
                    <div class="contenido_marco_sup"></div>
                    <div class="contenido_fondo">
                      <div class="subcontenido">

<div class="subtitulos">
    Un total de <?php echo $total;?> compras realizadas<div class="subtitulos_menu">
</div>
</div>
<div class="subcontenido2">

    <table width="750" border="0">
        <tr>
            <td width="300" rowspan="2" valign="top">
                <form method="get" action="menuAdmin.php">
                    Filtrar Fechas:<br />
                    Desde: <input type="text" name="desde" id="datepicker" value="<?php echo $_GET['desde']?>" />
                    <br />
                    Hasta:&nbsp; <input type="text" id="datepicker2" name="hasta"  value="<?php echo $_GET['hasta']?>" />
                    <br />
                    <input type="submit" value="Filtrar">
                    <input type="hidden" name="s" value="reportes" />
                    <input type="hidden" name="filter" value="1" />
                    <input type="hidden" name="filtroUser" value="<?php echo $_GET['filtroUser'];?>" />
                    <input type="hidden" name="filtroCat1" value="<?php echo $_GET['filtroCat1'];?>" />
                    <input type="hidden" name="filtroCat2" value="<?php echo $_GET['filtroCat2'];?>" />
                </form>
            </td>
            <td colspan="3">
                <form action="">
                    Buscar cliente:<br />
                    Nombre:
                    <form action="" method="get">
                    <select name="filtroUser">
                        <option value="0"></option>
                      <?php foreach($users as $u){ if($u->getIsAdmin()) continue; ?>
                      <option value="<?php echo $u->getId();?>" <?php if($u->getId() == $_GET['filtroUser']) echo 'selected' ?>>
                          <?php echo $u->getNombre();?> <?php echo $u->getApellidos();?>
                      </option>
                      <?php } ?>
                    </select>
                    <input type="hidden" name="s" value="reportes" />
                    <input type="hidden" name="filter" value="1" />
                    <input type="submit" value="Buscar" />
                  </form>
                    
                </form>
            </td>
        </tr>
        <tr>
            <td>
                Total Compras del cliente:
                <br />
                <?php echo number_format($totalAmountUser,  2, ',', '.')?> COP
            </td>
            <td align="right">
                <b>Total Compras:</b>
                <br />
                <?php echo number_format($totalAmount,  2, ',', '.')?> COP
            </td>
        </tr>
    </table>
    <br />
    <table border="0" width="700">
        <tr>
              <td colspan="2">
                  Filtrar por:<br  />
                  <form action="" method="get">
                    <select onchange="this.form.submit()" name="filtroUser">
                    <option value="0">Clientes</option>
                      <?php foreach($users as $u){  if($u->getIsAdmin()) continue;?>
                      <option value="<?php echo $u->getId();?>" <?php if($u->getId() == $_GET['filtroUser']) echo 'selected' ?>>
                          <?php echo $u->getNombre();?>
                      </option>
                      <?php } ?>
                    </select>
                    <input type="hidden" name="s" value="reportes" />
                    <input type="hidden" name="filter" value="1" />
                    <input type="hidden" name="filtroCat1" value="<?php echo $_GET['filtroCat1'];?>" />
                    <input type="hidden" name="filtroCat2" value="<?php echo $_GET['filtroCat2'];?>" />
                  </form>
              </td>
              <td colspan="1">
                  <br  />
                  <form action="" method="get">
                  <select onchange="this.form.submit()" name="filtroCat1">
                      <option>Categorias</option>
                      <?php foreach($cats1 as $cat1Fil){  ?>
                      <option value="<?php echo $cat1Fil->getId();?>" <?php if($cat1Fil->getId() == $_GET['filtroCat1']) echo 'selected' ?>>
                          <?php echo $cat1Fil->getNombre();?>
                      </option>
                      <?php } ?>
                  </select>
                      <input type="hidden" name="s" value="reportes" />
                      <input type="hidden" name="filter" value="1" />
                      <input type="hidden" name="filtroUser" value="<?php echo $_GET['filtroUser'];?>" />
                  </form>
              </td>
              <td colspan="2" >
                  <br  />
                  <form action="" method="get">
                  <select onchange="this.form.submit()" name="filtroCat2">
                      <option>Sub Categorias</option>
                      <?php foreach($cats2 as $cat2Fil){ if( $cat2Fil->getIdCat1() != $_GET['filtroCat1'] ) continue; ?>
                      <option value="<?php echo $cat2Fil->getId();?>" <?php if($cat2Fil->getId() == $_GET['filtroCat2']) echo 'selected' ?>>
                          <?php echo $cat2Fil->getNombre();?>
                      </option>
                      <?php } ?>
                  </select>
                      <input type="hidden" name="s" value="reportes" />
                      <input type="hidden" name="filter" value="1" />
                      <input type="hidden" name="filtroCat1" value="<?php echo $_GET['filtroCat1'];?>" />
                      <input type="hidden" name="filtroUser" value="<?php echo $_GET['filtroUser'];?>" />
                  </form>
            </td>
            <td>
                <br />
                <a href="reportesExport.php?<?php echo $gets;?>">
                Exportar XSL
                </a>
            </td>
          </tr>
    </table>
<form runat="server">
	  <div id="buscador">
      <div class="rowElem"></div>
      </div>
      <div class="enunciados"></div>
      <table width="100%" class="yui" id="tableOne">
        <thead>
          <?php if( isset($_GET['ok']) ){ ?>
            <tr>
                <td class="EstiloGreen" colspan="6">Producto añadido</td>
            </tr>
          <?php }?>
          <?php if( isset($_GET['del']) ){ ?>
            <tr>
                <td class="EstiloGreen" colspan="6">Producto Eliminado</td>
            </tr>
          <?php }?>
          <tr>
            <th><a href='#' title="Click para ordenar">Cliente</a></th>
            <th><a href='#' title="Click para ordenar">Fecha</a></th>
            <th><a href='#' title="Click para ordenar">Productos</a></th>
            <th><a href='#' title="Click para ordenar">Categoria</a></th>
            <th><a href='#' title="Click para ordenar">Subcategoria</a></th>
            <th><a href='#' title="Click para ordenar">Valor</a></th>
          </tr>
        </thead>
        <tbody>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td align="right">
                  <b><?php echo number_format($totalSeen,  2, ',', '.');?></b>
              </td>
          </tr>
        <?php
        foreach ($pedidos as $pedido) {
            $user = $userDAO->getById($pedido->getUserId());
            if( $user == null ){
                $user = new user();
                $user->setNombre("No disponible");
                $user->setId(0);
            }

            //filtro fechas
            if( isset($_GET['filter']) && $_GET['filter'] == 1 ){
                
                $order_date = strtotime(str_replace("/", "-", $pedido->getDate()));
                $init_date = strtotime(str_replace("/", "-", $desde));
                $end_date = strtotime(str_replace("/", "-", $hasta));
                if( $order_date > $end_date || $order_date < $init_date )
                    continue;
            }

            $lines = $orderLineDAO->getsByPedido("id", "asc", $pedido->getId());
            $dir = './../php/action/pedidoDel.php?id='.$pedido->getId();
        ?>
          <tr>
            <td>
                <a href="menuAdmin.php?s=usuariosVer&id=<?php echo $user->getId();?>">
                <?php echo $user->getNombre();?>
                </a>
            </td>
            <td><?php echo $pedido->getDate();?></td>
            <td width="200">
                <ul>
                <?php
                foreach($lines as $productLine){
                    $p = $productDAO->getById($productLine->getIdProduct());
                    if( $p == null ){
                        $p = new producto ();
                        $p->setNombre("No disponible");
                    }

                    echo '<li>'.$productLine->getQuantity().' x '.$p->getNombre().'</li>';
                }
                ?>
                </ul>
            </td>
            <td>
                <ul>
                <?php
                foreach($lines as $productLine){
                    $p = $productDAO->getById($productLine->getIdProduct());
                    if( $p == null ){
                        $p = new producto ();
                        $p->setNombre("No disponible");
                        $p->setIdCat1(0);
                    }
                    $cat1 = $cat1DAO->getById($p->getIdCat1());
                    if( $cat1 == null ){
                        $cat1 = new cat1();
                        $cat1->setNombre("no disponible");
                    }
                    echo '<li>'.$cat1->getNombre().'</li>';
                }
                ?>
                </ul>
            </td>
            <td>
                <ul>
                <?php
                foreach($lines as $productLine){
                    $p = $productDAO->getById($productLine->getIdProduct());
                    if( $p == null ){
                        $p = new producto ();
                        $p->setNombre("No disponible");
                        $p->setIdCat1(0);
                    }
                    $cat1 = $cat2DAO->getById($p->getIdCat2());
                    if( $cat1 == null ){
                        $cat1 = new cat1();
                        $cat1->setNombre("no disponible");
                    }
                    echo '<li>'.$cat1->getNombre().'</li>';
                }
                ?>
                </ul>
            </td>
            <td>
                <a href="menuAdmin.php?s=pedidosVer&id=<?php echo $pedido->getId();?>">
                    <?php echo number_format($pedido->getTotal(),  2, ',', '.');?> <?php echo $pedido->getDivisa();?>
                </a>
            </td>
          </tr>
      <?php } ?>
        </tbody>
<!--
        <tfoot>
          <tr id="pagerOne">
            <td colspan="5"><img src="jquery/jQueryTableSorterConPaging/_assets/img/first.png" class="first"/> <img src="jquery/jQueryTableSorterConPaging/_assets/img/prev.png" class="prev"/>
                <input type="text" class="pagedisplay"/>
                <img src="jquery/jQueryTableSorterConPaging/_assets/img/next.png" class="next"/> <img src="jquery/jQueryTableSorterConPaging/_assets/img/last.png" class="last"/>
                <select name="select" class="pagesize">
                  <option selected="selected"  value="10">10</option>
                  <option value="20">20</option>
                  <option value="30">30</option>
                  <option  value="40">40</option>
                </select>            </td>
          </tr>
        </tfoot>
-->
      </table>
    </form>
    </div>
  </div>
  <!-- FIN SUBCONTENIDO -->
  <!-- FIN SUBCONTENIDO -->
</div>
<!-- FIN CONTENIDO FONDO -->
<div class="contenido_marco_inf"></div>
