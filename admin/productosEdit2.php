<?php
if( !isset($_SESSION['admin']) ){
    header("location: ./index.php");
    exit;
}

include("fckeditor/fckeditor.php") ;

$p = $_GET['p'];
$id = $_GET['id'];

$productoDAO = new productoDAO();
$producto = $productoDAO->getById($id);


$cat1DAO = new cat1DAO();
$cat1 = $cat1DAO->getById($producto->getIdCat1());

$idCat1 = $producto->getIdCat1();

$cat2DAO = new cat2DAO();
$cat2s = $cat2DAO->getsByIdCat1("nombre", "asc", $producto->getIdCat1());


$productos = $productoDAO->gets("nombre", "asc", 0, 10000);
$productosCat1 = $productoDAO->search("nombreSimple", "asc", 0, 10000, 'idCat1 = "'.$idCat1.'"');
$productosCat2 = $productoDAO->search("nombreSimple", "asc", 0, 10000, 'idCat2 = "'.$producto->getIdCat2().'"');

$pRelDAO = new proRelDAO();
$pRels = $pRelDAO->getsByIdPro("id", "asc", $id);
?>
                    <div class="titulos">
                      <div class="titulos_texto1">Productos de <b><?php echo $cat1->getNombre();?></b><div class="cerrar"><a href="../php/action/logout.php"><img src="imagenes/contenido/cerrar.png" alt="Cerrar Sesi&oacute;n" border="0" /></a></div></div>
                      <div class="titulos_texto2">
                      </div>
                    </div>
                    <!-- FIN TITULOS -->
                    <div class="contenido_marco_sup"></div>
                    <div class="contenido_fondo">
                      <div class="subcontenido">

<div class="subtitulos">
 <table width="700">
    <tr>
        <td>
            <a href="menuAdmin.php?s=productos2&p=<?php echo $p;?>">
            <?php echo $p;?>
            </a>
            >
            <b>Editar</b>
        </td>
    </tr>
 </table>
<div class="subtitulos_menu">
<!--  <form id="form_boton" ><input type="button" value="Adicionar Nuevo Equipo" id="nuevo" /></form> -->
</div>
</div>
<div class="subcontenido2">
<table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
</table>
	  <div id="buscador">
      <div class="rowElem"></div>
      </div>
      <div class="enunciados"></div>
      <form action="../php/action/productoEdit2.php" method="post" enctype="multipart/form-data">
      <table border="0" align="l" width="600" border="0" cellspacing="4" cellpadding="4">
         <?php if( isset($_GET['error1']) ){?>
           <tr>
             <td colspan="4" class="EstiloRed">Tienes que introducir todos los datos</td>
             </tr>
        <?php }if( isset($_GET['error2']) ){?>
            <tr>
             <td colspan="4" class="EstiloRed">Tienes que introducir la 'imagen'</td>
             </tr>
        <?php }if( isset($_GET['error3']) ){?>
            <tr>
             <td colspan="4" class="EstiloRed">Precio y Stock deben ser caracteres numéricos</td>
             </tr>
        <?php }if( isset($_GET['ok']) ){?>
            <tr>
             <td colspan="4" class="EstiloGreen">Datos almacenados</td>
             </tr>
        <?php }?>
          <tr>
            <td colspan="2"><b><font style="font-size:large;color:#154894">Información Básica</font></b></td>
          </tr>
          <tr bgcolor="#efefef">
            <td>
                Nombre:<font style="font-size:x-small;color:red">*</font>
            </td>
            <td>
                <input size="35" name="nombre" value="<?php echo $producto->getNombre();?>">
            </td>
          </tr>
          <tr bgcolor="#efefef">
            <td>
                Referencia:<font style="font-size:x-small;color:red">* ÚNICA PARA CADA PEDIDO</font>
            </td>
            <td>
                <input size="35" name="ref" value="<?php echo $producto->getRef();?>">
            </td>
          </tr>
          <tr>
            <td>
                Tiene IVA:<font style="font-size:x-small;color:red">*</font>
            </td>
            <td>
                <input size="35" name="iva" type="checkbox" <?php if($producto->getIva()) echo 'checked'; ?> >
            </td>
          </tr>
          <tr>
            <td>
                Precio:<font style="font-size:x-small;color:red">* no poner simbolo de miles!</font>
            </td>
            <td>
                <input size="35" name="precio" value="<?php echo $producto->getPrecio();?>">
            </td>
          </tr>
          <tr>
            <td>
                Descuento:<font style="font-size:x-small;color:red">*</font>
            </td>
            <td>
                <input size="35" name="descuento" type="checkbox" <?php if($producto->getDescuento()) echo 'checked'; ?> >
            </td>
          </tr>
          <tr>
            <td>
                Precio Si hay descuento:
                <br /><font style="font-size:x-small;color:red">* no poner simbolo de miles!</font>
                <br /><font style="font-size:x-small;color:red">* este sería el precio que quedaría tachado</font>
            </td>
            <td>
                <input size="35" name="precioDescuento" value="<?php echo $producto->getPrecioDescuento();?>">
            </td>
          </tr>
          <tr bgcolor="#efefef">
            <td>
                Stock:<font style="font-size:x-small;color:red">* número de productos disponibless</font>
            </td>
            <td>
                <input size="35" name="stock" value="<?php echo $producto->getStock();?>">
            </td>
          </tr>
          <tr>
            <td>
                Sub-Categoría:<font style="font-size:x-small;color:red">*</font>
            </td>
            <td>
                <select name="idCat2">
                    <?php foreach($cat2s as $cat2){ ?>
                    <option value="<?php echo $cat2->getId();?>" <?php if( $cat2->getId() == $producto->getIdCat2() ) echo 'selected';?>>
                        <?php echo $cat2->getNombre();?>
                    </option>
                    <?php } ?>
                </select>
            </td>
          </tr>
          <?php for($i=1;$i<=5; $i++){ ?>
          <tr>
            <td>
                Sub-Categoría adicional (<?php echo $i;?>):
            </td>
            <td>
                <select name="idCat1<?php echo $i;?>">
                    <option value="0">No</option>
                    <?php foreach($cat2s as $cat2){ ?>
                    <option value="<?php echo $cat2->getId();?>" <?php if( $cat2->getId() == $producto->{"getIdCat1".$i}() ) echo 'selected';?>>
                        <?php echo $cat2->getNombre();?>
                    </option>
                    <?php } ?>
                </select>
            </td>
          </tr>
          <?php } ?>
          <tr bgcolor="#efefef">
              <td valign="top">Datos Generales:</td>
              <td>
                  <?php
                $oFCKeditor_es = new FCKeditor('datos') ;
                $oFCKeditor_es->BasePath = 'fckeditor/';
                $oFCKeditor_es->Width  = '350' ;
                $oFCKeditor_es->Height = '210' ;
                $oFCKeditor_es->Value = $producto->getDatos();
                $oFCKeditor_es->ToolbarSet = 'CamiloCifuentes';
                $oFCKeditor_es->Create() ;
                ?>
              </td>
          </tr>
          <tr>
              <td valign="top">Datos Específicos:</td>
              <td>
                  <textarea name="datosTecnicos" rows="3" cols="30"><?php echo $producto->getDatosTecnicos();?></textarea>
              </td>
          </tr>
          <tr bgcolor="#efefef">
            <td valign="top">
                Descripción:
            </td>
            <td>
                <?php
                $oFCKeditor_es = new FCKeditor('corta') ;
                $oFCKeditor_es->BasePath = 'fckeditor/';
                $oFCKeditor_es->Width  = '350' ;
                $oFCKeditor_es->Height = '210' ;
                $oFCKeditor_es->Value = $producto->getCorta();
                $oFCKeditor_es->ToolbarSet = 'CamiloCifuentes';
                $oFCKeditor_es->Create() ;
                ?>
            </td>
          </tr>
          <tr>
            <td colspan="2"><b><font style="font-size:large;color:#154894">Destacar en</font></b></td>
          </tr>
          <tr bgcolor="#efefef">
              <td>
                  <input type="checkbox" name="novedad" <?php if($producto->getNovedad()) echo 'checked'; ?>  /> Novedad <br />
                  <input type="checkbox" name="regalar" <?php if($producto->getRegalar()) echo 'checked'; ?> /> Para Regalar <br />
                  <input type="checkbox" name="vendido" <?php if($producto->getVendido()) echo 'checked'; ?> /> Más Vendido <br />
              </td>
              <td>
                  <input type="checkbox" name="recomendado" <?php if($producto->getRecomendado()) echo 'checked'; ?> /> Recomendado <br />
                  <input type="checkbox" name="popular" <?php if($producto->getPopular()) echo 'checked'; ?> /> Más Popular <br />
                  <input type="checkbox" name="promocion" <?php if($producto->getPromocion()) echo 'checked'; ?> /> En Promoción <br />
              </td>
          </tr>
          <tr>
            <td colspan="2"><b><font style="font-size:large;color:#154894">Imágenes</font></b></td>
          </tr>
          <tr bgcolor="#efefef">
              <td>
                  Imagen Principal
                  <br />
                  <span style="color: red;font-size: x-small">* Tamaño: 113 x 80px!!</span>
                  <?php if($producto->getImg() != ""){ ?>
                  <br />
                  <img alt="" src="../imagenes/productos/<?php echo $producto->getImg()?>"/>
                  <?php } ?>
              </td>
              <td>
                  <input type="file" name="img">
              </td>
          </tr>
          <?php for($i = 1; $i <= 5 ; $i++){ ?>
          <tr  <?php if($i%2 == 0) echo 'bgcolor="#efefef"'; ?> >
              <td>
                  Imagen <?php echo $i;?>
                  <?php if($producto->{"getImg".$i}() != ""){ ?>
                  <br />
                  <img alt="" src="../imagenes/productos/thumb_<?php echo $producto->{"getImg".$i}()?>" width="115"/>
                  <br />
                  <span style="font-size: x-small;">
                  <input type="checkbox" name="borrar<?php echo $i;?>"> Borrar?
                  </span>
                  <?php } ?>
              </td>
              <td>
                  <input type="file" name="img<?php echo $i;?>">
              </td>
          </tr>
          <?php  } ?>
          <tr>
            <td></td>
            <td></td>
          </tr>
          <tr>
              <td align="right" colspan="2">
                <input type="hidden" name="id" value="<?php echo $id;?>">
                <input type="hidden" name="p" value="<?php echo $p;?>">
                <input type="submit" value="Guardar">
            </td>
          </tr>
          <tr>
            <td colspan="2">
                <font style="font-size:x-small;color:red">
                    * campos obligatorios
                </font>
            </td>
          </tr>
      </table>
      </form>

      <table style="border:solid 1px #aaaaaa" width="700">
          <tr>
              <tr>
                <td colspan="4"><b><font style="font-size:large;color:#154894">Productos Relacionados</font></b></td>
              </tr>
              <tr bgcolor="#efefef">
                  <td colspan="4">
                      <form action="../php/action/productoEditRel2.php" method="post">
                          Por orden Alfabetico (todos)
                          <select name="idProductoRel">
                              <?php foreach ($productos as $item){?>
                              <option value="<?php echo $item->getId();?>">
                                <?php echo $item->getNombre();?>
                              </option>
                              <?php } ?>
                          </select>
                            <input type="hidden" name="id" value="<?php echo $id;?>">
                            <input type="hidden" name="p" value="<?php echo $p;?>">
                            <input type="submit" value="Añadir">
                      </form>
                  </td>
              </tr>
              <tr bgcolor="#efefef">
                  <td colspan="4">
                      <form action="../php/action/productoEditRel2.php" method="post">
                          Por orden Alfabetico (De la misma categoría)
                          <select name="idProductoRel">
                              <?php foreach ($productosCat1 as $item){?>
                              <option value="<?php echo $item->getId();?>">
                                <?php echo $item->getNombre();?>
                              </option>
                              <?php } ?>
                          </select>
                            <input type="hidden" name="id" value="<?php echo $id;?>">
                            <input type="hidden" name="p" value="<?php echo $p;?>">
                            <input type="submit" value="Añadir">
                      </form>
                  </td>
              </tr>
              <tr bgcolor="#efefef">
                  <td colspan="4">
                      <form action="../php/action/productoEditRel2.php" method="post">
                          Por orden Alfabetico (De la misma sub-categoría)
                          <select name="idProductoRel">
                              <?php foreach ($productosCat2 as $item){?>
                              <option value="<?php echo $item->getId();?>">
                                <?php echo $item->getNombre();?>
                              </option>
                              <?php } ?>
                          </select>
                            <input type="hidden" name="id" value="<?php echo $id;?>">
                            <input type="hidden" name="p" value="<?php echo $p;?>">
                            <input type="submit" value="Añadir">
                      </form>
                  </td>
              </tr>
              <tr>
                  <?php
                        $i = 0; foreach ($pRels as $pRel){
                        $pr = $productoDAO->getById($pRel->getIdProductoRel());
                        if($pr == null)
                            continue;
                  ?>
                  <td width="80" align="center">
                      <span style="font-size: x-small;">
                          <?php echo $pr->getNombre();?>
                          <br />
                          <img src="../imagenes/productos/<?php echo $pr->getImg();?>" alt="" width="60" />
                          <br />
                          <a href="../php/action/productoEditRelDel2.php?idRel=<?php echo $pRel->getId();?>&p=<?php echo $p;?>&id=<?php echo $id;?>">Eliminar</a>
                      </span>
                  </td>
                  <?php $i++; if($i%4 == 0) echo '</tr><tr>'; } ?>
              </tr>
          </tr>
      </table>
      <a name="relacionados"></a>
    </div>
  </div>
  <!-- FIN SUBCONTENIDO -->
  <!-- FIN SUBCONTENIDO -->
</div>
<!-- FIN CONTENIDO FONDO -->
<div class="contenido_marco_inf"></div>
