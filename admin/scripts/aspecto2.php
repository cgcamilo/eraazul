<?php
if( !isset($_SESSION['admin']) ){
    header("location: ./index.php");
    exit;
}

$aspectoNombre = $_GET['a'];

$aspectoDAO= new AspectsDAO;
$aspecto = $aspectoDAO->getAspect($aspectoNombre);

?>
                    <div class="titulos">
                      <div class="titulos_texto1">Editar Zonas<div class="cerrar"><a href="../php/action/logout.php"><img src="imagenes/contenido/cerrar.png" alt="Cerrar Sesi&oacute;n" border="0" /></a></div></div>
                      <div class="titulos_texto2">
                      </div>
                    </div>
                    <!-- FIN TITULOS -->
                    <div class="contenido_marco_sup"></div>
                    <div class="contenido_fondo">
                      <div class="subcontenido">

<div class="subtitulos">
 <table width="700">
    <tr>
        <td>
            Editar '<?php echo $aspecto->getDescrip(); ?>'
        </td>
    </tr>
 </table>
<div class="subtitulos_menu">
<!--  <form id="form_boton" ><input type="button" value="Adicionar Nuevo Equipo" id="nuevo" /></form> -->
</div>
</div>
<div class="subcontenido2">
<table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
</table>
	  <div id="buscador">
      <div class="rowElem"></div>
      </div>
      <div class="enunciados"></div>

      <table border="0" align="center" width="400" border="0" cellspacing="4" cellpadding="4">
      <?php if( isset($_GET['ok']) ){ ?>
            <tr>
                <td class="EstiloGreen" colspan="2">Zona Editada correctamente</td>
            </tr>
          <?php }?>
          <?php if( isset($_GET['error1']) ){ ?>
            <tr>
                <td class="EstiloRed" colspan="2">Tienes que introducir algo</td>
            </tr>
      <?php }?>
      </table>


      <form action="../php/action/aspectEdit2.php" name="0" method="post" enctype="multipart/form-data">
      <table border="0" align="center" width="400" border="0" cellspacing="4" cellpadding="4">
          <tr>
            <td colspan="2">
                <img src="../images/<?php echo $aspecto->getValue(); ?>">
            </td>
          </tr>
          <tr>
            <td colspan="2"><b><font style="font-size:large;color:#154894">Cambiar</font></b></td>
          </tr>
          <tr>
            <td colspan="2">
                <input name="file" type="file">
            </td>
          </tr>
          <tr>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="2" align="right">
                <input type="hidden" name="nombre" value="<?php echo $aspecto->getName();?>">
                <input type="hidden" name="s" value="400">
                <input type="hidden" name="a" value="<?php echo $aspectoNombre; ?>">
                <input type="image" src="imagenes/botones/bt_modificar1.png">
            </td>
          </tr>
          </table>
          </form>
    </div>
  </div>
  <!-- FIN SUBCONTENIDO -->
  <!-- FIN SUBCONTENIDO -->
</div>
<!-- FIN CONTENIDO FONDO -->
<div class="contenido_marco_inf"></div>
