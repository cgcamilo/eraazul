<?php

if( !isset($_SESSION['admin']) ){
    header("location: ./index.php");
    exit;
}

$idCat1 = $_GET['idCat1'];

$cat1DAO = new cat1DAO();
$cat1 = $cat1DAO->getById($idCat1);

$cat2DAO = new cat2DAO();
$cat2s = $cat2DAO->getsByIdCat1("nombre", "asc", $idCat1);
?>
                    <div class="titulos">
                      <div class="titulos_texto1">Productos<div class="cerrar"><a href="../php/action/logout.php"><img src="imagenes/contenido/cerrar.png" alt="Cerrar Sesi&oacute;n" border="0" /></a></div></div>
                      <div class="titulos_texto2">
                      </div>
                    </div>
                    <!-- FIN TITULOS -->
                    <div class="contenido_marco_sup"></div>
                    <div class="contenido_fondo">
                      <div class="subcontenido">

<div class="subtitulos">
    <table>
        <tr>
            <td>
                <a href="menuAdmin.php?s=productos&idCat1=<?php echo $idCat1;?>">
                Productos
                </a>
                >
                Importar
            </td>
        </tr>
    </table>
<div class="subtitulos_menu">
<!--  <form id="form_boton" ><input type="button" value="Adicionar Nuevo Equipo" id="nuevo" /></form> -->
</div>
</div>
<div class="subcontenido2">

<table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
</table>
	  <div id="buscador">
      <div class="rowElem"></div>
      </div>
      <div class="enunciados"></div>

      <form method="post" action="../php/action/productoImport.php" enctype="multipart/form-data">
      <table cellpadding="2" cellspacing="2">
        <?php if( isset($_GET['ok']) ){ ?>
        <tr>
            <td colspan="2" style="color:green">
                Se actualizaron <?php echo $_GET['ok'];?> productos
            </td>
        </tr>
        <?php } ?>
        <?php if( isset($_GET['error']) ){ ?>
        <tr>
            <td colspan="2" style="color:red">
                No se introdujo un archivo o no se pudo leer.
            </td>
        </tr>
        <?php } ?>
        <tr>
            <td style="font-size: x-small;">
                Las cantidades se actualizarán para los productos cuya referencia sea encontrada.
                <br />
                Haga clic <a href="../php/action/cantidades.xls">aqui</a> para descargar el xml de muestra
            </td>
        </tr>
        <tr>
            <td>
                Archivo <b>XLS</b>
            </td>
            <td>
                <input type="file" name="file" size="40" />
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" name="idCat1" value="<?php echo $idCat1;?>">
                <input type="submit" value="Importar">
            </td>
        </tr>
      </table>
      </form>

    </div>
  </div>
  <!-- FIN SUBCONTENIDO -->
  <!-- FIN SUBCONTENIDO -->
</div>
<!-- FIN CONTENIDO FONDO -->
<div class="contenido_marco_inf"></div>
