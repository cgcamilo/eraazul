<?php

if( !isset($_SESSION['admin']) ){
    header("location: ./index.php");
    exit;
}

$userDAO = new userDAO;
$listaUsuarios = $userDAO->getUsers("id","asc",0, 100);
$total = $userDAO->total();

?>
                    <div class="titulos">
                      <div class="titulos_texto1">Usuarios<div class="cerrar"><a href="../php/action/logout.php"><img src="imagenes/contenido/cerrar.png" alt="Cerrar Sesi&oacute;n" border="0" /></a></div></div>
                      <div class="titulos_texto2">
                      </div>
                    </div>
                    <!-- FIN TITULOS -->
                    <div class="contenido_marco_sup"></div>
                    <div class="contenido_fondo">
                      <div class="subcontenido">

<div class="subtitulos">Un total de <?php echo ($total-1);?> usuarios registrados<div class="subtitulos_menu">
<!--  <form id="form_boton" ><input type="button" value="Adicionar Nuevo Equipo" id="nuevo" /></form> -->
</div>
</div>
<div class="subcontenido2">
<form runat="server">
<table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
</table>
	  <div id="buscador">
      <div class="rowElem"></div>
      </div>
      <div class="enunciados"></div>
      <table width="100%" class="yui" id="tableOne">
        <thead>
          <tr>
            <td colspan="5" class="filter"><span class="rowElem">
              Buscar:
              <input id="filterBoxOne" value="" maxlength="30" size="30" type="text" />
              </span>
            </td>
          </tr>
        <?php if( isset($_GET['ok']) ){ ?>
            <tr>
                <td class="EstiloGreen" colspan="5">Usuario Activado/Bloqueado</td>
            </tr>
          <?php }?>
          <?php if( isset($_GET['ok2']) ){ ?>
            <tr>
                <td class="EstiloGreen" colspan="5">Usuario Eliminado (se eliminó tambien sus fotos y blogs, mensajes, ...)</td>
            </tr>
          <?php }?>
          <?php if( isset($_GET['errorDel1']) ){ ?>
            <tr>
                <td class="EstiloGreen" colspan="5">No se puede eliminar/bloquear al administrador</td>
            </tr>
          <?php }?>
          <tr>
            <th><a href='#' title="Click para ordenar">E-mail</a></th>
            <th><a href='#' title="Click para ordenar">Nombre completo</a></th>
            <th><a href='#' title="Click para ordenar">Pais</a></th>
            <th><a href='#' title="Click para ordenar">Ciudad</a></th>
            <th><a href='#' title="Click para ordenar">Accion</a></th>
          </tr>
        </thead>
        <tbody>
        <?php
        foreach ($listaUsuarios as $item) {
        if($item->getIsAdmin())
            continue;
        $dir = './../php/action/userDel.php?id='.$item->getId();
        ?>
          <tr>
            <td><?php echo $item->getLogin();?></td>
            <td><?php echo $item->getNombre().' '.$item->getApellidos();?></td>
            <td><?php echo $item->getPais();?></td>
            <td><?php echo $item->getCiudad();?></td>
            <td >
                <?php if( !$item->getIsAdmin() ){?>
                <a href="javascript:confirmar('<?php echo $dir; ?>',' Usuario (<?php echo $item->getLogin();?>)')" title="Eliminar">Eliminar</a>
                <?php }?>
                |
                <a href="menuAdmin.php?s=usuariosVer&id=<?php echo $item->getId(); ?>" >ver</a>

            </td>
          </tr>
      <?php } ?>
        </tbody>
<!--
        <tfoot>
          <tr id="pagerOne">
            <td colspan="5"><img src="jquery/jQueryTableSorterConPaging/_assets/img/first.png" class="first"/> <img src="jquery/jQueryTableSorterConPaging/_assets/img/prev.png" class="prev"/>
                <input type="text" class="pagedisplay"/>
                <img src="jquery/jQueryTableSorterConPaging/_assets/img/next.png" class="next"/> <img src="jquery/jQueryTableSorterConPaging/_assets/img/last.png" class="last"/>
                <select name="select" class="pagesize">
                  <option selected="selected"  value="10">10</option>
                  <option value="20">20</option>
                  <option value="30">30</option>
                  <option  value="40">40</option>
                </select>            </td>
          </tr>
        </tfoot>
-->
      </table>
    </form>

    </div>
  </div>
  <!-- FIN SUBCONTENIDO -->
  <!-- FIN SUBCONTENIDO -->
</div>
<!-- FIN CONTENIDO FONDO -->
<div class="contenido_marco_inf"></div>
