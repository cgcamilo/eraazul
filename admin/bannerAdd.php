<?php
if( !isset($_SESSION['admin']) ){
    header("location: ./index.php");
    exit;
}

include("fckeditor/fckeditor.php") ;

$productoDAO = new productoDAO();
$productos = $productoDAO->gets("nombre", "asc");

$bannerDAO = new bannerDAO();
$total = $bannerDAO->total();
if($total > 10)
    die;

?>
                    <div class="titulos">
                      <div class="titulos_texto1">Banner<div class="cerrar"><a href="../php/action/logout.php"><img src="imagenes/contenido/cerrar.png" alt="Cerrar Sesi&oacute;n" border="0" /></a></div></div>
                      <div class="titulos_texto2">
                      </div>
                    </div>
                    <!-- FIN TITULOS -->
                    <div class="contenido_marco_sup"></div>
                    <div class="contenido_fondo">
                      <div class="subcontenido">

<div class="subtitulos">
 <table width="700">
    <tr>
        <td>
            <a href="menuAdmin.php?s=banners">
            Banner
            </a>
            >
            <b>Nuevo</b>
        </td>
    </tr>
 </table>
<div class="subtitulos_menu">
<!--  <form id="form_boton" ><input type="button" value="Adicionar Nuevo Equipo" id="nuevo" /></form> -->
</div>
</div>
<div class="subcontenido2">
<table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
</table>
	  <div id="buscador">
      <div class="rowElem"></div>
      </div>
      <div class="enunciados"></div>
      <form action="../php/action/bannerAdd.php" method="post" enctype="multipart/form-data">
      <table border="0" align="l" width="600" border="0" cellspacing="4" cellpadding="4">
         <?php if( isset($_GET['error1']) ){?>
           <tr>
             <td colspan="4" class="EstiloRed">Tienes que introducir todos los datos</td>
             </tr>
        <?php }if( isset($_GET['error2']) ){?>
            <tr>
             <td colspan="4" class="EstiloRed">Tienes que introducir la 'imagen'</td>
             </tr>
        <?php }if( isset($_GET['error3']) ){?>
            <tr>
             <td colspan="4" class="EstiloRed">Tienes que introducir la URL ó seleccionar un producto</td>
             </tr>
        <?php }?>
          <tr>
            <td colspan="2"><b><font style="font-size:large;color:#154894">Información Básica</font></b></td>
          </tr>
          <tr>
            <td  colspan="2">
                Título:<font style="font-size:x-small;color:red">*</font>
                <br />
                <input size="35" name="nombre" value="<?php echo $_GET['nombre'];?>">
            </td>
          </tr>
          <tr>
              <td colspan="2" bgcolor="#eeeeee">
                  URL <font style="font-size: x-small;color: red">Introduce la URL ó el producto al cual redireccionará el enlace</font><br />
                  http://<input type="text" name="url" size="40"><br />
                  ó<br />
                  <select name="idProducto">
                      <option value="0">NO</option>
                      <?php foreach ($productos as $producto){ ?>
                      <option value="<?php echo $producto->getId();?>"><?php echo $producto->getNombre();?></option>
                      <?php } ?>
                  </select>
              </td>
          </tr>
          <tr>
            <td colspan="2"><b><font style="font-size:large;color:#154894">Imagen</font></b></td>
          </tr>
          <tr>
            <td>
                Imagen principal:
                <font style="font-size:x-small; color: red">
                    *
                    dimensiones
                    (794x260px) !!
                </font>
            </td>
            <td><input type="file" name="file"></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="2" align="right">
                <input type="submit" value="Guardar">
            </td>
          </tr>
          <tr>
            <td colspan="2">
                <font style="font-size:x-small;color:red">
                    * campos obligatorios
                </font>
            </td>
          </tr>
      </table>
      </form>
    </div>
  </div>
  <!-- FIN SUBCONTENIDO -->
  <!-- FIN SUBCONTENIDO -->
</div>
<!-- FIN CONTENIDO FONDO -->
<div class="contenido_marco_inf"></div>
