<?php

if( !isset($_SESSION['admin']) ){
    header("location: ./index.php");
    exit;
}


$idCat1 = $_GET['idCat1'];

$cat2DAO = new cat2DAO();

$cat1DAO = new cat1DAO();
$cat1 = $cat1DAO->getById($idCat1);

$productosDAO = new productoDAO();
$lista = $productosDAO->search("id", "asc", 0, 10000, 'idCat1 = "'.$idCat1.'"');
$total = $productosDAO->total(2, "idCat1", $idCat1);
?>
<script type="text/javascript" src="../js/jquery.dataTables.min.js" ></script>
<script>
$(document).ready(function(){
    $('#tableOne').dataTable();
});

</script>
                    <div class="titulos">
                        <div class="titulos_texto1">Productos de <b><?php echo $cat1->getNombre();?></b><div class="cerrar"><a href="../php/action/logout.php"><img src="imagenes/contenido/cerrar.png" alt="Cerrar Sesi&oacute;n" border="0" /></a></div></div>
                      <div class="titulos_texto2">
                      </div>
                    </div>
                    <!-- FIN TITULOS -->
                    <div class="contenido_marco_sup"></div>
                    <div class="contenido_fondo">
                      <div class="subcontenido">

<div class="subtitulos">Un total de <?php echo $total;?> Productos en <?php echo $cat1->getNombre();?><div class="subtitulos_menu">
<!--  <form id="form_boton" ><input type="button" value="Adicionar Nuevo Equipo" id="nuevo" /></form> -->
</div>
</div>
<div class="subcontenido2">

<table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
</table>
	  <div id="buscador">
      <div class="rowElem"></div>
      </div>
      <div class="enunciados"></div>
      <form runat="server" action="../php/action/productoDelVarios.php" id="overall" method="get">
      <table width="100%" class="yui" id="tableOne">
        <thead>
          <tr>
              <td colspan="2">
                <a href="menuAdmin.php?s=productosAdd&idCat1=<?php echo $idCat1;?>">
                    Añadir
                </a>
                  |
                <a href="menuAdmin.php?s=productosImportar&idCat1=<?php echo $idCat1;?>">
                    Importar Cantidades
                </a>
            </td>
            <td colspan="3" class="filter"><span class="rowElem">
             Buscar:
              <input id="filterBoxOne" value="" maxlength="30" size="30" type="text" onkeypress="return checkEnter(event)"/>
             </span>
            </td>
          </tr>
          <?php if( isset($_GET['ok']) ){ ?>
            <tr>
                <td class="EstiloGreen" colspan="5">
                    <?php if($_GET['id'] != 0){ ?>
                    Producto añadido.

                    <br />
                    Haga click
                    <a href="menuAdmin.php?idCat1=<?php echo $idCat1?>&id=<?php echo $_GET['id'];?>&s=productosEdit#relacionados">
                        <b>AQUI</b>
                    </a>
                    para añadir productos relacionados a el.
                    <?php }else echo 'error al crear el producto'; ?>
                </td>
            </tr>
          <?php }?>
          <?php if( isset($_GET['del']) ){ ?>
            <tr>
                <td class="EstiloGreen" colspan="5">Producto Eliminado</td>
            </tr>
          <?php }?>
          <tr>
            <th><a href='#' title="Click para ordenar">Nombre (ref)</a></th>
            <th><a href='#' title="Click para ordenar">Sub-Categoría</a></th>
            <th><a href='#' title="Click para ordenar">Precio</a></th>
            <th><a href='#' title="Click para ordenar">Stock</a></th>
            <th><a href='#' title="Click para ordenar">Accion</a></th>
          </tr>
        </thead>
        <tbody>
        <?php
        foreach ($lista as $item) {
        $dir = './../php/action/productoDel.php?id='.$item->getId().'&idCat1='.$idCat1;
        ?>
          <tr>
            <td>
                <?php echo $item->getNombre();?> (<?php echo $item->getRef();?>)
                <br />
                <?php if($item->getNovedad()) echo '<img src="../imagenes/nov.png" atl="" />'; ?>
                <?php if($item->getRegalar()) echo '<img src="../imagenes/reg.png" atl="" />'; ?>
                <?php if($item->getVendido()) echo '<img src="../imagenes/ven.png" atl="" />'; ?>
                <?php if($item->getRecomendado()) echo '<img src="../imagenes/rec.png" atl="" />'; ?>
                <?php if($item->getPopular()) echo '<img src="../imagenes/pop.png" atl="" />'; ?>
                <?php if($item->getPromocion()) echo '<img src="../imagenes/promo.png" atl="" />'; ?>
            </td>
            <td>
                <?php
                    $cat2 = $cat2DAO->getById($item->getIdCat2());
                    if($cat2 != null)
                        echo $cat2->getNombre();
                    else
                        echo '<span style="color:red">No encontrada</span>';
                ?>
                <?php
                    for($i=1;$i<=5;$i++){
                        if($item->{"getIdCat1".$i}() != 0 ){
                            $cat2 = $cat2DAO->getById($item->{"getIdCat1".$i}());
                            if($cat2 != null)
                                echo '<br />'.$cat2->getNombre();
                        }
                    }
                ?>
            </td>
            <td>
                <?php echo $item->getPrecioFormato(); ?>
            </td>
            <td>
                <?php echo $item->getStock(); ?>
            </td>
            <td>
                <a href="javascript:confirmar('<?php echo $dir; ?>',' subcategoria (<?php echo $item->getNombre();?>)')" title="Eliminar">Eliminar</a>
                |
                <a href="menuAdmin.php?s=productosEdit&id=<?php echo $item->getId(); ?>&idCat1=<?php echo $idCat1;?>">Editar</a>
                <br />
                <input type="checkbox" name="prod-<?php echo $item->getId(); ?>" value="<?php echo $item->getId(); ?>"><br />
            </td>
          </tr>
      <?php } ?>
        </tbody>
<!--
        <tfoot>
          <tr id="pagerOne">
            <td colspan="5"><img src="jquery/jQueryTableSorterConPaging/_assets/img/first.png" class="first"/> <img src="jquery/jQueryTableSorterConPaging/_assets/img/prev.png" class="prev"/>
                <input type="text" class="pagedisplay"/>
                <img src="jquery/jQueryTableSorterConPaging/_assets/img/next.png" class="next"/> <img src="jquery/jQueryTableSorterConPaging/_assets/img/last.png" class="last"/>
                <select name="select" class="pagesize">
                  <option selected="selected"  value="10">10</option>
                  <option value="20">20</option>
                  <option value="30">30</option>
                  <option  value="40">40</option>
                </select>            </td>
          </tr>
        </tfoot>
-->
      </table>
          <input type="hidden" name="idCat1" value="<?php echo $_GET['idCat1'];?>">
          <input type="hidden" name="s" value="productos">
        <div style="text-align: right">
            <input type="button" value="Seleccionar todos" onclick="checkedAll(); return false;">
            <input type="submit" value="Eliminar seleccionados" onclick="return confirmarForm2(overall, 'Esta seguro de que desea eliminar los elementos seleccionados?');">
        </div>
    </form>
      <br />
      <div style="font-size: x-small;">
          <img src="../imagenes/nov.png" alt="" /> = Novedad <br />
          <img src="../imagenes/pop.png" alt="" /> = Más Popular <br />
          <img src="../imagenes/rec.png" alt="" /> = Recomendado <br />
          <img src="../imagenes/reg.png" alt="" /> = Para Regalar <br />
          <img src="../imagenes/ven.png" alt="" /> = Más Vendido <br />
          <img src="../imagenes/promo.png" alt="" /> = Promoción <br />
      </div>
    </div>
  </div>
  <!-- FIN SUBCONTENIDO -->
  <!-- FIN SUBCONTENIDO -->
</div>
<!-- FIN CONTENIDO FONDO -->
<div class="contenido_marco_inf"></div>
<script>
    function checkEnter(e){
     e = e || event;
     return (e.keyCode || event.which || event.charCode || 0) !== 13;
    }

    checked = false;
      function checkedAll () {
        if (checked == false){checked = true}else{checked = false}
        for (var i = 0; i < document.getElementById('overall').elements.length; i++) {
          document.getElementById('overall').elements[i].checked = checked;
        }
      }
</script>