<?php
ini_set('memory_limit', '-1');
if( !isset($_SESSION['admin']) ){
    header("location: ./index.php");
    exit;
}

foreach (glob("../php/dao/*.php") as $filename){
    include $filename;
}

foreach (glob("../php/entities/*.php") as $filename){
    include $filename;
}

foreach (glob("../php/functions/*.php") as $filename){
    include $filename;
}
?>
