<?php
if( !isset($_SESSION['admin']) ){
    header("location: ./index.php");
    exit;
}

$productoDAO = new productoDAO;

$id = $_GET['id'];

$orderDAO = new orderDAO;
$pedido = $orderDAO->getById($id);

$orderLineDAO = new orderLinesDAO;
$lines = $orderLineDAO->getsByPedido("id", "asc", $pedido->getId());

?>
                    <div class="titulos">
                      <div class="titulos_texto1">Pedidos<div class="cerrar"><a href="../php/action/logout.php"><img src="imagenes/contenido/cerrar.png" alt="Cerrar Sesi&oacute;n" border="0" /></a></div></div>
                      <div class="titulos_texto2">
                      </div>
                    </div>
                    <!-- FIN TITULOS -->
                    <div class="contenido_marco_sup"></div>
                    <div class="contenido_fondo">
                      <div class="subcontenido">

<div class="subtitulos">
 <table width="700">
    <tr>
        <td>
            <a href="menuAdmin.php?s=pedidos">
            Pedidos
            </a>
            >
            <a href="menuAdmin.php?s=pedidosVer&id=<?php echo $id;?>">Ver
            </a>
            >
            <b>
            Editar Codigo - Estado
            </b>
        </td>
    </tr>
 </table>
<div class="subtitulos_menu">
<!--  <form id="form_boton" ><input type="button" value="Adicionar Nuevo Equipo" id="nuevo" /></form> -->
</div>
</div>
<div class="subcontenido2">
<table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
</table>
	  <div id="buscador">
      <div class="rowElem"></div>
      </div>
      <div class="enunciados"></div>
      <form method="post" action="../php/action/pedidoEdit.php">
      <table border="0" align="l" width="600" border="0" cellspacing="4" cellpadding="4">
          <tr>
            <td colspan="2"><b><font style="font-size:large;color:#154894">Información general del Pedido</font></b></td>
          </tr>
          <tr>
            <td  width="150">Código:</td>
            <td><input type="text" name="code" value="<?php echo $pedido->getCode();?>"></td>
          </tr>
          <tr>
            <td>Estado:</td>
            <td><input type="text" name="state" value="<?php echo $pedido->getState();?>"></td>
          </tr>
          <tr>
            <td></td>
            <td>
                <input type="submit" value="Cambiar">
                <input type="hidden" name="id" value="<?php echo $pedido->getId();?>">
            </td>
          </tr>
      </table>
      </form>
    </div>
  </div>
  <!-- FIN SUBCONTENIDO -->
  <!-- FIN SUBCONTENIDO -->
</div>
<!-- FIN CONTENIDO FONDO -->
<div class="contenido_marco_inf"></div>
