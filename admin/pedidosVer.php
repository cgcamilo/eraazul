<?php
if( !isset($_SESSION['admin']) ){
    header("location: ./index.php");
    exit;
}

$productoDAO = new productoDAO;

$id = $_GET['id'];

$orderDAO = new orderDAO;
$pedido = $orderDAO->getById($id);


$orderLineDAO = new orderLinesDAO;
$lines = $orderLineDAO->getsByPedido("id", "asc", $pedido->getId());

$userDAO = new userDAO;
$userOrder = $userDAO->getById($pedido->getUserId());
if($userOrder == null){
    $userOrder = new user;
    $userOrder->setNombre('Usuario no encontrado');
}


?>
                    <div class="titulos">
                      <div class="titulos_texto1">Pedidos<div class="cerrar"><a href="../php/action/logout.php"><img src="imagenes/contenido/cerrar.png" alt="Cerrar Sesi&oacute;n" border="0" /></a></div></div>
                      <div class="titulos_texto2">
                      </div>
                    </div>
                    <!-- FIN TITULOS -->
                    <div class="contenido_marco_sup"></div>
                    <div class="contenido_fondo">
                      <div class="subcontenido">

<div class="subtitulos">
 <table width="700">
    <tr>
        <td>
            <a href="menuAdmin.php?s=pedidos">
            Pedidos
            </a>
            >
            <b>Ver</b>
            >
            <a href="menuAdmin.php?s=pedisosEdit&id=<?php echo $id;?>">
            Editar Codigo - Estado
            </a>
        </td>
    </tr>
 </table>
<div class="subtitulos_menu">
<!--  <form id="form_boton" ><input type="button" value="Adicionar Nuevo Equipo" id="nuevo" /></form> -->
</div>
</div>
<div class="subcontenido2">
<table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
</table>
	  <div id="buscador">
      <div class="rowElem"></div>
      </div>
      <div class="enunciados"></div>
      <table border="0" align="l" width="600" border="0" cellspacing="4" cellpadding="4">
          <tr>
            <td colspan="2"><b><font style="font-size:large;color:#154894">Información general del Pedido</font></b></td>
          </tr>
          <tr>
            <td  width="150">Código:</td>
            <td><?php echo $pedido->getCode();?></td>
          </tr>
          <tr>
            <td  width="150">Fecha:</td>
            <td><?php echo $pedido->getDate();?></td>
          </tr>
          <tr>
            <td>Estado:</td>
            <td><b><?php echo $pedido->getState();?></b></td>
          </tr>
          <tr>
            <td>Método:</td>
            <td>
                <?php echo $pedido->getMethod();?>
            </td>
          </tr>
          <tr>
            <td>SubTotal:</td>
            <td>
                $<?php echo number_format($pedido->getSubTotal(),  2, ',', '.');?>
            </td>
          </tr>
          <tr>
            <td>Envio:</td>
            <td>
                $<?php echo number_format($pedido->getShipping(),  2, ',', '.');?>
            </td>
          </tr>
          <tr>
            <td>Total:</td>
            <td>
                $<b><?php echo number_format($pedido->getTotal(),  2, ',', '.');?></b>
            </td>
          </tr>
          <tr>
            <td>Usuario</td>
            <td>
                <a href="menuAdmin.php?s=usuariosVer&id=<?php echo $userOrder->getId();?>">
                <?php echo $userOrder->getLogin();?>
                </a>
            </td>
          </tr>
          <tr>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td colspan="2"><b><font style="font-size:large;color:#154894">Datos de Facturación</font></b></td>
          </tr>
          <tr>
            <td>Nombre:</td>
            <td>
                <?php echo $pedido->getBName();?>
            </td>
          </tr>
          <tr>
            <td>Dirección:</td>
            <td>
                <?php echo $pedido->getBAddress();?>
            </td>
          </tr>
          <tr>
            <td>Ciudad:</td>
            <td>
                <?php echo $pedido->getBCity();?>
            </td>
          </tr>
          <tr>
            <td>Barrio:</td>
            <td>
                <?php echo $pedido->getBState();?>
            </td>
          </tr>
          <tr>
            <td>Telefono:</td>
            <td>
                <?php echo $pedido->getBPhone();?>
            </td>
          </tr>
          <tr>
            <td>Fax:</td>
            <td>
                <?php echo $pedido->getBZip();?>
            </td>
          </tr>
          <tr>
            <td colspan="2"><b><font style="font-size:large;color:#154894">Datos de Envío</font></b></td>
          </tr>
          <tr>
            <td>Dirección:</td>
            <td>
                <?php echo $pedido->getSAddress();?>
            </td>
          </tr>
          <tr>
            <td>Ciudad:</td>
            <td>
                <?php echo $pedido->getSCity();?>
            </td>
          </tr>
          <tr>
            <td>Barrio:</td>
            <td>
                <?php echo $pedido->getSState();?>
            </td>
          </tr>
          <tr>
            <td>Teléfono:</td>
            <td>
                <?php echo $pedido->getSPhone();?>
            </td>
          </tr>
          <tr>
            <td colspan="2"><b><font style="font-size:large;color:#154894">Productos</font></b></td>
          </tr>
          <tr>
            <td colspan="2">

            <table>

                <tr>
                    <td bgcolor="#facfd2" width="100">Img</td>
                    <td bgcolor="#facfd2" width="70">Ref.</td>
                    <td bgcolor="#facfd2" width="200">Nombre</td>
                    <td bgcolor="#facfd2">Cantidad</td>
                </tr>
                <?php
                    foreach ($lines as $line){
                        $producto = $productoDAO->getById($line->getIdProduct());
                        if($producto != null){
                ?>
                <tr>
                    <td><img alt="" src="../imagenes/productos/<?php echo $producto->getImg();?>" width="60" /></td>
                    <td><?php echo $producto->getRef();?></td>
                    <td><?php echo $producto->getNombre();?></td>
                    <td align="center"><?php echo $line->getQuantity();?></td>
                </tr>
                <?php }else{ ?>
                <tr>
                    <td colspan="2">No se encontró el producto</td>
                    <td align="center"><?php echo $line->getQuantity();?></td>
                    <td align="center"><?php echo strtoupper($line->getSize());?></td>
                </tr>
                <?php }} ?>
            </table>


            </td>
          </tr>
      </table>
    </div>
  </div>
  <!-- FIN SUBCONTENIDO -->
  <!-- FIN SUBCONTENIDO -->
</div>
<!-- FIN CONTENIDO FONDO -->
<div class="contenido_marco_inf"></div>
