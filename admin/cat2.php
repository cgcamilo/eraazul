<?php

if( !isset($_SESSION['admin']) ){
    header("location: ./index.php");
    exit;
}


$idCat1 = $_GET['idCat1'];

$productoDAO = new productoDAO();

$cat1DAO = new cat1DAO();
$cat1 = $cat1DAO->getById($idCat1);

$cat2DAO = new cat2DAO();
$lista = $cat2DAO->getsByIdCat1("id", "asc", $idCat1);
$total = $cat2DAO->total(2, "idCat1", $idCat1);
?>
                    <div class="titulos">
                        <div class="titulos_texto1">Subcategorías de <b><?php echo $cat1->getNombre();?></b><div class="cerrar"><a href="../php/action/logout.php"><img src="imagenes/contenido/cerrar.png" alt="Cerrar Sesi&oacute;n" border="0" /></a></div></div>
                      <div class="titulos_texto2">
                      </div>
                    </div>
                    <!-- FIN TITULOS -->
                    <div class="contenido_marco_sup"></div>
                    <div class="contenido_fondo">
                      <div class="subcontenido">

<div class="subtitulos">Un total de <?php echo $total;?> subcategorias de <?php echo $cat1->getNombre();?><div class="subtitulos_menu">
<!--  <form id="form_boton" ><input type="button" value="Adicionar Nuevo Equipo" id="nuevo" /></form> -->
</div>
</div>
<div class="subcontenido2">

<table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
</table>
	  <div id="buscador">
      <div class="rowElem"></div>
      </div>
      <div class="enunciados"></div>
      <form action="../php/action/cat2Add.php" method="post">
         <table  id="tabletwo" style="font-family:arial;border-collapse:collapse;border: 1px solid #BFBFBF;font-size:8pt;text-align: left;">
              <tr>
                <td>
                    Nueva:<input type="text" size="30" name="nombre" />
                    <input type="hidden" name="idCat1" value="<?php echo $idCat1;?>" >
                    <input type="submit" value="Añadir">
                    <?php if( isset($_GET['error1']) ){ ?>
                    <br /><span style="color: red">Introduce el nombre</span>
                  <?php }?>
                </td>
              </tr>
          </table>
      </form>
      <br />
      <form runat="server">
      <table width="100%" class="yui" id="tableOne">
        <thead>
          <tr>
            <td>

            </td>
            <td colspan="2" class="filter"><span class="rowElem">
              Buscar:
              <input id="filterBoxOne" value="" maxlength="30" size="30" type="text" />
              </span>
            </td>
          </tr>
          <?php if( isset($_GET['ok']) ){ ?>
            <tr>
                <td class="EstiloGreen" colspan="3">Subcategoria añadida</td>
            </tr>
          <?php }?>
          <?php if( isset($_GET['del']) ){ ?>
            <tr>
                <td class="EstiloGreen" colspan="3">Subcategoria Eliminada</td>
            </tr>
          <?php }?>
          <tr>
            <th><a href='#' title="Click para ordenar">Nombre</a></th>
            <th><a href='#' title="Click para ordenar">Número de Productos</a></th>
            <th><a href='#' title="Click para ordenar">Accion</a></th>
          </tr>
        </thead>
        <tbody>
        <?php
        foreach ($lista as $item) {
        $dir = './../php/action/cat2Del.php?id='.$item->getId().'&idCat1='.$idCat1;
        ?>
          <tr>
            <td>
                <?php echo $item->getNombre();?>
            </td>
            <td>
                <?php
                    echo $productoDAO->total(2, "idCat2", $item->getId());
                ?>
            </td>
            <td>
                <a href="javascript:confirmar('<?php echo $dir; ?>',' subcategoria (<?php echo $item->getNombre();?>)')" title="Eliminar">Eliminar</a>
                |
                <a href="menuAdmin.php?s=cat2Edit&id=<?php echo $item->getId(); ?>&idCat1=<?php echo $idCat1;?>">Editar</a>
            </td>
          </tr>
      <?php } ?>
        </tbody>
<!--
        <tfoot>
          <tr id="pagerOne">
            <td colspan="5"><img src="jquery/jQueryTableSorterConPaging/_assets/img/first.png" class="first"/> <img src="jquery/jQueryTableSorterConPaging/_assets/img/prev.png" class="prev"/>
                <input type="text" class="pagedisplay"/>
                <img src="jquery/jQueryTableSorterConPaging/_assets/img/next.png" class="next"/> <img src="jquery/jQueryTableSorterConPaging/_assets/img/last.png" class="last"/>
                <select name="select" class="pagesize">
                  <option selected="selected"  value="10">10</option>
                  <option value="20">20</option>
                  <option value="30">30</option>
                  <option  value="40">40</option>
                </select>            </td>
          </tr>
        </tfoot>
-->
      </table>
    </form>

    </div>
  </div>
  <!-- FIN SUBCONTENIDO -->
  <!-- FIN SUBCONTENIDO -->
</div>
<!-- FIN CONTENIDO FONDO -->
<div class="contenido_marco_inf"></div>
