<?php
if( !isset($_SESSION['admin']) ){
    header("location: ./index.php");
    exit;
}

include("fckeditor/fckeditor.php") ;

$idCat1 = $_GET['idCat1'];


$cat1DAO = new cat1DAO();
$cat1 = $cat1DAO->getById($idCat1);

$cat2DAO = new cat2DAO();
$cat2s = $cat2DAO->getsByIdCat1("nombre", "asc", $idCat1);


?>
                    <div class="titulos">
                      <div class="titulos_texto1">Productos de <b><?php echo $cat1->getNombre();?></b><div class="cerrar"><a href="../php/action/logout.php"><img src="imagenes/contenido/cerrar.png" alt="Cerrar Sesi&oacute;n" border="0" /></a></div></div>
                      <div class="titulos_texto2">
                      </div>
                    </div>
                    <!-- FIN TITULOS -->
                    <div class="contenido_marco_sup"></div>
                    <div class="contenido_fondo">
                      <div class="subcontenido">

<div class="subtitulos">
 <table width="700">
    <tr>
        <td>
            <a href="menuAdmin.php?s=productos&idCat1=<?php echo $idCat1;?>">
            Productos
            </a>
            >
            <b>Nuevo</b>
        </td>
    </tr>
 </table>
<div class="subtitulos_menu">
<!--  <form id="form_boton" ><input type="button" value="Adicionar Nuevo Equipo" id="nuevo" /></form> -->
</div>
</div>
<div class="subcontenido2">
<table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
</table>
	  <div id="buscador">
      <div class="rowElem"></div>
      </div>
      <div class="enunciados"></div>
      <form action="../php/action/productoAdd.php" method="post" enctype="multipart/form-data">
      <table border="0" align="l" width="600" border="0" cellspacing="4" cellpadding="4">
         <?php if( isset($_GET['error1']) ){?>
           <tr>
             <td colspan="4" class="EstiloRed">Tienes que introducir todos los datos</td>
             </tr>
        <?php }if( isset($_GET['error2']) ){?>
            <tr>
             <td colspan="4" class="EstiloRed">Tienes que introducir la 'imagen'</td>
             </tr>
        <?php }if( isset($_GET['error3']) ){?>
            <tr>
             <td colspan="4" class="EstiloRed">Precio y Stock deben ser caracteres numéricos</td>
             </tr>
        <?php }if( isset($_GET['ok']) ){?>
            <tr>
             <td colspan="4" class="EstiloGreen">Datos almacenados</td>
             </tr>
        <?php }?>
          <tr>
            <td colspan="2"><b><font style="font-size:large;color:#154894">Información Básica</font></b></td>
          </tr>
          <tr bgcolor="#efefef">
            <td>
                Nombre:<font style="font-size:x-small;color:red">*</font>
            </td>
            <td>
                <input size="35" name="nombre" value="<?php echo $_GET['nombre'];?>">
            </td>
          </tr>
          <tr bgcolor="#efefef">
            <td>
                Referencia:<font style="font-size:x-small;color:red">* ÚNICA PARA CADA PEDIDO</font>
            </td>
            <td>
                <input size="35" name="ref" value="<?php echo $_GET['ref'];?>">
            </td>
          </tr>
          <tr>
            <td>
                Tiene IVA:<font style="font-size:x-small;color:red">*</font>
            </td>
            <td>
                <input size="35" name="iva" type="checkbox" >
            </td>
          </tr>
          <tr>
            <td>
                Precio:<font style="font-size:x-small;color:red">* no poner simbolo de miles!</font>
            </td>
            <td>
                <input size="35" name="precio" value="<?php echo $_GET['precio'];?>">
            </td>
          </tr>
          <tr>
            <td>
                Descuento:<font style="font-size:x-small;color:red">*</font>
            </td>
            <td>
                <input size="35" name="descuento" type="checkbox" >
            </td>
          </tr>
          <tr>
            <td>
                Precio Si hay descuento:
                <br /><font style="font-size:x-small;color:red">* no poner simbolo de miles!</font>
                <br /><font style="font-size:x-small;color:red">* este sería el precio que quedaría tachado</font>
            </td>
            <td>
                <input size="35" name="precioDescuento" value="<?php echo $_GET['precioDescuento'];?>">
            </td>
          </tr>
          <tr bgcolor="#efefef">
            <td>
                Stock:<font style="font-size:x-small;color:red">* número de productos disponibless</font>
            </td>
            <td>
                <input size="35" name="stock" value="<?php echo $_GET['stock'];?>">
            </td>
          </tr>
          <tr>
            <td>
                Sub-Categoría:<font style="font-size:x-small;color:red">*</font>
            </td>
            <td>
                <select name="idCat2">
                    <?php foreach($cat2s as $cat2){ ?>
                    <option value="<?php echo $cat2->getId();?>" <?php if( $cat2->getId() == $_GET['idCat2'] ) echo 'selected';?>>
                        <?php echo $cat2->getNombre();?>
                    </option>
                    <?php } ?>
                </select>
            </td>
          </tr>
          <?php for($i=1;$i<=5; $i++){ ?>
          <tr>
            <td>
                Sub-Categoría adicional (<?php echo $i;?>):
            </td>
            <td>
                <select name="idCat1<?php echo $i;?>">
                    <option value="0">No</option>
                    <?php foreach($cat2s as $cat2){ ?>
                    <option value="<?php echo $cat2->getId();?>" <?php if( $cat2->getId() == $_GET['idCat1'.$i] ) echo 'selected';?>>
                        <?php echo $cat2->getNombre();?>
                    </option>
                    <?php } ?>
                </select>
            </td>
          </tr>
          <?php } ?>
          <tr bgcolor="#efefef">
              <td valign="top">Datos Generales:</td>
              <td>
                  <?php
                $oFCKeditor_es = new FCKeditor('datos') ;
                $oFCKeditor_es->BasePath = 'fckeditor/';
                $oFCKeditor_es->Width  = '350' ;
                $oFCKeditor_es->Height = '210' ;
                $oFCKeditor_es->Value = $_GET['datos'];
                $oFCKeditor_es->ToolbarSet = 'CamiloCifuentes';
                $oFCKeditor_es->Create() ;
                ?>
              </td>
          </tr>
          <tr>
              <td valign="top">Datos Específicos:</td>
              <td>
                  <textarea name="datosTecnicos" rows="3" cols="30"><?php echo $_GET['datosTecnicos'];?></textarea>
              </td>
          </tr>
          <tr bgcolor="#efefef">
            <td valign="top">
                Descripción:
            </td>
            <td>
                <?php
                $oFCKeditor_es = new FCKeditor('corta') ;
                $oFCKeditor_es->BasePath = 'fckeditor/';
                $oFCKeditor_es->Width  = '350' ;
                $oFCKeditor_es->Height = '210' ;
                $oFCKeditor_es->Value = $_GET['corta'];
                $oFCKeditor_es->ToolbarSet = 'CamiloCifuentes';
                $oFCKeditor_es->Create() ;
                ?>
            </td>
          </tr>
          <tr>
            <td colspan="2"><b><font style="font-size:large;color:#154894">Destacar en</font></b></td>
          </tr>
          <tr bgcolor="#efefef">
              <td>
                  <input type="checkbox" name="novedad" <?php if($_GET['novedad'] == "1") echo 'checked'; ?>  /> Novedad <br />
                  <input type="checkbox" name="regalar" <?php if($_GET['regalar'] == "1") echo 'checked'; ?> /> Para Regalar <br />
                  <input type="checkbox" name="vendido" <?php if($_GET['vendido'] == "1") echo 'checked'; ?> /> Más Vendido <br />
              </td>
              <td>
                  <input type="checkbox" name="recomendado" <?php if($_GET['recomendado'] == "1") echo 'checked'; ?> /> Recomendado <br />
                  <input type="checkbox" name="popular" <?php if($_GET['popular'] == "1") echo 'checked'; ?> /> Más Popular <br />
                  <input type="checkbox" name="promocion" <?php if($_GET['promocion'] == "1") echo 'checked'; ?> /> Enn Promoción <br />
              </td>
          </tr>
          <tr>
            <td colspan="2"><b><font style="font-size:large;color:#154894">Imágenes</font></b></td>
          </tr>
          <tr bgcolor="#efefef">
              <td>
                  Imagen Principal
                  <br />
                  <span style="color: red;font-size: x-small">* Tamaño: 113 x 80px!!</span>
              </td>
              <td>
                  <input type="file" name="img">
              </td>
          </tr>
          <?php for($i = 1; $i <= 5 ; $i++){ ?>
          <tr>
              <td>
                  Imagen <?php echo $i;?>
              </td>
              <td>
                  <input type="file" name="img<?php echo $i;?>">
              </td>
          </tr>
          <?php  } ?>
          <tr>
            <td></td>
            <td></td>
          </tr>
          <tr>
              <td align="right" colspan="2">
                <input type="hidden" name="idCat1" value="<?php echo $idCat1;?>">
                <input type="submit" value="Guardar">
            </td>
          </tr>
          <tr>
            <td colspan="2">
                <font style="font-size:x-small;color:red">
                    * campos obligatorios
                </font>
            </td>
          </tr>
      </table>
      </form>
    </div>
  </div>
  <!-- FIN SUBCONTENIDO -->
  <!-- FIN SUBCONTENIDO -->
</div>
<!-- FIN CONTENIDO FONDO -->
<div class="contenido_marco_inf"></div>
