function load(x, y) {
      if (GBrowserIsCompatible()) {
        var map = new GMap2(document.getElementById("map"));
        map.addControl(new GSmallMapControl());
        map.addControl(new GMapTypeControl());
        var center = new GLatLng(x, y);
        map.setCenter(center, 5);
        geocoder = new GClientGeocoder();
        var marker = new GMarker(center, {draggable: true});
        map.addOverlay(marker);

        document.getElementById("lat").innerHTML = center.lat().toFixed(5);
        document.getElementById("lng").innerHTML = center.lng().toFixed(5);

        document.getElementById("cX").value = center.lat().toFixed(5);
        document.getElementById("cY").value = center.lng().toFixed(5);

	  GEvent.addListener(marker, "dragend", function() {
       var point = marker.getPoint();
	      map.panTo(point);
       document.getElementById("lat").innerHTML = point.lat().toFixed(5);
       document.getElementById("lng").innerHTML = point.lng().toFixed(5);

       document.getElementById("cX").value = point.lat().toFixed(5);
       document.getElementById("cY").value = point.lng().toFixed(5);

        });


	 GEvent.addListener(map, "moveend", function() {
		  map.clearOverlays();
                  var center = map.getCenter();
		  var marker = new GMarker(center, {draggable: true});
		  map.addOverlay(marker);
		  document.getElementById("lat").innerHTML = center.lat().toFixed(5);
                  document.getElementById("lng").innerHTML = center.lng().toFixed(5);

                  document.getElementById("cX").value = center.lat().toFixed(5);
                  document.getElementById("cY").value = center.lng().toFixed(5);


	 GEvent.addListener(marker, "dragend", function() {
      var point =marker.getPoint();
	     map.panTo(point);
         document.getElementById("lat").innerHTML = point.lat().toFixed(5);
	     document.getElementById("lng").innerHTML = point.lng().toFixed(5);

         document.getElementById("cX").value = point.lat().toFixed(5);
         document.getElementById("cY").value = point.lng().toFixed(5);

        });

        });

      }
    }

function showAddress(address) {
 
   var map = new GMap2(document.getElementById("map"));
   map.addControl(new GSmallMapControl());
   map.addControl(new GMapTypeControl());
   if (geocoder) {
        geocoder.getLatLng(
            address,
            function(point) {
                if (!point) {
                    alert(address + " not found");
                    } else {
                    document.getElementById("lat").innerHTML = point.lat().toFixed(5);
                    document.getElementById("lng").innerHTML = point.lng().toFixed(5);

                    document.getElementById("cX").value = point.lat().toFixed(5);
                    document.getElementById("cY").value = point.lng().toFixed(5);

                    map.clearOverlays()
                    map.setCenter(point, 14);
                    var marker = new GMarker(point, {draggable: true});
                    map.addOverlay(marker);

                    GEvent.addListener(marker, "dragend", function() {
                        var pt = marker.getPoint();
                        map.panTo(pt);
                        document.getElementById("lat").innerHTML = pt.lat().toFixed(5);
                        document.getElementById("lng").innerHTML = pt.lng().toFixed(5);

                        document.getElementById("cX").value = pt.lat().toFixed(5);
                        document.getElementById("cY").value = pt.lng().toFixed(5);
                    });


                    GEvent.addListener(map, "moveend", function() {
                    map.clearOverlays();
                    var center = map.getCenter();
                      var marker = new GMarker(center, {draggable: true});
                      map.addOverlay(marker);
                      document.getElementById("lat").innerHTML = center.lat().toFixed(5);
                      document.getElementById("lng").innerHTML = center.lng().toFixed(5);

                      document.getElementById("cX").value = center.lat().toFixed(5);
                      document.getElementById("cY").value = center.lng().toFixed(5);

                        GEvent.addListener(marker, "dragend", function() {
                            var pt = marker.getPoint();
                            map.panTo(pt);
                            document.getElementById("lat").innerHTML = pt.lat().toFixed(5);
                            document.getElementById("lng").innerHTML = pt.lng().toFixed(5);

                            document.getElementById("cX").value = pt.lat().toFixed(5);
                            document.getElementById("cY").value = pt.lng().toFixed(5);
                        });

                    });
            }
          }
        );
      }
}

function handleKeyPress(event){
    var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
    if (keyCode==13){
        showAddress(document.productos.elements['address'].value);
        return false;
    }
    else
        return true;
}