<?php
$_SESSION['admin']="sadsadsa";

ini_set('memory_limit', '-1');

include 'includes.php';


$thisUser = unserialize($_SESSION['admin']);



$orderDAO = new orderDAO;
$orderLineDAO = new orderLinesDAO();
$pedidos = $orderDAO->gets("id", "asc");
$total = $orderDAO->total();
$totalAmount = $orderDAO->totalAmount();

$clientID = ( isset($_GET['filtroUser']) ) ? $_GET['filtroUser'] : 0;
$desde = ( isset($_GET['desde']) && $_GET['desde'] != "" ) ? $_GET['desde'] : "01-01-1980";
$hasta = ( isset($_GET['hasta']) && $_GET['hasta'] != "" ) ? $_GET['hasta'] : "01-01-2030";

$totalAmountUser = $orderDAO->totalAmountUser($clientID);

$userDAO = new userDAO();
$productDAO = new productoDAO();
$cat1DAO = new cat1DAO();
$cat2DAO = new cat2DAO();

$users = $userDAO->getUsers("nombre", "asc", 0, 9999);
$cats1 = $cat1DAO->gets("nombre", "asc", 0, 9999);
$cats2 = $cat2DAO->gets("nombre", "asc", 0, 9999);



if( isset($_GET['filter']) && $_GET['filter'] != 0 ){

        for($i = count($pedidos) - 1; $i >= 0 ; $i--){
            if( $_GET['filtroUser'] != "" && $_GET['filtroUser'] != 0 ){
                $user = $userDAO->getById($pedidos[$i]->getUserId());
                if( $user->getId() != $_GET['filtroUser'] ){
                    unset($pedidos[$i]);
                    continue;
                }

            }

            if( $_GET['filtroCat1']  != "" && $_GET['filtroCat1']  != 0 ){

            $lines = $orderLineDAO->getsByPedido("id", "asc", $pedidos[$i]->getId());
            $eliminar = true;
            foreach($lines as $productLine){
                $p = $productDAO->getById($productLine->getIdProduct());
                if( $p == null ){
                    $p = new producto ();
                    $p->setIdCat1(0);
                }
                $cat1 = $cat1DAO->getById($p->getIdCat1());
                if( $cat1 == null ){
                    $cat1 = new cat1();
                }

                //filtro cat
                if( $cat1->getId() == $_GET['filtroCat1'] ){
                    $eliminar = false;
                }

                //filtrar por subcat2
                if( $_GET['filtroCat2']  != "" && $_GET['filtroCat2']  != 0 ){
                    $cat2 = $cat2DAO->getById($p->getIdCat2());
                    if( $cat2 == null ){
                        $cat2 = new cat2();
                    }

                    if( $cat2->getId() != $_GET['filtroCat2'] ){
                        $eliminar = true;
                    }
                    else
                        break;
                }
            }

            if( $eliminar )
                unset($pedidos[$i]);
        }
    }
}


$totalSeen = 0;
foreach ($pedidos as $pedido) {
    //filtro fechas
    if( isset($_GET['filter']) && $_GET['filter'] == 1 ){

        $order_date = strtotime(str_replace("/", "-", $pedido->getDate()));
        $init_date = strtotime(str_replace("/", "-", $desde));
        $end_date = strtotime(str_replace("/", "-", $hasta));
        if( $order_date > $end_date || $order_date < $init_date )
            continue;
    }
    $totalSeen += $pedido->getTotal();
}

header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=reporte.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
        <table>
          <tr>
            <th>Cliente</th>
            <th>Fecha</th>
            <th>Productos</th>
            <th>Categoria</th>
            <th>Subcategoria</th>
            <th>Valor</th>
          </tr>
        <tbody>
        <?php
        foreach ($pedidos as $pedido) {
            $user = $userDAO->getById($pedido->getUserId());
            if( $user == null ){
                $user = new user();
                $user->setNombre("No disponible");
                $user->setId(0);
            }

            //filtro fechas
            if( isset($_GET['filter']) && $_GET['filter'] == 1 ){

                $order_date = strtotime(str_replace("/", "-", $pedido->getDate()));
                $init_date = strtotime(str_replace("/", "-", $desde));
                $end_date = strtotime(str_replace("/", "-", $hasta));
                if( $order_date > $end_date || $order_date < $init_date )
                    continue;
            }

            $lines = $orderLineDAO->getsByPedido("id", "asc", $pedido->getId());
        ?>
          <tr>
            <td valign="top">
                <?php echo $user->getNombre();?>
            </td>
            <td valign="top"><?php echo $pedido->getDate();?></td>
            <td valign="top">
                <?php
                foreach($lines as $productLine){
                    $p = $productDAO->getById($productLine->getIdProduct());
                    if( $p == null ){
                        $p = new producto ();
                        $p->setNombre("No disponible");
                    }

                    echo ''.$productLine->getQuantity().' x '.$p->getNombre().'<br />';
                }
                ?>
            </td>
            <td valign="top">
                <?php
                foreach($lines as $productLine){
                    $p = $productDAO->getById($productLine->getIdProduct());
                    if( $p == null ){
                        $p = new producto ();
                        $p->setNombre("No disponible");
                        $p->setIdCat1(0);
                    }
                    $cat1 = $cat1DAO->getById($p->getIdCat1());
                    if( $cat1 == null ){
                        $cat1 = new cat1();
                        $cat1->setNombre("no disponible");
                    }
                    echo ''.$cat1->getNombre().'<br />';
                }
                ?>
            </td>
            <td valign="top">
                <?php
                foreach($lines as $productLine){
                    $p = $productDAO->getById($productLine->getIdProduct());
                    if( $p == null ){
                        $p = new producto ();
                        $p->setNombre("No disponible");
                        $p->setIdCat1(0);
                    }
                    $cat1 = $cat2DAO->getById($p->getIdCat2());
                    if( $cat1 == null ){
                        $cat1 = new cat1();
                        $cat1->setNombre("no disponible");
                    }
                    echo $cat1->getNombre().'<br />';
                }
                ?>
            </td>
            <td valign="top">
                <?php echo number_format($pedido->getTotal(),  2, ',', '.');?> <?php echo $pedido->getDivisa();?>
            </td>
          </tr>
      <?php } ?>
        </tbody>
     </table>
