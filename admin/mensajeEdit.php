<?php
if( !isset($_SESSION['admin']) ){
    header("location: ./index.php");
    exit;
}

include("fckeditor/fckeditor.php") ;

$id = $_GET['id'];
$mensajeDAO = new mensajeDAO();
$mensaje = $mensajeDAO->getById($id);
?>
                    <div class="titulos">
                      <div class="titulos_texto1">Mensaje del Día<div class="cerrar"><a href="../php/action/logout.php"><img src="imagenes/contenido/cerrar.png" alt="Cerrar Sesi&oacute;n" border="0" /></a></div></div>
                      <div class="titulos_texto2">
                      </div>
                    </div>
                    <!-- FIN TITULOS -->
                    <div class="contenido_marco_sup"></div>
                    <div class="contenido_fondo">
                      <div class="subcontenido">

<div class="subtitulos">
 <table width="700">
    <tr>
        <td>
            <a href="menuAdmin.php?s=mensaje">
            Mensaje del Día
            </a>
            >
            <b>Edit</b>
        </td>
    </tr>
 </table>
<div class="subtitulos_menu">
<!--  <form id="form_boton" ><input type="button" value="Adicionar Nuevo Equipo" id="nuevo" /></form> -->
</div>
</div>
<div class="subcontenido2">
<table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
</table>
	  <div id="buscador">
      <div class="rowElem"></div>
      </div>
      <div class="enunciados"></div>
      <form action="../php/action/mensajeEdit.php" method="post" enctype="multipart/form-data">
      <table align="l" width="600" border="0" cellspacing="4" cellpadding="4">
         <?php if( isset($_GET['error1']) ){?>
           <tr>
             <td colspan="4" class="EstiloRed">Tienes que introducir todos los datos</td>
             </tr>
        <?php }if( isset($_GET['error2']) ){?>
            <tr>
             <td colspan="4" class="EstiloRed">Tienes que introducir la 'imagen'</td>
             </tr>
        <?php }if( isset($_GET['error3']) ){?>
            <tr>
             <td colspan="4" class="EstiloRed">Tienes que introducir la URL ó seleccionar un producto</td>
             </tr>
        <?php }?>
          <tr>
            <td colspan="2"><b><font style="font-size:large;color:#154894">Información Básica</font></b></td>
          </tr>
          <tr>
            <td  colspan="2">
                Mensaje:<font style="font-size:x-small;color:red">*</font>
                <br />
                <?php
                $oFCKeditor_es = new FCKeditor('corta') ;
                $oFCKeditor_es->BasePath = 'fckeditor/';
                $oFCKeditor_es->Width  = '350' ;
                $oFCKeditor_es->Height = '210' ;
                $oFCKeditor_es->Value = $mensaje->getCorta();
                $oFCKeditor_es->ToolbarSet = 'CamiloCifuentes';
                $oFCKeditor_es->Create() ;
                ?>
            </td>
          </tr>
          <tr>
            <td colspan="2"><b><font style="font-size:large;color:#154894">Imagen</font></b></td>
          </tr>
          <tr>
            <td>
                Imagen principal:
                <font style="font-size:x-small; color: red">
                    dimensiones
                    (ancho mínimo 150px) !!
                </font>
                <br />
                <?php if($mensaje->getImg() != ""){ ?>
                <img src="../imagenes/mensajes/<?php echo $mensaje->getImg()?>" title="men" alt="" width="150">
                <?php }?>
            </td>
            <td><input type="file" name="file"></td>
          </tr>
          <tr>
            <td>Fecha (año-mes-dia)</td>
            <td><input type="text" name="fecha" value="<?php echo $mensaje->getFecha()?>"></td>
          </tr>
          <tr>
            <td colspan="2" align="right">
                <input type="hidden" name="id" value="<?php echo $id;?>">
                <input type="submit" value="Guardar">
            </td>
          </tr>
          <tr>
            <td colspan="2">
                <font style="font-size:x-small;color:red">
                    * campos obligatorios
                </font>
            </td>
          </tr>
      </table>
      </form>
    </div>
  </div>
  <!-- FIN SUBCONTENIDO -->
  <!-- FIN SUBCONTENIDO -->
</div>
<!-- FIN CONTENIDO FONDO -->
<div class="contenido_marco_inf"></div>
