<?php

if( !isset($_SESSION['admin']) ){
    header("location: ./index.php");
    exit;
}


$productoDAO = new productoDAO();
$banner = new publicidadDAO();
$lista = $banner->gets("id", "asc");
$total = $banner->total();
?>
                    <div class="titulos">
                      <div class="titulos_texto1">Banner Lateral<div class="cerrar"><a href="../php/action/logout.php"><img src="imagenes/contenido/cerrar.png" alt="Cerrar Sesi&oacute;n" border="0" /></a></div></div>
                      <div class="titulos_texto2">
                      </div>
                    </div>
                    <!-- FIN TITULOS -->
                    <div class="contenido_marco_sup"></div>
                    <div class="contenido_fondo">
                      <div class="subcontenido">

<div class="subtitulos">Un total de <?php echo $total;?> Banners Laterales<div class="subtitulos_menu">
<!--  <form id="form_boton" ><input type="button" value="Adicionar Nuevo Equipo" id="nuevo" /></form> -->
</div>
</div>
<div class="subcontenido2">
<form runat="server">
<table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
</table>
	  <div id="buscador">
      <div class="rowElem"></div>
      </div>
      <div class="enunciados"></div>
      <table width="100%" class="yui" id="tableOne">
        <thead>
          <tr>
            <td>
                <?php if($total < 6){ ?>
                <a href="menuAdmin.php?s=publicidadAdd">
                Nuevo [+] (máximo 6)
                </a>
                <?php }else{ ?>
                Ya tienes 6 Banners, si deseas añadir uno debes eliminar uno actual.
                <?php } ?>
            </td>
            <td colspan="3" class="filter"><span class="rowElem">
              Buscar:
              <input id="filterBoxOne" value="" maxlength="30" size="30" type="text" />
              </span>
            </td>
          </tr>
          <?php if( isset($_GET['ok']) ){ ?>
            <tr>
                <td class="EstiloGreen" colspan="4">Banner añadido</td>
            </tr>
          <?php }?>
          <?php if( isset($_GET['del']) ){ ?>
            <tr>
                <td class="EstiloGreen" colspan="4">Banner Eliminado</td>
            </tr>
          <?php }?>
          <tr>
            <th><a href='#' title="Click para ordenar">Titulo</a></th>
            <th><a href='#' title="Click para ordenar">Imagen</a></th>
            <th><a href='#' title="Click para ordenar">URL</a></th>
            <th><a href='#' title="Click para ordenar">Accion</a></th>
          </tr>
        </thead>
        <tbody>
        <?php
        foreach ($lista as $item) {
        $dir = './../php/action/publicidadDel.php?id='.$item->getId();
        ?>
          <tr>
            <td width="200"  >
                <?php echo $item->getNombre();?>
            </td>
            <td>
                <?php if($item->getImg() != ""){ ?>
                <?php if( end(explode(".",  $item->getImg())) != "swf"){ ?>
                <img src="../imagenes/banners/<?php echo $item->getImg()?>" title="noti" alt="" width="100">
                <?php
                    }else{
                        echo 'SWF';
                ?>
                <a href="../imagenes/banners/<?php echo $item->getImg()?>" target="_blank">[ver]</a>
                <?
                        }
                    }
                ?>
            </td>
            <td>
               <?php
                    if( !is_numeric($item->getUrl()) )
                        echo 'http://'.$item->getUrl();
                    else{

                        $producto = $productoDAO->getById($item->getUrl());
                        if($producto != null)
                            echo $producto->getNombre();
                        else
                            echo '<font style="color:red">Producto no encontrado</font>';

                    }

                ?>
            </td>
            <td>
                <a href="javascript:confirmar('<?php echo $dir; ?>',' Banner Lateral (<?php echo $item->getNombre();?>)')" title="Eliminar">Eliminar</a>
                |
                <a href="menuAdmin.php?s=publicidadEdit&id=<?php echo $item->getId(); ?>">Editar</a>
            </td>
          </tr>
      <?php } ?>
        </tbody>
<!--
        <tfoot>
          <tr id="pagerOne">
            <td colspan="5"><img src="jquery/jQueryTableSorterConPaging/_assets/img/first.png" class="first"/> <img src="jquery/jQueryTableSorterConPaging/_assets/img/prev.png" class="prev"/>
                <input type="text" class="pagedisplay"/>
                <img src="jquery/jQueryTableSorterConPaging/_assets/img/next.png" class="next"/> <img src="jquery/jQueryTableSorterConPaging/_assets/img/last.png" class="last"/>
                <select name="select" class="pagesize">
                  <option selected="selected"  value="10">10</option>
                  <option value="20">20</option>
                  <option value="30">30</option>
                  <option  value="40">40</option>
                </select>            </td>
          </tr>
        </tfoot>
-->
      </table>
    </form>

    </div>
  </div>
  <!-- FIN SUBCONTENIDO -->
  <!-- FIN SUBCONTENIDO -->
</div>
<!-- FIN CONTENIDO FONDO -->
<div class="contenido_marco_inf"></div>
