<?php
if( !isset($_SESSION['admin']) ){
    header("location: ./index.php");
    exit;
}

$id = $_GET['id'];
$userDAO = new userDAO;
$usuario = $userDAO->getById($id);
if( $usuario == null ){
    $usuario = new user();
    $usuario->setNombre("Este usuario ya no se encuentra en la BD. Por favor volver a la pagina anterior");
}

//$usuario = new user;
?>
                    <div class="titulos">
                      <div class="titulos_texto1">Usuario<div class="cerrar"><a href="../php/action/logout.php"><img src="imagenes/contenido/cerrar.png" alt="Cerrar Sesi&oacute;n" border="0" /></a></div></div>
                      <div class="titulos_texto2">
                      </div>
                    </div>
                    <!-- FIN TITULOS -->
                    <div class="contenido_marco_sup"></div>
                    <div class="contenido_fondo">
                      <div class="subcontenido">

<div class="subtitulos">
 <table width="700">
    <tr>
        <td>
            Ver Usuario
            >
            <a href="menuAdmin.php?s=usuario">Atrás</a>
        </td>
    </tr>
 </table>
<div class="subtitulos_menu">
<!--  <form id="form_boton" ><input type="button" value="Adicionar Nuevo Equipo" id="nuevo" /></form> -->
</div>
</div>
<div class="subcontenido2">
<table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
</table>
	  <div id="buscador">
      <div class="rowElem"></div>
      </div>
      <div class="enunciados"></div>
      <table border="0" align="center" width="700" border="0" cellspacing="4" cellpadding="4">
          <tr>
            <td colspan="2"><b><font style="font-size:large;color:#154894">Datos Generales</font></b></td>
          </tr>
          <tr>
            <td  width="250"  >Login:</td>
            <td><?php echo $usuario->getLogin();?></td>
          </tr>
          <tr>
            <td>Nombre:</td>
            <td><?php echo $usuario->getNombre();?></td>
          </tr>
          <tr>
            <td>Apellidos:</td>
            <td><?php echo $usuario->getApellidos();?></td>
          </tr>
          <tr>
            <td colspan="2"><b><font style="font-size:large;color:#154894">Contacto</font></b></td>
          </tr>
          <tr>
            <td>Empresa:</td>
            <td><?php echo $usuario->getEmpresa();?></td>
          </tr>
          <tr>
            <td>Teléfono:</td>
            <td><?php echo $usuario->getTel();?></td>
          </tr>
          <tr>
            <td>Dirección:</td>
            <td><?php echo $usuario->getDir();?></td>
          </tr>
          <tr>
            <td>Ciudad:</td>
            <td><?php echo $usuario->getCiudad();?></td>
          </tr>
          <tr>
            <td>País :</td>
            <td><?php echo $usuario->getPais();?></td>
          </tr>
          <tr>
            <td>Fecha nacimiento :</td>
            <td><?php echo $usuario->getNacimiento();?></td>
          </tr>
          <!--
          <tr>
            <td></td>
            <td><a href="menuAdmin.php?s=usuariosEditar&id=<?php echo $usuario->getId();?>"><font style="font-size:large;color:#154894">Editar</font></a></td>
          </tr>
          -->
      </table>

    </div>
  </div>
  <!-- FIN SUBCONTENIDO -->
  <!-- FIN SUBCONTENIDO -->
</div>
<!-- FIN CONTENIDO FONDO -->
<div class="contenido_marco_inf"></div>

