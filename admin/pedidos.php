<?php

if( !isset($_SESSION['admin']) ){
    header("location: ./index.php");
    exit;
}



$orderDAO = new orderDAO;
$lista = $orderDAO->gets("id", "asc");
$total = $orderDAO->total();

?>
                    <div class="titulos">
                      <div class="titulos_texto1">Pedidos<div class="cerrar"><a href="../php/action/logout.php"><img src="imagenes/contenido/cerrar.png" alt="Cerrar Sesi&oacute;n" border="0" /></a></div></div>
                      <div class="titulos_texto2">
                      </div>
                    </div>
                    <!-- FIN TITULOS -->
                    <div class="contenido_marco_sup"></div>
                    <div class="contenido_fondo">
                      <div class="subcontenido">

<div class="subtitulos">Un total de <?php echo $total;?> pedidos en el sistema<div class="subtitulos_menu">
<!--  <form id="form_boton" ><input type="button" value="Adicionar Nuevo Equipo" id="nuevo" /></form> -->
</div>
</div>
<div class="subcontenido2">
<form runat="server">
<table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
</table>
	  <div id="buscador">
      <div class="rowElem"></div>
      </div>
      <div class="enunciados"></div>
      <table width="100%" class="yui" id="tableOne">
        <thead>
          <tr>
            <td colspan="6" class="filter"><span class="rowElem">
              Buscar:
              <input id="filterBoxOne" value="" maxlength="30" size="30" type="text" />
              </span>
            </td>
          </tr>
          <?php if( isset($_GET['ok']) ){ ?>
            <tr>
                <td class="EstiloGreen" colspan="6">Producto añadido</td>
            </tr>
          <?php }?>
          <?php if( isset($_GET['del']) ){ ?>
            <tr>
                <td class="EstiloGreen" colspan="6">Producto Eliminado</td>
            </tr>
          <?php }?>
          <tr>
            <th><a href='#' title="Click para ordenar">Codigo</a></th>
            <th><a href='#' title="Click para ordenar">Estado</a></th>
            <th><a href='#' title="Click para ordenar">Fecha</a></th>
            <th><a href='#' title="Click para ordenar">Total</a></th>
            <th><a href='#' title="Click para ordenar">Método</a></th>
            <th><a href='#' title="Click para ordenar">Accion</a></th>
          </tr>
        </thead>
        <tbody>
        <?php
        foreach ($lista as $item) {
        $dir = './../php/action/pedidoDel.php?id='.$item->getId();
        ?>
          <tr>
            <td>
                <?php echo $item->getCode();?>
            </td>
            <td  ><?php echo $item->getState();?></td>
            <td  ><?php echo $item->getDate();?></td>
            <td  ><?php echo number_format($item->getTotal(),  2, ',', '.');?> (<?php echo $item->getDivisa();?>)</td>
            <td >
                <?php echo $item->getMethod();?>
            </td>
            <td >
                <a href="javascript:confirmar('<?php echo $dir; ?>',' Pedido (<?php echo $item->getCode();?>)')" title="Eliminar">Eliminar</a>
                |
                <a href="menuAdmin.php?s=pedidosVer&id=<?php echo $item->getId(); ?>" >ver</a>

            </td>
          </tr>
      <?php } ?>
        </tbody>
<!--
        <tfoot>
          <tr id="pagerOne">
            <td colspan="5"><img src="jquery/jQueryTableSorterConPaging/_assets/img/first.png" class="first"/> <img src="jquery/jQueryTableSorterConPaging/_assets/img/prev.png" class="prev"/>
                <input type="text" class="pagedisplay"/>
                <img src="jquery/jQueryTableSorterConPaging/_assets/img/next.png" class="next"/> <img src="jquery/jQueryTableSorterConPaging/_assets/img/last.png" class="last"/>
                <select name="select" class="pagesize">
                  <option selected="selected"  value="10">10</option>
                  <option value="20">20</option>
                  <option value="30">30</option>
                  <option  value="40">40</option>
                </select>            </td>
          </tr>
        </tfoot>
-->
      </table>
    </form>

    </div>
  </div>
  <!-- FIN SUBCONTENIDO -->
  <!-- FIN SUBCONTENIDO -->
</div>
<!-- FIN CONTENIDO FONDO -->
<div class="contenido_marco_inf"></div>
