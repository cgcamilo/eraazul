<script type="text/javascript">alert("'.$shipping.'")</script>
<?php
include 'includes.php';

$location = "location: ./cart.php?";
if( !isset($_SESSION['usuario']) ){
    header($location.'&user');
    exit;
}

$usuario = unserialize($_SESSION['usuario']);

$location = "location: ./checkout.php?";
if( !isset($_SESSION['order']) ){
    header($location.'&order');
    exit;
}

$order = unserialize($_SESSION['order']);
if( !$order->full() ){
    header($location.'&falta');
    exit;
}

$aspectoDAO = new AspectsDAO;
echo'<script type="text/javascript">alert("'.$shipping.'")</script>';
if( strtolower($order->getSCity() ) == strtolower("Bogot&aacute;") || strtolower($order->getSCity() ) == strtolower("Bogotá"))
    $shipping = $aspectoDAO->getAspect('shipping');
else
    $shipping = $aspectoDAO->getAspect('shipping2');


$carrito = unserialize($_SESSION['carrito']);
$total = unserialize($_SESSION['total']);

$iva = 0;
$descuento = 0;
foreach($carrito as $producto){
    if($producto->getDescuento()){
        $descuento += ($producto->getPrecioDescuento()-$producto->getPrecio());
    }
    if($producto->getIva()){
        //$iva += porcentaje($producto->getPrecio(), 16, 2);
        $productoSinIva = round(( $producto->getPrecio() / 1.16 ));
        $iva += ( ($productoSinIva - $producto->getPrecio()) * $producto->getCantidad() );
    }
}
$ivaFormato = number_format(-1*$iva ,0, ',', '.');;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>La Era Azul - Libros y Accesorios</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<link type="image/x-icon" href="era_azul.ico" rel="shortcut icon" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/large.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/mootools-core-1.3.1-full-compat-yc.js"></script>
<script type="text/javascript" src="js/sexyforms.v1.3.mootools.min.js"></script>
<script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link   type="text/css" rel="stylesheet" href="sexyforms/gamma/sexyforms-gamma.css"  media="all" id="theme" />
<script type="text/javascript">
window.addEvent('domready', function() {
  $$(".sexyform input", ".sexyform select", ".sexyform textarea").each(function(el) {
    el.DoSexy();
  });
});
</script>

<script src="js/jquery.tools.min.js"></script>
</head>
<body> 
<div id="header">
  <div class="gutter">
    <div class="inner">
      <div id="htop">
        <?php include("includes/bar.php"); ?>
        <?php include("includes/logo.php"); ?>
      </div>
      <?php include("includes/menu.php"); ?>
    </div>
  </div>
</div>
<div id="wrapper">
  <div id="wrapperbg">
    <div class="inner clearfix">
      <div id="main">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="215" valign="top">
              <?php include("includes/publicidad.php"); ?></td>
            <td valign="top"><div id="m_tot">
              <div id="contenido">
                <div class="content">
                  <div id="cart"> 
                    <h1>Pagar</h1>
                    <form action="" method="get" class="sexyform">
                     <div class="modcont">
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                         <tr>
                           <th width="10%">Tu compra</th>
                           <th width="27%">Descripción</th>
                           <th width="9%">Unidades</th>
                            <th width="14%">&nbsp;</th>
                            <th width="20%">Precio</th>
                            <th width="20%">Total</th>
                            <th width="20%">&nbsp;</th>
                         </tr>
                           <?php foreach ($carrito as $producto){ ?>
                         <tr class="cartab">
                             <td><div class="cart_ima"><a href="producto.php?id=<?php echo $producto->getId();?>"><img src="imagenes/productos/<?php echo $producto->getImg();?>" width="50" /></a></div></td>
                           <td><a href="producto.php?id=<?php echo $producto->getId();?>"><?php echo $producto->getNombre();?></a></td>
                           <td>
                             <label for="qty1"></label>
                             <?php echo $producto->getCantidad();?>
                           </td>
                           <td></td>
                           <td>$<?php echo $producto->getPrecioFormato();?></td>
                           <td>$<?php echo number_format($producto->getPrecio()*$producto->getCantidad(),  0, ',', '.')?></td>
                           <td>&nbsp;</td>
                         </tr>
                         <?php } ?>
                         <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                         </tr>
                         <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><strong>SUBTOTAL</strong></td>
                            <td><strong>$<?php echo number_format(($total+$iva+$descuento),0,',','.');?></strong></td>
                            <td>&nbsp;</td>
                         </tr>
                           <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><strong>IVA</strong></td>
                            <td><strong>$<?php echo $ivaFormato;?></strong></td>
                            <td>&nbsp;</td>
                         </tr>
                         <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><strong>Descuento</strong></td>
                            <td><strong>$<?php echo number_format($descuento,0,',','.');?></strong></td>
                            <td>&nbsp;</td>
                         </tr>
                           <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><strong>ENVIO</strong></td>
                            <td><strong>$<?php echo number_format($shipping->getValue(),0,',','.');?></strong></td>
                            <td>&nbsp;</td>
                         </tr>
                         <tr>
                           <td colspan="2">
                              
                           </td>

                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td><h6><strong>TOTAL</strong></h6></td>
                           <td><h6><strong>$<?php echo number_format($total+$shipping->getValue(),0,',','.');?></strong></h6></td>
                           <td><label for="button3"></label>
                            
                            </td>
                         </tr>
                         <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                         </tr>
                        </table>
                     </div>
                   </form>
                </div>
                </div>
              </div>
            </div>
            <div id="m_tot">
              <div id="contenido">
                <div class="content">
                  <div id="cart"> 
                    <h1>Seleccionar método de pago</h1>
                    <form action="php/action/checkOutDoPayment.php" method="get" class="sexyform">
                     <div class="modcont">
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                         <tr>
                           <td width="18%">&nbsp;</td>
                           <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="19%"><label>
                                        <input type="radio" name="metodo" value="po" checked id="metodo_0" />
                               Pago online</label>
                            </td>
                                <td width="81%"><label>
</label></td>
                              </tr>
                            </table>
                           </td>
                           </tr>
                         <tr>
                           <td valign="top">&nbsp;</td>
                           <td valign="top">&nbsp;</td>
                         </tr>
                         <tr>
                           <td valign="top">Compruebe los datos y confirme el pedido:</td>
                           <td valign="top"><p><b>Dirección de Envío:</b></p>
                             <p><?php echo $order->getSAddress();?>.<br />Barrio: <?php echo $order->getSState();?>.<br />Ciudad: <?php echo $order->getSCity();?>.</p>
                             <p><?php echo $order->getSCountry();?></p>
                             <p><b>Teléfono</b></p>
                             <p><?php echo $order->getSPhone();?></p>
                             <p><b>Email</b></p>
                             <p><?php echo $order->getSEmail();?></p>
                             <p></p>
                             <p>Verifique su orden y compruebe que los datos sean correctos. Una vez confirme el pedido, el sistema lo enviará a realizar el pago. TENGA EN CUENTA QUE SU COMPRA SÓLO SERÁ VALIDA SI EL PAGO ES APROBADO. Para mayor información contactenos.</p>
                             </td>
                           </tr>
                         <tr>
                           <td valign="top">&nbsp;</td>
                           <td valign="top">&nbsp;</td>
                         </tr>
                         <tr>
                           <td>&nbsp;</td>
                           <td>
                               <input type="submit" name="button3" id="button3" value="Confirmar" />
                               </td>
                           </tr>
                       </table>
                     </div>
                   </form>
                    <form action="checkout.php" method="get" class="sexyform">
                   <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="18%">&nbsp;</td>
                        <td width="82%"><input type="submit" name="button4" id="button4" value="Atrás" /></td>
                      </tr>
                    </table>
                   </form>
                </div>
                </div>
              </div>
            </div>
              </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include("includes/footer.php"); ?>
</div>




<!--Carrito -->
<?php include("includes/mcart.php"); ?>



<!--Registro o login -->
<?php include("includes/relog.php"); ?>
<!--Funtions -->
<script type="text/javascript" src="js/jquery.hoverIntent.minified.js"></script> 
<script type="text/javascript" src="js/funtionm.js"></script> 
<script> 
$(document).ready(function() { 
	var triggers = $(".modalInput").overlay({ 
		mask: {
			color: '#ebecff',
			loadSpeed: 200,
			opacity: 0.9
		}
	});
});
</script>
</body>
</html>