<?php

if( !isset($_SESSION['admin']) ){
    header("location: ./index.php");
    exit;
}


$mensajeDAO = new mensajeDAO();
$lista = $mensajeDAO->gets("id", "asc");
$total = $mensajeDAO->total();
?>
                    <div class="titulos">
                      <div class="titulos_texto1">Mensaje del Día<div class="cerrar"><a href="../php/action/logout.php"><img src="imagenes/contenido/cerrar.png" alt="Cerrar Sesi&oacute;n" border="0" /></a></div></div>
                      <div class="titulos_texto2">
                      </div>
                    </div>
                    <!-- FIN TITULOS -->
                    <div class="contenido_marco_sup"></div>
                    <div class="contenido_fondo">
                      <div class="subcontenido">

<div class="subtitulos">Un total de <?php echo $total;?> Mensajes<div class="subtitulos_menu">
<!--  <form id="form_boton" ><input type="button" value="Adicionar Nuevo Equipo" id="nuevo" /></form> -->
</div>
</div>
<div class="subcontenido2">
<form runat="server">
<table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td></td>
  </tr>
</table>
	  <div id="buscador">
      <div class="rowElem"></div>
      </div>
      <div class="enunciados"></div>
      <table width="100%" class="yui" id="tableOne">
        <thead>
          <tr>
            <td>
                <a href="menuAdmin.php?s=mensajeAdd">
                Nuevo [+]
                </a>
            </td>
            <td colspan="3" class="filter"><span class="rowElem">
              Buscar:
              <input id="filterBoxOne" value="" maxlength="30" size="30" type="text" />
              </span>
            </td>
          </tr>
          <?php if( isset($_GET['ok']) ){ ?>
            <tr>
                <td class="EstiloGreen" colspan="4">Mensaje añadido</td>
            </tr>
          <?php }?>
          <?php if( isset($_GET['del']) ){ ?>
            <tr>
                <td class="EstiloGreen" colspan="4">Mensaje Eliminado</td>
            </tr>
          <?php }?>
          <tr>
            <th><a href='#' title="Click para ordenar">Mensaje</a></th>
            <th><a href='#' title="Click para ordenar">Fecha</a></th>
            <th><a href='#' title="Click para ordenar">Imagen</a></th>
            <th><a href='#' title="Click para ordenar">Accion</a></th>
          </tr>
        </thead>
        <tbody>
        <?php
        foreach ($lista as $item) {
        $dir = './../php/action/mensajeDel.php?id='.$item->getId();
        ?>
          <tr>
            <td width="200"  >
                <?php echo $item->getCortaShortNoTags(100);?>
            </td>
            <td width="200"  >
                <?php echo $item->getFecha();?>
            </td>
            <td>
                <?php if($item->getImg() != ""){ ?>
                <img src="../imagenes/mensajes/<?php echo $item->getImg()?>" title="men" alt="" width="100">
                <?php }?>
            </td>
            <td>
                <a href="javascript:confirmar('<?php echo $dir; ?>',' Mensaje (<?php echo $item->getNombre();?>)')" title="Eliminar">Eliminar</a>
                |
                <a href="menuAdmin.php?s=mensajeEdit&id=<?php echo $item->getId(); ?>">Editar</a>
            </td>
          </tr>
      <?php } ?>
        </tbody>
<!--
        <tfoot>
          <tr id="pagerOne">
            <td colspan="5"><img src="jquery/jQueryTableSorterConPaging/_assets/img/first.png" class="first"/> <img src="jquery/jQueryTableSorterConPaging/_assets/img/prev.png" class="prev"/>
                <input type="text" class="pagedisplay"/>
                <img src="jquery/jQueryTableSorterConPaging/_assets/img/next.png" class="next"/> <img src="jquery/jQueryTableSorterConPaging/_assets/img/last.png" class="last"/>
                <select name="select" class="pagesize">
                  <option selected="selected"  value="10">10</option>
                  <option value="20">20</option>
                  <option value="30">30</option>
                  <option  value="40">40</option>
                </select>            </td>
          </tr>
        </tfoot>
-->
      </table>
    </form>

    </div>
  </div>
  <!-- FIN SUBCONTENIDO -->
  <!-- FIN SUBCONTENIDO -->
</div>
<!-- FIN CONTENIDO FONDO -->
<div class="contenido_marco_inf"></div>
