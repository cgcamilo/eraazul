<?php
include 'includes.php';
$aspectoDAO = new AspectsDAO();
$email = $aspectoDAO->getAspect('email');
$tel1 = $aspectoDAO->getAspect('tel1');
$tel2 = $aspectoDAO->getAspect('tel2');


if($_POST['nombre']){
    $nombre = $_POST['nombre'];
    $tel = $_POST['tel'];
    $tel = $_POST['fax'];
    $email2 = $_POST['email'];
    $corta = $_POST['corta'];
    $dir = $_POST['dir'];
    $empresa = $_POST['empresa'];

    if($email2 == "" || $nombre == "" || $corta == "")
        $error = 1;

    if($error == 0 && !ValidaMail($email2)){
        $error = 2;
    }

    if($error == 0){

        //todo bien
        $error = 3;
        //envio mail de notificacion
        $body = "<h3>Notificaci&oacute;n de contacto LA ERA AZUL  </h3>\n\n";
        $body .= "Un usuario envia este mensaje desde la zona de contacto.<br />\n";
        $body .= "<b>Nombre completo:</b> ".text2HTML($nombre)."<br />\n";
        $body .= "<b>Direcci&oacute;n:</b> ".text2HTML($dir)."<br />\n";
        $body .= "<b>Tel&eacute;fono:</b>".$tel."<br />\n";
        $body .= "<b>Fax:</b> ".text2HTML($fax)."<br />\n";
        $body .= "<b>Correo Electr&oacute;nico:</b> ".text2HTML($email2)."<br />\n";
        $body .= "<b>Empresa:</b> ".text2HTML($empresa)."<br />\n";
        $body .= "<b>Comentario:</b> <br />".text2HTML(nl2br($corta))."<br />\n";


        $mail2 = new sendCMail($email->getValue(), "info@laerazul.com.co", "zona de contacto", $body, "text/html");
        $mail2->sendMail();

        unset($_POST);
    }
}


$p1 = 'contacto.php?';
$p2 = 'contacto.php?';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>La Era Azul - Libros y Accesorios</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<link type="image/x-icon" href="era_azul.ico" rel="shortcut icon" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/large.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/mootools-core-1.3.1-full-compat-yc.js"></script>
<script type="text/javascript" src="js/sexyforms.v1.3.mootools.min.js"></script>
<script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link   type="text/css" rel="stylesheet" href="sexyforms/gamma/sexyforms-gamma.css"  media="all" id="theme" />
<script type="text/javascript">
window.addEvent('domready', function() {
  $$(".sexyform input", ".sexyform select", ".sexyform textarea").each(function(el) {
    el.DoSexy();
  });
});
</script>
<script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<script src="js/jquery.tools.min.js"></script>
<style type="text/css">
<!--
label {padding-right: 10px;}
-->
</style>
</head>
<body> 
<div id="header">
  <div class="gutter">
    <div class="inner">
      <div id="htop">
        <?php include("includes/bar.php"); ?>
        <?php include("includes/logo.php"); ?>
      </div>
      <?php include("includes/menu.php"); ?>
    </div>
  </div>
</div>
<div id="wrapper">
  <div id="wrapperbg">
    <div class="inner clearfix">
      <div id="main">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="215" valign="top"><?php include("includes/publicidad.php"); ?></td>
            <td valign="top">
            <div id="m_tot">
              <div id="contenido">
                <div class="content">
                  <div id="cart"> 
                    <h1>Contáctenos</h1>
                    <form action="" method="post" class="sexyform">
                     <div class="modcont">
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                           <?php if($error == 1){ ?>
                            <tr>
                                <td class="text_04" colspan="2">
                                    <span style="color:red">Introduce al menos el nombre, email y comentario</span>
                                </td>
                            </tr>
                            <?php } ?>
                            <?php if($error == 2){ ?>
                            <tr>
                                <td class="text_04" colspan="2">
                                    <span style="color:red">Email invalido</span>
                                </td>
                            </tr>
                            <?php } ?>
                            <?php if($error == 3){ ?>
                            <tr>
                                <td class="text_04" colspan="2">
                                    <span style="color:green">Mensaje enviado correctamente. Gracias.</span>
                                </td>
                            </tr>
                            <?php } ?>
                         <tr>
                           <td width="18%" align="right"><label for="nombre">Nombre completo:*</label></td>
                           <td width="21%"><input name="nombre" type="text" id="nombre" value="<?php echo $_POST['nombre'];?>"/></td>
                           <td width="21%" rowspan="9" valign="top">
                               <img src="img/phone.png" />
                               <h5><strong>Línea Directa</strong></h5>
                               <p><?php echo $tel1->getValue();?></p>
                                 <h5><strong>Resto del país</strong></h5>
                                 <p><?php echo $tel2->getValue();?></p>
                                 <h5><strong>Email:</strong></h5>
                                 <p><a href="mailto:<?php echo $email->getValue();?>"><?php echo $email->getValue();?></a></p>
                                 <h5><strong>Chat:</strong></h5>
                                 <p><a href="chat_login.php">Iniciar chat</a> </p>
                           </td>
                         </tr>
                         <tr>
                           <td align="right"><label for="dir">Dirección:</label></td>
                           <td>
                               <input name="dir" type="text" id="dir" value="<?php echo $_POST['dir'];?>" /></td>
                           </tr>
                         <tr>
                           <td align="right"><label for="tel">Teléfono:</label></td>
                           <td><input name="tel" type="text" id="tel" value="<?php echo $_POST['tel'];?>"/></td>
                           </tr>
                         <tr>
                           <td align="right"><label for="fax">FAX:</label></td>
                           <td><input name="fax" type="text" id="fax" value="<?php echo $_POST['fax'];?>" /></td>
                           </tr>
                         <tr>
                           <td align="right"><label for="email">Correo electrónico:*</label></td>
                           <td><input name="email" type="text" id="email" <?php echo $_POST['email'];?> /></td>
                           </tr>
                         <tr>
                           <td align="right"><label for="empresa">Empresa:</label></td>
                           <td><input name="empresa" type="text" id="empresa" value="<?php echo $_POST['empresa'];?>"/></td>
                           </tr>
                         <tr>
                           <td align="right" valign="top"><label for="comment">Comentarios:*</label></td>
                           <td valign="top"><textarea name="corta" id="comment" cols="45" rows="5"><?php echo $_POST['corta'];?></textarea></td>
                         </tr>
                         <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                           </tr>
                         <tr>
                           <td>&nbsp;</td>
                           <td valign="top"><label for="button3"></label>
                             <input type="submit" name="button3" id="button3" value="Enviar" /></td>
                           </tr>
                       </table>
                     </div>
                   </form>
                </div>
                </div>
              </div>
            </div>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include("includes/footer.php"); ?>
</div>




<!--Carrito -->
<?php include("includes/mcart.php"); ?>



<!--Registro o login -->
<?php include("includes/relog.php"); ?>
<!--Funtions -->
<script type="text/javascript" src="js/jquery.hoverIntent.minified.js"></script> 
<script type="text/javascript" src="js/funtionm.js"></script> 
<script> 
$(document).ready(function() { 
	var triggers = $(".modalInput").overlay({ 
		mask: {
			color: '#ebecff',
			loadSpeed: 200,
			opacity: 0.9
		}
	});
});
</script>
</body>
</html>