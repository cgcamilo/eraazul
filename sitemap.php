<?php
include 'includes.php';

$cat1DAO = new cat1DAO();
$cat2DAO = new cat2DAO();

$p1 = 'sitemap.php?';
$p2 = 'sitemap.php?';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>La Era Azul - Libros y Accesorios</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<link type="image/x-icon" href="era_azul.ico" rel="shortcut icon" />
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="css/large.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/mootools-core-1.3.1-full-compat-yc.js"></script>
<script type="text/javascript" src="js/sexyforms.v1.3.mootools.min.js"></script>
<script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link   type="text/css" rel="stylesheet" href="sexyforms/gamma/sexyforms-gamma.css"  media="all" id="theme" />
<script type="text/javascript">
window.addEvent('domready', function() {
  $$(".sexyform input", ".sexyform select", ".sexyform textarea").each(function(el) {
    el.DoSexy();
  });
});
</script>

<script src="js/jquery.tools.min.js"></script>
<style type="text/css">
<!--
.sitemap{background:#6AA63B url(img/green-item-bg.jpg) top left no-repeat;}
.inisit{color:#FFFFFF}
.sitactive {color:#fff;background: transparent;font-weight:bold;}
-->
</style>
</head>
<body> 
<div id="header">
  <div class="gutter">
    <div class="inner">
      <div id="htop">
        <?php include("includes/bar.php"); ?>
        <?php include("includes/logo.php"); ?>
      </div>
      <?php include("includes/menu.php"); ?>
    </div>
  </div>
</div>
<div id="wrapper">
  <div id="wrapperbg">
    <div class="inner clearfix">
      <div id="main">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="215" valign="top"><?php include("includes/publicidad.php"); ?></td>
            <td valign="top">
            <div id="m_tot">
              <div id="contenido">
                <div class="contentx">
                  <div id="cart"> 
                    <h1>Mapa del Sitio</h1>
                     <div class="modcont">
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                         <tr>
                           <td width="66%" valign="top">
                           <div id="sitemap">
                            <ul class="arrow">
                              <li><a href="index.php">INICIO</a></li>
                              <li><a href="#" class="modalInput" rel="#modalreglog">INGRESAR / REGISTRARSE</a></li>
                              <li><a href="contacto.php">CONTÁCTENOS</a></li>
                              <li><a href="cart.php">CARRITO DE COMPRAS</a></li>
                              <li><a href="cita.php">MENSAJE DEL DÍA</a></li>
                              <?php
                                    $cats1 = $cat1DAO->gets("id", "asc");
                                    foreach($cats1 as $cat1){
                                        $cats2 = $cat2DAO->getsByIdCat1("id", "asc", $cat1->getId());
                                ?>
                              <li>
                                <a href="productos.php?param=idCat1&value=<?php echo $cat1->getId();?>">
                                    <?php echo $cat1->getNombre();?>
                                </a>
                                  <?php if(count($cats2) > 0){ ?>
                                <ul>
                                  <?php foreach($cats2 as $cat2){ ?>
                                  <li><a href="productos.php?param=idCat2&value=<?php echo $cat2->getId();?>"><?php echo $cat2->getNombre();?></a></li>
                                  <?php } ?>
                                </ul>
                                  <?php } ?>
                              </li>
                              <?php } ?>
                              <li>
                                <a href="promo.php">PROMOCIONES</a>
                              </li>
                            </ul> 
                            </div>
                           </td>
                            <td width="34%" valign="top">
                              <img src="img/sitemapbg.png" />                            </td>
                          </tr>
                        </table>
                     </div>
                  </div>
                </div>
              </div>
            </div>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<?php include("includes/footer.php"); ?>
</div>




<!--Carrito -->
<?php include("includes/mcart.php"); ?>



<!--Registro o login -->
<?php include("includes/relog.php"); ?>
<!--Funtions -->
<script type="text/javascript" src="js/jquery.hoverIntent.minified.js"></script> 
<script type="text/javascript" src="js/funtionm.js"></script> 
<script> 
$(document).ready(function() { 
	var triggers = $(".modalInput").overlay({ 
		mask: {
			color: '#ebecff',
			loadSpeed: 200,
			opacity: 0.9
		}
	});
});
</script>
</body>
</html>